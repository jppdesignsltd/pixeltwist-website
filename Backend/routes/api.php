<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Tag API Routes
Route::resource('tag', 'TagController', ['except' => ['edit', 'create'], 'middleware' => ['auth.jwt']]);

// Service API Routes
Route::resource('service', 'ServiceController', ['except' => ['edit', 'create'], 'middleware' => ['auth.jwt']]);

// Category API Routes
Route::resource('category', 'CategoryController', ['except' => ['edit', 'create'], 'middleware' => ['auth.jwt']]);

// Post API Routes
Route::resource('post', 'PostController', ['except' => ['edit', 'create'], 'middleware' => ['auth.jwt']]);

// Roles API Routes
Route::resource('roles', 'RoleController', ['except' => ['show', 'edit', 'destroy'], 'middleware' => ['auth.jwt']]);

// Permission API Routes
Route::resource('permissions', 'PermissionController', ['except' => ['show', 'edit', 'destroy'], 'middleware' => ['auth.jwt']]);

// Project API Routes
Route::resource('project', 'ProjectController', ['except' => ['edit', 'create'], 'middleware' => ['auth.jwt']]);

// Blog API Routes
Route::resource('blog', 'BlogController', ['except' => ['edit', 'create', 'destroy', 'update']]);
Route::get('blog/category/{slug}', ['uses' => 'BlogController@getCategoryPosts']);
Route::get('blog/tag/{slug}', ['uses' => 'BlogController@getTagPosts']);

// Work API Routes
Route::resource('work', 'WorkController', ['except' => ['edit', 'create', 'destroy', 'update']]);
Route::get('work/service/{slug}', ['uses' => 'WorkController@getServiceProjects']);
Route::get('work/tag/{slug}', ['uses' => 'WorkController@getTagProjects']);

// User API Routes
Route::resource('user', 'UserController', ['except' => ['edit', 'create', 'destroy', 'store']]);

// Auth API Routes
Route::get('auth/refresh', ['uses' => 'AuthController@refreshToken', 'middleware' => 'auth.jwt']);
Route::get('auth', ['uses' => 'AuthController@index', 'middleware' => 'auth.jwt']);
Route::post('register', ['uses' => 'UserController@store']);
Route::post('signin', ['uses' => 'AuthController@signin']);
Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


// Author API Routes
Route::get('author', ['uses' => 'AuthorController@index', 'middleware' => ['auth.jwt']]);
Route::get('author/project', ['uses' => 'AuthorController@getAuthorProjects', 'middleware' => 'auth.jwt']);
Route::get('author/post', ['uses' => 'AuthorController@getAuthorPosts', 'middleware' => 'auth.jwt']);
