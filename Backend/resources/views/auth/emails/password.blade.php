Click here to reset your password:
<br>
<br>
<a href="http://localhost:4200/auth/reset-password?token={{ $token }}">
    Reset Password
</a>
<br>
<br>
Alternatively copy and paste this link
<br>
http://localhost:4200/auth/reset-password?token={{ $token }}