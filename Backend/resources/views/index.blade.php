<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pixel Twist</title>
    </head>
    <body>
        <p>
            We are not using this. Please contact support.
        </p>
    </body>
</html>
