<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$post = [
			[
				'title' => 'Initial Test Post',
				'subtitle' => 'This is a test subtitle',
				'slug' => 'initial-test-post',
				'category_id' => '1',
				'user_id' => '1',
				'meta_author' => '1',
				'status' => 'published',
				'main_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, dolores velit! Alias cum, earum eos esse excepturi itaque laudantium minima necessitatibus nihil odit recusandae tempore, vitae voluptate. Amet blanditiis, tenetur.</p>',
				'live_date' => '2017-08-11',
			],
		];

		foreach ($post as $key => $value) {
			Post::create($value);
		}
	}
}
