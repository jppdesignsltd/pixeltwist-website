<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$permissions_roles = [
			'standard' => ['standard'],
			'write-posts' => ['editor', 'admin', 'author'],
			'write-projects' => ['editor', 'admin', 'author'],
			'edit-posts' => ['editor', 'admin'],
			'edit-projects' => ['editor', 'admin'],
			'manage-users' => ['admin'],
			'manage-roles' => ['admin'],
			'manage-permissions' => ['admin']
		];

		foreach ($permissions_roles as $permission => $roles) {

			foreach ($roles as $role) {

				DB::table('permission_role')->insert([
					'permission_id' => DB::table('permissions')->where('name', '=', $permission)->first()->id,
					'role_id' => DB::table('roles')->where('name', '=', $role)->first()->id
				]);

			}
		}

	}
}
