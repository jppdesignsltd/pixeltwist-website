<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$role1 = new Role();
		$role1->name = 'standard';
		$role1->display_name = 'Standard User'; // optional
		$role1->description = 'Standard Account User'; // optional
		$role1->save();

		$role2 = new Role();
		$role2->name = 'author';
		$role2->display_name = 'Author'; // optional
		$role2->description = 'This user can only create posts/projects'; // optional
		$role2->save();

		$role3 = new Role();
		$role3->name = 'editor';
		$role3->display_name = 'Editor'; // optional
		$role3->description = 'This user can see the admin panel and edit/create post and project content only.'; // optional
		$role3->save();

		$role4 = new Role();
		$role4->name = 'admin';
		$role4->display_name = 'Admin'; // optional
		$role4->description = 'This user is all powerful!!!'; // optional
		$role4->save();
	}
}
