<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $webdesign = new Service();
        $webdesign->title = 'Web Design';
        $webdesign->slug = 'web-design';

        $webdesign->save();

        $webdev = new Service();
        $webdev->title = 'Web Development';
        $webdev->slug = 'web-development';

        $webdev->save();

        $email = new Service();
        $email->title = 'Email Setup';
        $email->slug = 'email-setup';

        $email->save();

        $logo = new Service();
        $logo->title = 'Logo Design';
        $logo->slug = 'logo-design';

        $logo->save();

        $visual = new Service();
        $visual->title = 'Visual Branding';
        $visual->slug = 'visual-branding';

        $visual->save();

        $cms = new Service();
        $cms->title = 'Wordpress CMS';
        $cms->slug = 'wordpress-cms';

        $cms->save();

        $custom = new Service();
        $custom->title = 'Custom CMS';
        $custom->slug = 'custom-cms';

        $custom->save();

        $graphicdesign = new Service();
        $graphicdesign->title = 'Graphic Design';
        $graphicdesign->slug = 'graphic-design';

        $graphicdesign->save();

        $seo = new Service();
        $seo->title = 'SEO';
        $seo->slug = 'seo';

        $seo->save();

        $hosting = new Service();
        $hosting->title = 'Hosting';
        $hosting->slug = 'Hosting';

        $hosting->save();
    }
}
