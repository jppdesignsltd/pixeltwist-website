<?php

use Illuminate\Database\Seeder;

class ProjectServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project_services = [
            'web-design' => ['cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'web-development' => ['gipsy-hill-brewing-company', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'logo-design' => ['cyclone-promotions', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'visual-branding' => ['cyclone-promotions', 'mj-rooney'],
            'wordpress-cms' => ['gipsy-hill-brewing-company', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'email-setup' => ['cc-east-dulwich', 'quick-print'],
            'graphic-design' => ['cyclone-promotions', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'seo' => ['quick-print'],
            'hosting' => ['gipsy-hill-brewing-company', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london']
        ];

        foreach ($project_services as $service => $projects) {

            foreach ($projects as $project) {

                DB::table('project_service')->insert([
                    'project_id' => DB::table('projects')->where('slug', '=', $project)->first()->id,
                    'service_id' => DB::table('services')->where('slug', '=', $service)->first()->id
                ]);

            }
        }
    }
}
