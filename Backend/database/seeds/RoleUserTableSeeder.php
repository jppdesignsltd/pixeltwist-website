<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleUserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    DB::table('role_user')->insert([
      'user_id' => DB::table('users')->where('firstname', '=', 'Tech')->first()->id,
      'role_id' => DB::table('roles')->where('name', '=', 'admin')->first()->id
    ]);

    DB::table('role_user')->insert([
      'user_id' => DB::table('users')->where('firstname', '=', 'Editor')->first()->id,
      'role_id' => DB::table('roles')->where('name', '=', 'editor')->first()->id
    ]);

		DB::table('role_user')->insert([
			'user_id' => DB::table('users')->where('firstname', '=', 'Author')->first()->id,
			'role_id' => DB::table('roles')->where('name', '=', 'author')->first()->id
		]);

    DB::table('role_user')->insert([
      'user_id' => DB::table('users')->where('firstname', '=', 'Standard')->first()->id,
      'role_id' => DB::table('roles')->where('name', '=', 'standard')->first()->id
    ]);

  }
}
