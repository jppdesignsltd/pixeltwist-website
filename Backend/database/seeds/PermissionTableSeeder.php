<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$permission = [
			[
				'name' => 'standard',
				'display_name' => 'Standard User'
			],
			[
				'name' => 'write-posts',
				'display_name' => 'Create Posts'
			],
			[
				'name' => 'write-projects',
				'display_name' => 'Create Projects'
			],
			[
				'name' => 'edit-posts',
				'display_name' => 'Edit Posts'
			],
			[
				'name' => 'edit-projects',
				'display_name' => 'Edit Projects'
			],
			[
				'name' => 'manage-users',
				'display_name' => 'Manage Users'
			],
			[
				'name' => 'manage-roles',
				'display_name' => 'Manage Roles'
			],
			[
				'name' => 'manage-permissions',
				'display_name' => 'Manage Permissions'
			]
		];

		foreach ($permission as $key => $value) {
			Permission::create($value);
		}
	}
}
