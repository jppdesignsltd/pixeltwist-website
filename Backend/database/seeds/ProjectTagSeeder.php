<?php

use Illuminate\Database\Seeder;

class ProjectTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project_tags = [
            'web-development' => ['gipsy-hill-brewing-company', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'graphic-design' => ['cyclone-promotions', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london'],
            'seo' => ['quick-print'],
            'hosting' => ['gipsy-hill-brewing-company', 'cc-east-dulwich', 'mj-rooney', 'quick-print', 'tree-of-life-london']
        ];

        foreach ($project_tags as $tag => $projects) {

            foreach ($projects as $project) {

                DB::table('project_tag')->insert([
                    'project_id' => DB::table('projects')->where('slug', '=', $project)->first()->id,
                    'tag_id' => DB::table('tags')->where('slug', '=', $tag)->first()->id
                ]);

            }
        }
    }
}
