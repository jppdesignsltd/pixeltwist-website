<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $tech = new User();
    $tech->username = 'Tech';
    $tech->firstname = 'Tech';
    $tech->lastname = 'JPPdesigns';
    $tech->email = 'jpp@jppdesigns.co.uk';
    $tech->password = bcrypt('test123');

    $tech->save();

    $editor = new User();
    $editor->username = 'Editor';
    $editor->firstname = 'Editor';
    $editor->lastname = 'Test';
    $editor->email = 'editor@test.co.uk';
    $editor->password = bcrypt('test123');

    $editor->save();

		$author = new User();
		$author->username = 'Author';
		$author->firstname = 'Author';
		$author->lastname = 'Test';
		$author->email = 'author@test.co.uk';
		$author->password = bcrypt('test123');

		$author->save();

    $demo = new User();
    $demo->username = 'Standard';
    $demo->firstname = 'Standard';
    $demo->lastname = 'User';
    $demo->email = 'test@test.com';
    $demo->password = bcrypt('test123');

    $demo->save();
  }
}
