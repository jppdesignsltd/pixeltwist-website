<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(ProjectServiceSeeder::class);
        $this->call(ProjectTagSeeder::class);
    }
}
