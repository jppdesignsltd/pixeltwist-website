<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $webdesign = new Category();
        $webdesign->title = 'Web Design';
        $webdesign->slug = 'web-design';

        $webdesign->save();

		$graphicdesign = new Category();
		$graphicdesign->title = 'Graphic Design';
		$graphicdesign->slug = 'graphic-design';

		$graphicdesign->save();

		$seo = new Category();
		$seo->title = 'SEO';
		$seo->slug = 'seo';

		$seo->save();

		$hosting = new Category();
		$hosting->title = 'Hosting';
		$hosting->slug = 'Hosting';

		$hosting->save();
    }
}
