<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $webdesign = new Tag();
        $webdesign->title = 'Web Development';
        $webdesign->slug = 'web-development';

        $webdesign->save();

        $graphicdesign = new Tag();
        $graphicdesign->title = 'Graphic Design';
        $graphicdesign->slug = 'graphic-design';

        $graphicdesign->save();

        $seo = new Tag();
        $seo->title = 'SEO';
        $seo->slug = 'seo';

        $seo->save();

        $hosting = new Tag();
        $hosting->title = 'Hosting';
        $hosting->slug = 'Hosting';

        $hosting->save();
    }
}
