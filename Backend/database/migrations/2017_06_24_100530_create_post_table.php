<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('subtitle')->nullable();
			$table->string('slug');
			$table->string('featured_image')->nullable();
			$table->string('status');
			$table->date('live_date');
			$table->text('main_content')->nullable();
			$table->integer('user_id')->unsigned();
			$table->integer('category_id')->unsigned();
			$table->integer('meta_author');
			$table->text('meta_keywords')->nullable();
			$table->text('meta_description')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('posts');
	}
}
