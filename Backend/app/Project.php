<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	/**
	 * Tags That Belong to project
	 **/
	public function tags() {
		return $this->belongsToMany('App\Tag');
	}

	/**
	 * Services That Belong to project
	 **/
	public function services() {
		return $this->belongsToMany('App\Service');
	}

	/**
	 * User That Belong to project
	 **/
	public function user() {
		return $this->belongsTo('App\User');
	}
}
