<?php

namespace App\Http\Controllers;

use App\Permission;
use JWTAuth;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
	/**
	 * Instantiate a new PostController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->token = JWTAuth::getToken();
		if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
		$this->noPermission = ['error' => 'You do not have permission'];
	}

	public function index()
	{
		if (!$this->user->hasRole(['admin'])) {
			return response()->json($this->noPermission, 401);
		};

		$permission = Permission::with('roles')->get();

		$response = [
			'permissions' => $permission
		];

		return response()->json($permission, 200);

	}
}
