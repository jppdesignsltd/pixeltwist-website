<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Http\Request;
use App\User;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getResetToken(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response()->json([
                'error' => 'Email address does not match any registered users'
            ], 400);
        }

        $token = $this->broker()->createToken($user);

		$data = [
			'from' => 'j.pierrepowell@gmail.com',
			'email' => $request->input('email'),
			'token' => $token,
			'subject' => 'Reset Password'
		];

		Mail::send('auth.emails.password', $data, function($message) use ($data){
			$message->to($data['email']);
			$message->subject($data['subject']);
			$message->from('j.pierrepowell@gmail.com', 'Me');
		});

        return response()->json(['reset_token' => $token], 201);
    }


}
