<?php

namespace App\Http\Controllers;

use App\Project;
use App\Service;
use App\Tag;
use Illuminate\Http\Request;

class WorkController extends Controller
{

    public function index()
    {

        $projects = Project::with('tags')->with('user')->with('services')
            ->where('status', 'published')
            ->get();

        return response()->json($projects, 200);

    }

    public function getServiceProjects($slug)
    {
        $service = Service::where('slug', $slug)->first();

        if ($service) {

            $projects = Project::with('services')->with('user')->with('tags')
                ->whereHas('services', function ($query) use ($slug) {
                    $query->where('slug', $slug);
                })
                ->get();

            return response()->json($projects, 200);

        }
    }

    public function getTagProjects($slug)
    {
        $tag = Tag::where('slug', $slug)->first();

        if ($tag) {

            $projects = Project::with('services')->with('user')->with('tags')
                ->whereHas('tags', function ($query) use ($slug) {
                    $query->where('slug', $slug);
                })
                ->get();

            return response()->json($projects, 200);

        }
    }

    public function show($slug)
    {

        $project = Project::where('slug', '=', $slug)->with('tags')->with('services')->with('user')->first();

        if (!$project) {
            return response()->json(['error' => 'Project not found'], 404);
        }

        return response()->json($project, 200);

    }

}
