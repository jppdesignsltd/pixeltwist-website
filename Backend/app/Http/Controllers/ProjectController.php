<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Image;
use JWTAuth;
use Purifier;
use File;

class ProjectController extends Controller
{
    /**
     * Instantiate a new ProjectController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {

        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $projects = Project::with('tags')->with('services')->with('user')->get();

        return response()->json($projects, 200);

    }

    public function store(Request $request)
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $project = new Project();

        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'sometimes',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:projects,slug',
            'logo_image' => 'sometimes',
            'cover_image' => 'sometimes',
            'website_url' => 'sometimes',
            'user_id' => 'required',
            'status' => 'required',
            'main_content' => 'sometimes',
            'meta_keywords' => 'sometimes',
            'meta_description' => 'sometimes',
            'tags' => 'sometimes',
            'services' => 'sometimes'
        ]);

        $project->title = $request->title;

        if (isset($request->subtitle)) {
            $project->subtitle = $request->subtitle;
        }

        if (isset($request->website_url)) {
            $project->website_url = $request->website_url;
        }

        if (isset($request->logo_image)) {

            $image = $request->logo_image;

            $fileExt = explode(';', $image);
            $imageExt = explode('/', $fileExt[0]);

            $filename = time() . '_logo_' . $this->user->id . '.' . $imageExt[1];

            $location = public_path('uploads/projects/' . $filename);

            Image::make($image)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);

            $project->logo_image = $filename;
        }

        if (isset($request->cover_image)) {

            $coverImage = $request->cover_image;

            $coverFileExt = explode(';', $coverImage);
            $coverImageExt = explode('/', $coverFileExt[0]);

            $coverFilename = time() . '_cover_' . $this->user->id . '.' . $coverImageExt[1];

            $coverLocation = public_path('uploads/projects/' . $coverFilename);

            Image::make($coverImage)->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($coverLocation);

            $project->cover_image = $coverFilename;
        }

        if (isset($request->main_content)) {
            $project->main_content = Purifier::clean($request->main_content);
        }

        $project->slug = $request->slug;
        $project->status = $request->status;

        $project->user_id = $request->user_id;
        $project->meta_author = $request->user_id;

        if (isset($request->meta_keywords)) {
            $project->meta_keywords = $request->meta_keywords;
        }

        if (isset($request->meta_description)) {
            $project->meta_description = $request->meta_description;
        }

        $project->save();

        if (isset($request->tags)) {
            $project->tags()->sync($request->tags, false);
        } else {
            $project->tags()->sync([]);
        }

        if (isset($request->services)) {
            $project->services()->sync($request->services, false);
        } else {
            $project->services()->sync([]);
        }

        $project = Project::with('tags')->with('services')->find($project->id);

        return response()->json($project, 201);

    }

    public function show($id)
    {

        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $project = Project::with('tags')->with('services')->with('user')->find($id);

        if ($project) {
            return response()->json($project, 200);
        }

        return response()->json(['error' => 'Project not found'], 404);

    }

    public function update(Request $request, $id)
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $project = Project::find($id);

        if (!$project) {
            return response()->json(['error' => 'Project not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'sometimes',
            'slug' => "required|alpha_dash|min:1|max:255|unique:projects,slug,$id",
            'logo_image' => 'sometimes',
            'cover_image' => 'sometimes',
            'status' => 'required',
            'website_url' => 'sometimes',
            'user_id' => 'required',
            'main_content' => 'sometimes',
            'meta_keywords' => 'sometimes',
            'meta_description' => 'sometimes',
            'tags' => 'sometimes',
            'services' => 'sometimes'
        ]);

        $project->title = $request->title;

        if (isset($request->subtitle)) {
            $project->subtitle = $request->subtitle;
        }

        if (isset($request->website_url)) {
            $project->website_url = $request->website_url;
        }

        // Logo Image Store
        if ($request->logo_image === 'delete') {

            // Grab Old File Name and delete it
            $oldFileName = $project->logo_image;

            if ($oldFileName !== null) {
                File::delete('uploads/projects/' . $oldFileName);
            }

            $project->logo_image = '';

        } else if (isset($request->logo_image) && $request->logo_image !== null) {

            $image = $request->logo_image;
            // Grab Old File Name
            $oldFileName = $project->logo_image;

            if ($image !== $oldFileName) {
                $fileExt = explode(';', $image);
                $imageExt = explode('/', $fileExt[0]);

                $filename = time() . '_logo_' . $this->user->id . '.' . $imageExt[1];

                $location = public_path('uploads/projects/' . $filename);

                Image::make($image)->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($location);

                $project->logo_image = $filename;

                if ($oldFileName !== $filename) {
                    File::delete('uploads/projects/' . $oldFileName);
                }
            }
        }

        // Cover Image Store
        if ($request->cover_image === 'delete') {

            // Grab Old File Name and delete it
            $coverOldFileName = $project->cover_image;

            if ($coverOldFileName !== null) {
                File::delete('uploads/projects/' . $coverOldFileName);
            }

            $project->cover_image = '';

        } else if (isset($request->cover_image) && $request->cover_image !== null) {

            $coverImage = $request->cover_image;
            // Grab Old File Name
            $coverOldFileName = $project->cover_image;

            if ($coverImage !== $coverOldFileName) {

                $coverFileExt = explode(';', $coverImage);
                $coverImageExt = explode('/', $coverFileExt[0]);

                $coverFilename = time() . '_cover_' . $this->user->id . '.' . $coverImageExt[1];

                $coverLocation = public_path('uploads/projects/' . $coverFilename);

                Image::make($coverImage)->resize(1280, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($coverLocation);

                $project->cover_image = $coverFilename;

                if ($coverOldFileName !== $coverFilename) {
                    File::delete('uploads/projects/' . $coverOldFileName);
                }
            }
        }

        if (isset($request->main_content)) {
            $project->main_content = Purifier::clean($request->main_content);
        }

        $project->slug = $request->slug;

        if (isset($request->status)) {
            $project->status = $request->status;
        }

        if (isset($request->user_id)) {
            $project->user_id = $request->user_id;
            $project->meta_author = $request->user_id;
        }

        if (isset($request->meta_keywords)) {
            $project->meta_keywords = $request->meta_keywords;
        }

        if (isset($request->meta_description)) {
            $project->meta_description = $request->meta_description;
        }

        $project->save();

        if (isset($request->tags)) {
            $project->tags()->sync($request->tags, true);
        } else {
            $project->tags()->sync([]);
        }

        if (isset($request->services)) {
            $project->services()->sync($request->services, true);
        } else {
            $project->services()->sync([]);
        }

        $project = Project::with('tags')->with('services')->find($project->id);

        return response()->json($project, 200);

    }

    public function destroy($id)
    {

        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $project = Project::find($id);

        // Get Logo Image Value and remove if preset
        if (strlen($project->logo_image) > 0) {
            File::delete('uploads/projects/' . $project->logo_image);
        }

        // Get Cover Image Value and remove if present
        if (strlen($project->cover_image) > 0) {
            File::delete('uploads/projects/' . $project->cover_image);
        }


        $project->delete();

        return response()->json(['error' => 'Project Deleted'], 200);

    }

}
