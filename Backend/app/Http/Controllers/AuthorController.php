<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Project;
use App\Post;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Auth;
use Entrust;

class AuthorController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth.jwt', 'refresh.jwt'], ['only' => [
            'index',
            'getAuthorProjects',
            'getAuthorPosts'
        ]]);

        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {

        $token = JWTAuth::getToken();

        if ($token) $user = JWTAuth::parseToken()->toUser();

        if (!$user->hasRole(['admin', 'editor', 'author'])) return response()->json($this->noPermission, 401);

        $authors = User::with('roles.permissions')->whereHas('roles', function ($query) {
            $query->where('name', 'admin')->orWhere('name', 'editor')->orWhere('name', 'author');
        })->get();


        return response()->json($authors, 200);

    }

    public function getAuthorProjects()
    {

        if (!$this->user->hasRole(['author'])) {
            return response()->json($this->noPermission, 401);
        };

        $projects = Project::with('tags')->with('services')->with('user')->where('user_id', $this->user->id)->get();

        return response()->json($projects, 200);

    }

    public function getAuthorPosts()
    {

        if (!$this->user->hasRole(['author'])) {
            return response()->json($this->noPermission, 401);
        };

        $posts = Post::with('category')->with('tags')->with('user')->where('user_id', $this->user->id)->get();


        return response()->json($posts, 200);

    }

}
