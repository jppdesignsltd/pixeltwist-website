<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Auth;
use Entrust;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth.jwt'], ['only' => [
            'index',
            'refreshToken'
        ]]);

        $this->noPermission = ['error' => 'You do not have permission'];

    }

    public function signin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        $token = JWTAuth::attempt($credentials);

        $user = Auth::user();

        try {

            if ($user) {

                if ($user->active === '0') {
                    return response()->json([
                        'error' => 'User not active. Please contact support or your system administrator.'
                    ], 401);
                }

            }

            if (!$token) {

                return response()->json([
                    'error' => 'Invalid Credentials'
                ], 401);

            }

        } catch (JWTException $e) {

            return response()->json([
                'error' => 'Could not create token!'
            ], 500);

        }

        $user = User::with('roles.permissions')->find($user->id);

        return response()->json([
            'token' => $token,
            'user' => $user->id,
            'role' => $user->roles()->first()->name
        ], 200);

    }

    public function index()
    {

        $token = JWTAuth::getToken();

        if ($token) {
            $user = JWTAuth::parseToken()->authenticate();

            $user = User::with('roles.permissions')->find($user->id);

            return response()->json($user, 200);
        }

        return response()->json(['error' => 'User cannot be found'], 433);

    }

    public function refreshToken()
    {
        $token = JWTAuth::getToken();

        if ($token) {
            $newToken = JWTAuth::refresh($token);

            $response = [
                'token' => $newToken
            ];

            return response()->json($response, 200);
        }

        return response()->json(['error' => 'Token not refreshed'], 433);
    }

}
