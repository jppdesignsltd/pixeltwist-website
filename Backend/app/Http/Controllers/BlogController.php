<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Project;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function index()
    {
        $today = Carbon::today();

        $posts = Post::with('category')->with('user')->with('tags')
            ->where('live_date', '<=', $today)
            ->where('status', 'published')
            ->get();

        return response()->json($posts, 200);
    }

    public function show($slug)
    {
        $post = Post::where('slug', '=', $slug)->with('category')->with('user')->with('tags')->first();

        if (!$post) {
            return response()->json(['error' => 'Post not found'], 404);
        }

        return response()->json($post, 200);
    }

    public function getTagPosts($slug)
    {
        $tag = Tag::where('slug', $slug)->first();

        if ($tag) {

            $posts = Post::with('category')->with('user')->with('tags')
                ->whereHas('tags', function ($query) use ($slug) {
                    $query->where('slug', $slug);
                })
                ->get();

            return response()->json($posts, 200);

        }
    }

    public function getCategoryPosts($slug)
    {
        $category = Category::where('slug', $slug)->first();

        if ($category) {

            $posts = Post::with('category')->with('user')->with('tags')
                ->where('category_id', $category->id)
                ->get();


            return response()->json($posts, 200);

        }
    }
}
