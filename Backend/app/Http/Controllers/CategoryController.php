<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\User;
use JWTAuth;
use Illuminate\Http\Request;
use Zizaco\Entrust\Entrust;

class CategoryController extends Controller
{

    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $categories = Category::with('posts')->get();


        return response()->json($categories, 200);
    }

    public function store(Request $request)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $category = new Category();

        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:categories,slug',
        ]);

        $category->title = $request->title;
        $category->slug = $request->slug;

        $category->save();

        $newCategory = Category::with('posts')->find($category->id);

        return response()->json($newCategory, 201);

    }

    public function show($id)
    {

        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $category = Category::with('posts.tags')->with('posts.user')->find($id);

        if ($category) {
            return response()->json($category, 200);
        }

        return response()->json(['error' => 'Category not found'], 404);
    }

    public function update(Request $request, $id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $category = Category::find($id);

        if (!$category) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'slug' => "required|alpha_dash|min:1|max:255|unique:categories,slug, $id",
        ]);

        $category->title = $request->title;
        $category->slug = $request->slug;

        $category->save();

        return response()->json($category, 200);

    }

    public function destroy($id)
    {

        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $category = Category::find($id);

        if (!$category) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        $post = Post::where('category_id', $category->id)->get();

        if ($post->count() > 0) {
            return response()->json(['error' => 'Cannot delete category as there are posts attributed to it.'], 400);
        }

        $category->delete();

        return response()->json(['error' => 'Category Deleted'], 200);

    }

}
