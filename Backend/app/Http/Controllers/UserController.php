<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Image;
use JWTAuth;
use Auth;
use File;
use Entrust;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth.jwt', 'refresh.jwt'], ['only' => [
            'index',
            'show',
            'update'
        ]]);

        $this->noPermission = ['error' => 'You do not have permission'];

    }

    public function index()
    {
        $token = JWTAuth::getToken();
        if ($token) $user = JWTAuth::parseToken()->toUser();
        if (!$user->hasRole(['admin'])) return response()->json($this->noPermission, 401);

        $users = User::with('roles')->get();

        return response()->json($users, 200);
    }

    public function show($id)
    {
        $user = User::with('roles.permissions')->find($id);

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json($user, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'sometimes'
        ]);

        $user = new User([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);

        if (isset($request->role)) {
            $role = Role::where('name', $request->role)->get()->first();
        } else {
            $role = Role::where('name', 'standard')->get()->first();
        }

        $user->save();

        $user->roles()->attach($role);

        $user = User::with('roles.permissions')->find($user->id);

        return response()->json($user, 200);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $currentUser = Auth::user();

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'password' => 'sometimes',
            'avatar' => 'sometimes',
            'role' => 'sometimes'
        ]);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;

        if ($request->active === '0') {

            if ($user->id === $currentUser->id) {
                return response()->json([
                    'error' => 'You are currently logged in you cannot change your active status.'
                ], 401);
            }
            $user->active = $request->active;

        } else {

            $user->active = $request->active;

        }

        if ($request->avatar === 'delete') {

            // Grab Old File Name and delete it
            $oldFileName = $user->avatar;

            if ($oldFileName !== null) {
                File::delete('uploads/profiles/' . $oldFileName);
            }

            $user->avatar = '';

        } else if (isset($request->avatar)) {

            $image = $request->avatar;

            $fileExt = explode(';', $image);
            $imageExt = explode('/', $fileExt[0]);

            $filename = time() . '_' . $user->id . '.' . $imageExt[1];

            $location = public_path('uploads/profiles/' . $filename);

            Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);

            // Grab Old File Name
            $oldFileName = $user->avatar;

            $user->avatar = $filename;

            if ($oldFileName !== $filename) {
                File::delete('uploads/profiles/' . $oldFileName);
            }
        } else if ($request->avatar === null) {

            // Grab Old File Name and delete it
            $oldFileName = $user->avatar;
            File::delete('uploads/profiles/' . $oldFileName);

            $user->avatar = null;

        }

        if (isset($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        if (isset($request->role)) {
            $user->detachRoles($user->roles);

            $role = Role::where('name', $request->role)->get()->first();

            $user->roles()->attach($role);
        }

        $user = User::with('roles.permissions')->find($user->id);

        return response()->json($user, 200);

    }

}
