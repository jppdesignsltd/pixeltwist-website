<?php

namespace App\Http\Controllers;

use App\Tag;
use JWTAuth;
use Illuminate\Http\Request;

class TagController extends Controller
{

    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {

        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $tags = Tag::with('posts')->with('projects')->get();


        return response()->json($tags, 200);

    }

    public function store(Request $request)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $tag = new Tag();

        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:tags,slug',
        ]);

        $tag->title = $request->title;
        $tag->slug = $request->slug;

        $tag->save();

        $newTag = Tag::with('posts')->with('projects')->find($tag->id);


        return response()->json($newTag, 201);

    }

    public function show($id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $tag = Tag::with('posts.user')->with('posts.category')->with('projects.user')->find($id);

        if (!$tag) {
            return response()->json(['error' => 'Tag not found'], 404);
        }

        return response()->json($tag, 200);

    }

    public function update(Request $request, $id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $tag = Tag::find($id);

        if (!$tag) {
            return response()->json(['error' => 'Tag not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'slug' => "required|alpha_dash|min:1|max:255|unique:tags,slug, $id",
        ]);

        $tag->title = $request->title;
        $tag->slug = $request->slug;

        $tag->save();

        return response()->json($tag, 200);

    }

    public function destroy($id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $tag = Tag::find($id);

        if (!$tag) {
            return response()->json(['error' => 'Tag not found'], 404);
        }

        // We need to destroy the associated links.

        $tag->delete();

        return response()->json(['error' => 'Tag Deleted'], 200);

    }

}
