<?php

namespace App\Http\Controllers;

use App\Service;
use JWTAuth;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {

        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $services = Service::with('projects')->get();

        return response()->json($services, 200);
    }

    public function store(Request $request)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $service = new Service();

        if (!$service) {
            return response()->json(['error' => 'Service not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:services,slug',
        ]);

        $service->title = $request->title;
        $service->slug = $request->slug;

        $service->save();

        $newService = Service::with('projects')->find($service->id);


        return response()->json($newService, 201);

    }

    public function show($id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $service = Service::with('projects.user')->with('projects.tags')->find($id);

        if ($service) {
            return response()->json($service, 200);
        }

        return response()->json(['error' => 'Service not found'], 404);
    }

    public function update(Request $request, $id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $service = Service::find($id);

        if (!$service) {
            return response()->json(['error' => 'Service not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'slug' => "required|alpha_dash|min:1|max:255|unique:services,slug, $id",
        ]);

        $service->title = $request->title;
        $service->slug = $request->slug;

        $service->save();

        return response()->json($service, 200);

    }

    public function destroy($id)
    {
        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $service = Service::find($id);

        if (!$service) {
            return response()->json(['error' => 'Service not found'], 404);
        }

        // We need to destroy the associated links.
        $service->delete();

        return response()->json(['error' => 'Service Deleted'], 200);

    }
}
