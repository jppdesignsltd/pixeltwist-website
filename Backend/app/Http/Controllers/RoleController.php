<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use JWTAuth;

class RoleController extends Controller
{

	/**
	 * Instantiate a new PostController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->token = JWTAuth::getToken();
		if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
		$this->noPermission = ['error' => 'You do not have permission'];
	}

	public function index()
	{

		if (!$this->user->hasRole(['admin'])) {
			return response()->json($this->noPermission, 401);
		};

		$roles = Role::with('permissions')->get();

		$response = [
			'roles' => $roles
		];

		return response()->json($roles, 200);

	}

	public function store(Request $request)
	{

		if (!$this->user->hasRole(['admin'])) {
			return response()->json($this->noPermission, 401);
		};

		$role = new Role();

		$this->validate($request, [
			'name' => 'required',
			'display_name' => 'sometimes',
			'description' => 'sometimes',
			'permissions' => 'required'
		]);

		$role->name = $request->name;

		if (isset($request->display_name)) {
			$role->display_name = $request->display_name;
		}

		if (isset($request->description)) {
			$role->description = $request->description;
		}

		$role->save();

		if (isset($request->permissions)) {
			$role->permissions()->sync($request->permissions, false);
		} else {
			$role->permissions()->sync([]);
		}

		$role = Role::with('permissions')->find($role->id);

		return response()->json($role, 201);

	}

	public function update(Request $request, $id) {

		if (!$this->user->hasRole(['admin'])) {
			return response()->json($this->noPermission, 401);
		};

		$role = Role::find($id);

		$this->validate($request, [
			'name' => 'required',
			'display_name' => 'sometimes',
			'description' => 'sometimes',
			'permissions' => 'required'
		]);

		$role->name = $request->name;

		if (isset($request->display_name)) {
			$role->display_name = $request->display_name;
		}

		if (isset($request->description)) {
			$role->description = $request->description;
		}

		$role->save();

		if (isset($request->permissions)) {
			$role->permissions()->sync($request->permissions, true);
		} else {
			$role->permissions()->sync([]);
		}

		return response()->json($role, 201);

	}

}
