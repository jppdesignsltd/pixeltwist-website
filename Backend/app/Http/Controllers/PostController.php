<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use DateTime;
use Image;
use JWTAuth;
use Purifier;
use File;

class PostController extends Controller
{

    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = JWTAuth::getToken();
        if ($this->token) $this->user = JWTAuth::parseToken()->toUser();
        $this->noPermission = ['error' => 'You do not have permission'];
    }

    public function index()
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $posts = Post::with('category')->with('tags')->with('user')->get();

        return response()->json($posts, 200);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'sometimes',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:posts,slug',
            'featured_image' => 'sometimes',
            'status' => 'required',
            'user_id' => 'required',
            'category' => 'required',
            'live_date' => 'required',
            'main_content' => 'sometimes',
            'meta_keywords' => 'sometimes',
            'meta_description' => 'sometimes'
        ]);

        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $post = new Post();

        $post->title = $request->title;

        if (isset($request->subtitle)) {
            $post->subtitle = $request->subtitle;
        }

        if (isset($request->featured_image) && $request->featured_image !== null) {

            $image = $request->featured_image;

            $fileExt = explode(';', $image);
            $imageExt = explode('/', $fileExt[0]);

            $filename = time() . '_' . $this->user->id . '.' . $imageExt[1];

            $location = public_path('uploads/posts/' . $filename);

            Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);

            $post->featured_image = $filename;
        }

        if (isset($request->main_content)) {
            $post->main_content = Purifier::clean($request->main_content);
        }

        $post->slug = $request->slug;
        $post->status = $request->status;


        $date = new DateTime($request->live_date);
        $dateForTable = $date->format('Y-m-d');

//        dd($dateForTable);

        $post->live_date = $dateForTable;

        $post->user_id = $request->user_id;
        $post->category_id = $request->category;

        $post->meta_author = $request->user_id;

        if (isset($request->meta_keywords)) {
            $post->meta_keywords = $request->meta_keywords;
        }

        if (isset($request->meta_description)) {
            $post->meta_description = $request->meta_description;
        }

        $post->save();

        if (isset($request->tags)) {
            $post->tags()->sync($request->tags, false);
        } else {
            $post->tags()->sync([]);
        }

        return response()->json($post, 201);

    }

    public function show($id)
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $post = Post::with('tags')->with('user')->with('category')->find($id);

        if ($post) {
            return response()->json($post, 200);
        }

        return response()->json(['error' => 'Post not found'], 404);
    }

    public function update(Request $request, $id)
    {
        if (!$this->user->hasRole(['admin', 'editor', 'author'])) {
            return response()->json($this->noPermission, 401);
        };

        $post = Post::find($id);

        if (!$post) {
            return response()->json(['error' => 'Post not found'], 404);
        }

        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'sometimes',
            'slug' => "required|alpha_dash|min:1|max:255|unique:posts,slug,$id",
            'featured_image' => 'sometimes',
            'status' => 'required',
            'user_id' => 'required',
            'category_id' => 'required',
            'live_date' => 'required',
            'main_content' => 'sometimes',
            'meta_keywords' => 'sometimes',
            'meta_description' => 'sometimes'
        ]);

        $post->title = $request->title;

        $post->slug = $request->slug;

        if (isset($request->subtitle)) {
            $post->subtitle = $request->subtitle;
        }

        if ($request->featured_image === 'delete') {

            // Grab Old File Name and delete it
            $fileName = $post->featured_image;

            if ($fileName !== null) {
                File::delete('uploads/posts/' . $fileName);
            }

            $post->featured_image = null;

        } else if (isset($request->featured_image) && $request->featured_image !== $post->featured_image) {

            $image = $request->featured_image;
            // Grab Old File Name
            $oldFileName = $post->featured_image;

            if ($image !== $oldFileName) {
                $fileExt = explode(';', $image);
                $imageExt = explode('/', $fileExt[0]);

                $filename = $post->id . '_image_' . $this->user->id . '.' . $imageExt[1];

                $coverLocation = public_path('uploads/posts/' . $filename);

                Image::make($image)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($coverLocation);


                $post->featured_image = $filename;

                if ($oldFileName !== $filename) {
                    File::delete('uploads/posts/' . $oldFileName);
                }
            }
        }

        if (isset($request->main_content)) {
            $post->main_content = Purifier::clean($request->main_content);
        }

        if (isset($request->status)) {
            $post->status = $request->status;
        }

        if (isset($request->live_date)) {
            $date = new DateTime($request->live_date);
            $dateForTable = $date->format('Y-m-d');

            $post->live_date = $dateForTable;
        }

        if (isset($request->category_id)) {
            $post->category_id = $request->category_id;
        }

        if (isset($request->user_id)) {
            $post->user_id = $request->user_id;
            $post->meta_author = $request->user_id;
        }

        if (isset($request->meta_keywords)) {
            $post->meta_keywords = $request->meta_keywords;
        }

        if (isset($request->meta_description)) {
            $post->meta_description = $request->meta_description;
        }

        $post->save();


        if (isset($request->tags)) {
            $post->tags()->sync($request->tags, true);
        } else {
            $post->tags()->sync([]);
        }

        $post = Post::with('tags')->with('category')->with('user')->find($post->id);

        return response()->json($post, 200);

    }

    public function destroy($id)
    {

        if (!$this->user->hasRole(['admin', 'editor'])) {
            return response()->json($this->noPermission, 401);
        };

        $post = Post::find($id);

        // Get Featured Image Value and remove if preset
        if ($post->featured_image !== null) {
            File::delete('uploads/posts/' . $post->featured_image);
        }

        $post->delete();

        return response()->json(['error' => 'Post Deleted'], 200);

    }

}
