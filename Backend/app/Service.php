<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

	//	Servies belong to many projects
	public function projects() {
		return $this->belongsToMany('App\Project');
	}
}
