import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../shared/services/global.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html'
})

export class MyAccountComponent implements OnInit {


  constructor(private titleService: Title,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('My Account | ' + this.global.appTitle());
  }

}
