import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {ActivatedRoute, Data} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {Location} from '@angular/common';
import {User} from '../../../shared/interfaces/user.interface';

@Component({
  selector: 'app-show-project',
  templateUrl: './show-project.component.html',
  styles: []
})
export class ShowProjectComponent implements OnInit {

  id: number;
  author: User;
  project: Project;
  title: string;
  env_url: string;

  constructor(private activeRoute: ActivatedRoute,
              private titleService: Title,
              private _location: Location,
              private global: GlobalService) {
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.setTitle();
    this.getProject();
  }

  setTitle() {
    this.titleService.setTitle('Show Project | ' + this.global.appTitle());
  }

  getProject() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        this.project = data.json;
      }
    );
  }

}
