import {Component, ElementRef, OnInit, Renderer2, ViewEncapsulation} from '@angular/core';
import {SlugifyPipe} from 'ngx-pipes';
import {Select2OptionData} from 'ng2-select2';
import {ProjectService} from '../../../shared/services/project.service';
import {TagService} from '../../../shared/services/tag.service';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {NgForm} from '@angular/forms';
import {CanComponentDeactivate} from '../../../shared/interfaces/can-deactivate.interface';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';
import {User} from '../../../shared/interfaces/user.interface';
import {AuthorService} from '../../../shared/services/author.service';
import {AuthService} from '../../../shared/services/auth.service';
import {LoadingService} from '../../../shared/services/loading.service';
import {ServiceService} from '../../../shared/services/service.service';
import {Services} from '../../../shared/interfaces/service.interface';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: [
    '../../../../bower_components/select2/dist/css/select2.min.css'
  ],
  providers: [
    SlugifyPipe
  ],
  encapsulation: ViewEncapsulation.None
})
export class NewProjectComponent implements OnInit, CanComponentDeactivate {

  defaultStatus = 'published';
  role: string;
  adminRoles = ['admin', 'editor'];
  defaultAuthor;
  content;
  disabledSlug = true;
  tags: Tags[];
  services: Services[];
  authors: User[];
  title: string;
  changesSaved = false;
  logoImage = '';
  coverImage = '';
  coverHeight = '250px';
  selectedServices: string[] = [];
  public tagSelection = [];
  public tagsArray: Array<Select2OptionData>;
  public options: Select2Options;
  public current: string;

  constructor(private projectService: ProjectService,
              private tagService: TagService,
              private authService: AuthService,
              private authorService: AuthorService,
              private serviceService: ServiceService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private router: Router,
              private renderer: Renderer2,
              private slugPipe: SlugifyPipe,
              private el: ElementRef,
              private global: GlobalService,
              private titleService: Title) {
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.changesSaved) {
      return confirm('Are you sure you want to leave? All unsaved changes will be lost.');
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.role = this.authService.getRole();

    this.setTitle();
    this.getTags();
    this.getAuthors();
    this.getServices();
  }

  setTitle() {
    this.titleService.setTitle('Edit Post | ' + this.global.appTitle());
  }

  getAuthors() {
    this.authorService.getAuthors()
      .subscribe(
        (response) => {
          // console.log(response);

          this.authors = response.authors;
          this.defaultAuthor = response.currentUser.id;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getServices() {
    this.serviceService.getServices()
      .subscribe(
        (services: Services[]) => {
          this.services = services;
        },
        (error) => {
        }
      );
  }

  getTags() {
    this.tagService.getTags()
      .subscribe(
        (tags: Tags[]) => {
          if (tags) {
            this.tags = tags;
            this.select2Tags(tags);

            this.options = {
              width: '100%',
              multiple: true,
              theme: 'classic',
              closeOnSelect: true
            };
          }

          // console.log(this.tagsArray);
        },
        (error: Response) => {
          // console.log(error);
        }
      );
  }

  select2Tags(tags) {

    tags.forEach(function (v) {
      v.text = v.title;
      delete v.title;
      delete v.slug;
      delete v.posts;
      delete v.projects;

      // console.log(v);
    });

    this.tagsArray = tags;
  }

  select2Changed(data) {

    // console.log(data.value);

    if (data.value.length !== 0) {
      this.current = data.value.join('|');
      this.tagSelection = this.current.split('|');
    } else {
      this.tagSelection = [];
    }

    // console.log(data);
    // console.log(this.tagSelection);
  }

  logoChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e: any) {
        const preview = document.getElementById('logoPreview') as HTMLImageElement;
        const logoLabel = document.getElementById('logo_input-label') as HTMLElement;
        const remove_image = document.getElementById('remove_logo_image') as HTMLElement;

        preview.src = e.target.result;
        preview.classList.remove('d-none');
        remove_image.classList.remove('d-none');
        logoLabel.classList.add('d-none');
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeLogoImage() {
    const logo_image = document.getElementById('logo_image') as HTMLInputElement;
    const preview = document.getElementById('logoPreview') as HTMLImageElement;
    const logoLabel = document.getElementById('logo_input-label') as HTMLElement;
    const remove_image = document.getElementById('remove_logo_image') as HTMLElement;

    preview.src = '';
    this.renderer.addClass(preview, 'd-none');
    this.renderer.addClass(remove_image, 'd-none');
    this.renderer.removeClass(logoLabel, 'd-none');
    logo_image.value = '';
  }

  coverChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e: any) {
        const preview = document.getElementById('coverPreview') as HTMLImageElement;
        const coverLabel = document.getElementById('cover_input-label') as HTMLElement;
        const remove_image = document.getElementById('remove_cover_image') as HTMLElement;

        preview.src = e.target.result;
        preview.classList.remove('d-none');
        remove_image.classList.remove('d-none');
        coverLabel.classList.add('d-none');
      };

      reader.readAsDataURL(fileInput.target.files[0]);
      this.coverHeight = '350px';
    }
  }

  removeCoverImage() {
    const cover_image = document.getElementById('cover_image') as HTMLInputElement;
    const coverLabel = document.getElementById('cover_input-label') as HTMLImageElement;
    const preview = document.getElementById('coverPreview') as HTMLImageElement;
    const remove_image = document.getElementById('remove_cover_image') as HTMLElement;

    preview.src = '';
    this.renderer.addClass(preview, 'd-none');
    this.renderer.addClass(remove_image, 'd-none');
    this.renderer.removeClass(coverLabel, 'd-none');

    cover_image.value = '';
    this.coverHeight = '250px';
  }

  showInput(event, id: string) {
    const src = event.srcElement;
    const element = document.getElementById(id) as HTMLElement;
    const el_preview = document.getElementById(id + '_preview') as HTMLElement;

    element.classList.remove('d-none');
    el_preview.classList.add('d-none');

    if (element.id === 'slug') {
      this.disabledSlug = false;
    }
  }

  tinymceKeyup(content) {
    this.content = content;
    // console.log(content);
  }

  processLogoImage() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#logo_image');
    const imgPreview = document.getElementById('logoPreview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 600;
    const maxHeight = 424;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    file = inputEl.files.item(0);
    fileExt = file.name.split('.').pop();

    // console.log(fileExt);

    width = imgPreview.naturalWidth;
    height = imgPreview.naturalHeight;

    if (width > maxWidth) {
      ratio = maxWidth / width;
      width = width * ratio;
      height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      width = width * ratio;
      height = height * ratio;
    }

    canvas.width = width;
    canvas.height = height;

    ctx = canvas.getContext('2d');

    ctx.drawImage(imgPreview, 0, 0, width, height);

    if (fileExt === 'jpg' || fileExt === 'JPG' || fileExt === 'JPEG') {
      fileExt = 'jpeg';
      dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
    } else {
      // console.log(fileExt + ' was used');
      dataURL = canvas.toDataURL();
    }

    callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};
    // formData.logo_image = callback.url;

    return this.logoImage = callback.url;
  }

  processCoverImage() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#cover_image');
    const imgPreview = document.getElementById('coverPreview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 1280;
    const maxHeight = 700;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    file = inputEl.files.item(0);
    fileExt = file.name.split('.').pop();

    // console.log(fileExt);

    width = imgPreview.naturalWidth;
    height = imgPreview.naturalHeight;

    if (width > maxWidth) {
      ratio = maxWidth / width;
      width = width * ratio;
      height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      width = width * ratio;
      height = height * ratio;
    }

    canvas.width = width;
    canvas.height = height;

    ctx = canvas.getContext('2d');

    ctx.drawImage(imgPreview, 0, 0, width, height);

    if (fileExt === 'jpg' || fileExt === 'JPG' || fileExt === 'JPEG') {
      fileExt = 'jpeg';
      dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
    } else {
      // console.log(fileExt + ' was used');
      dataURL = canvas.toDataURL();
    }

    callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};
    // formData.cover_image = callback.url;

    return this.coverImage = callback.url;
  }

  onCheckboxChange(event, val) {
    const el = event.target;
    const index = this.selectedServices.indexOf(val.toString());

    if (index > -1) {
      this.selectedServices.splice(index, 1);
    } else {
      this.selectedServices.push(val.toString());
    }

    // console.log(this.selectedServices);
  }

  onSubmit(form: NgForm) {
    const formData = form.value;
    const inputLogoEl: HTMLInputElement = this.el.nativeElement.querySelector('#logo_image');
    const logoFileCount: number = inputLogoEl.files.length;
    const inputCoverEl: HTMLInputElement = this.el.nativeElement.querySelector('#cover_image');
    const coverFileCount: number = inputCoverEl.files.length;

    // Reset Errors
    this.errorService.resetErrors();
    this.loadingService.showLoading();

    if (this.selectedServices.length === 0) {
      this.loadingService.hideLoading();
      return this.errorService.showErrorHandler('Please select services for this project.');
    }

    // console.log(formData.mydate);

    formData.main_content = this.content;
    formData.slug = this.slugPipe.transform(form.value.title);

    if (this.tagSelection.length > 0) {
      formData.tags = this.tagSelection;
    }

    if (this.selectedServices.length > 0) {
      formData.services = this.selectedServices;
    }

    if (logoFileCount > 0) {
      this.processLogoImage();
      formData.logo_image = this.logoImage;
    }

    if (coverFileCount > 0) {
      this.processCoverImage();
      formData.cover_image = this.coverImage;
    }

    // console.log(formData);
    delete formData.mydate;
    // console.log(JSON.stringify(formData));

    this.projectService.createProject(formData)
      .subscribe(
        () => {
          // alert('Project Created');
          this.changesSaved = true;
          this.router.navigateByUrl('/admin/projects');
          this.loadingService.hideLoading();
        },
        (error) => {

          // console.log(error);

          this.errorService.errorHandler(error);
          this.loadingService.hideLoading();
        }
      );
    // // form.reset();
    //
  }

}
