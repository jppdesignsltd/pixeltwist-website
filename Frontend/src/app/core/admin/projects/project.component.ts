import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectService} from '../../../shared/services/project.service';
import {Project} from '../../../shared/interfaces/project.interface';
import {ErrorService} from '../../../shared/services/error.service';
import {AuthService} from '../../../shared/services/auth.service';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: '[app-project]',
  templateUrl: './project.component.html',
})
export class ProjectComponent implements OnInit {

  @Input() project: Project;
  @Input() viewBlock: string;
  @Output() projectDeleted = new EventEmitter<Project>();
  coverSrc: string;
  coverHeight = '150px';
  hasCoverImage = false;
  logoSrc: string;
  role: string;
  isAuthor = false;

  constructor(private projectService: ProjectService,
              private authService: AuthService,
              private global: GlobalService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
    this.role = this.authService.getRole();

    if (this.project.cover_image) {
      this.coverSrc = this.global.getUploadsUrl() + 'uploads/projects/' + this.project.cover_image;
      this.coverHeight = '150px';
      this.hasCoverImage = true;
    } else {
      this.hasCoverImage = false;
      this.coverHeight = '150px';
    }

    this.logoSrc = this.global.getUploadsUrl() + 'uploads/projects/' + this.project.logo_image;

    if (this.role === 'author') {
      this.isAuthor = true;
    }
  }

  onDelete() {
    this.errorService.resetErrors();

    this.projectService.deleteProject(this.project.id)
      .subscribe(
        () => {
          return this.projectDeleted.emit(this.project);
        },
        (error) => {
          console.log(error);
          return this.errorService.errorHandler(error);
        }
      );
  }

}
