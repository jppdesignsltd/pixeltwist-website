import {Component, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {ProjectService} from '../../../shared/services/project.service';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class MyProjectsComponent implements OnInit {

  projects: Project[];
  page: any;

  constructor(private projectService: ProjectService,
              private errorService: ErrorService,
              private global: GlobalService,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getProjects();
  }

  setTitle() {
    this.titleService.setTitle('All Projects | ' + this.global.appTitle());
  }

  getProjects() {
    // Reset Errors
    this.errorService.resetErrors();

    this.projectService.getAuthorProjects()
      .subscribe(
        (projects: Project[]) => {
          // console.log(projects);
          if (projects) {
            this.projects = projects.slice().reverse();
          }
        },
        (error: Response) => {
          // console.log(error);
          this.errorService.errorHandler(error);
        }
      );
  }

}
