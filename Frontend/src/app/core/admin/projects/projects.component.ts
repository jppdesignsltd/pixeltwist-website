import {Component, OnInit, Renderer2} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {ProjectService} from '../../../shared/services/project.service';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ProjectsComponent implements OnInit {

  projects: Project[];
  viewBlock = 'block';
  projectsCount;
  page: any;

  constructor(private projectService: ProjectService,
              private errorService: ErrorService,
              private global: GlobalService,
              private renderer: Renderer2,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getProjects();
  }

  changeView(el) {
    if (this.viewBlock === 'table') {
      this.renderer.removeClass(el, 'fa-toggle-off');
      this.renderer.addClass(el, 'fa-toggle-on');
      return this.viewBlock = 'block';
    }

    this.renderer.removeClass(el, 'fa-toggle-on');
    this.renderer.addClass(el, 'fa-toggle-off');
    return this.viewBlock = 'table';
  }

  setTitle() {
    this.titleService.setTitle('All Projects | ' + this.global.appTitle());
  }

  getProjects() {
    this.projectService.getProjects()
      .subscribe(
        (projects: Project[]) => {
          // console.log(projects);
          if (projects) {
            this.projects = projects.slice().reverse();
            this.projectsCount = this.projects.length;
          }
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  onDeleted(project: Project) {
    this.errorService.resetErrors();

    const position = this.projects.findIndex(
      (projectEl: Project) => {

        // console.log(postEl);
        return projectEl.id === project.id;
      }
    );

    this.projects.splice(position, 1);

    this.errorService.deleteHandler('Project deleted!', true);
  }

}
