import {
  Component, ElementRef, EventEmitter, OnInit, Output, Renderer2,
  ViewEncapsulation
} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {SlugifyPipe} from 'ngx-pipes';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {Select2OptionData} from 'ng2-select2';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {ProjectService} from '../../../shared/services/project.service';
import {TagService} from '../../../shared/services/tag.service';
import {Title} from '@angular/platform-browser';
import {NgForm} from '@angular/forms';
import {GlobalService} from '../../../shared/services/global.service';
import {Location} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {CanComponentDeactivate} from '../../../shared/interfaces/can-deactivate.interface';
import {ErrorService} from '../../../shared/services/error.service';
import {User} from '../../../shared/interfaces/user.interface';
import {AuthorService} from '../../../shared/services/author.service';
import {LoadingService} from '../../../shared/services/loading.service';
import {AuthService} from '../../../shared/services/auth.service';
import {Services} from '../../../shared/interfaces/service.interface';
import {ServiceService} from '../../../shared/services/service.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: [
    '../../../../bower_components/select2/dist/css/select2.min.css'
  ],
  providers: [
    SlugifyPipe
  ],
  encapsulation: ViewEncapsulation.None
})
export class EditProjectComponent implements OnInit, CanComponentDeactivate {

  @Output() onEditorKeyup = new EventEmitter<string>();
  env_url: string;
  id: number;
  project: Project;
  image: string;
  content;
  authors: User[];
  adminRoles = ['admin', 'editor'];
  defaultAuthor: User;
  defaultStatus = 'published';
  disabledStatus = true;
  disabledAuthor = true;
  tags: Tags[];
  services: Services[];
  changesSaved = false;
  logoImage: string;
  logoImageSrc = '';
  coverImage: string;
  coverImageSrc = '';
  coverHeight: string;
  role;
  selectedServices: string[] = [];
  projectServices = [];
  public tagsChanged = false;
  public showLogoImage = false;
  public showCoverImage = false;
  public projectTags;
  public tagSelection: string[] = [];
  public tagsArray: Array<Select2OptionData>;
  public options: Select2Options;
  public value: string[] = [];
  public current: string;

  constructor(private activeRoute: ActivatedRoute,
              private projectService: ProjectService,
              private tagService: TagService,
              private loadingService: LoadingService,
              private errorService: ErrorService,
              private renderer: Renderer2,
              private serviceService: ServiceService,
              private authService: AuthService,
              private router: Router,
              private authorService: AuthorService,
              private el: ElementRef,
              private _location: Location,
              private global: GlobalService,
              private titleService: Title) {
  }

  backClicked() {
    this._location.back();
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.changesSaved) {
      return confirm('Are you sure you want to leave? All unsaved changes will be lost.');
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.role = this.authService.getRole();
    this.setTitle();
    this.getTags();
    this.getAuthors();
    // this.getServices();

    this.options = {
      width: '100%',
      multiple: true,
      theme: 'classic',
      closeOnSelect: true
    };
  }

  setTitle() {
    this.titleService.setTitle('Edit Project | ' + this.global.appTitle());
  }

  getAuthors() {
    this.authorService.getAuthors()
      .subscribe(
        (response) => {
          // console.log(response);
          this.authors = response.authors;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getServices() {
    this.serviceService.getServices()
      .subscribe(
        (services: Services[]) => {
          this.services = services;

          setTimeout(() => {
            this.servicesChecked();
          }, 1);

          // console.log(this.services);
        },
        (error) => {
        }
      );
  }

  servicesChecked() {
    for (let i = 0, l = this.projectServices.length; i < l; i++) {
      const checkboxName = 'checkbox_' + this.projectServices[i].id;
      const checkbox = document.getElementById(checkboxName) as HTMLInputElement;

      // Set checked to true for all found checkboxes
      checkbox.checked = true;
      checkbox.setAttribute('checked', 'checked');

      this.selectedServices.push(this.projectServices[i].id.toString());
    }
  }

  getProject() {
    this.activeRoute.data.subscribe(
      (data: Data) => {

        // console.log(data.json);
        this.project = data.json;
        this.projectServices = this.project.services;

        if (!this.project) {
          return this.router.navigateByUrl('/admin');
        }

        this.image = this.project.logo_image;
        this.projectTags = this.project.tags;
        this.id = this.project.id;
        this.defaultAuthor = this.project.user.id;

        for (let i = 0; i < this.projectTags.length; i++) {
          this.value.push(this.projectTags[i].id);
        }

        this.current = this.value.join(' | ');

        if (this.project.logo_image !== '') {
          this.showLogoImage = true;
          this.logoImageSrc = this.env_url + 'uploads/projects/' + this.project.logo_image;

          if (this.project.logo_image === null) {
            this.showLogoImage = false;
          }
        }

        if (this.project.cover_image !== '') {
          this.showCoverImage = true;
          this.coverImageSrc = this.env_url + 'uploads/projects/' + this.project.cover_image;
          this.coverHeight = '350px';

          if (this.project.cover_image === null) {
            this.showCoverImage = false;
          }
        } else {
          this.showCoverImage = false;
          this.coverHeight = '250px';
        }

        setTimeout(() => {
          this.getServices();
        }, 1);

        // console.log(this.value);
        // console.log(this.current);

      },
      (error) => {
        console.log(error);
      }
    );
  }

  getTags() {
    this.tagService.getTags()
      .subscribe(
        (tags: Tags[]) => {
          if (tags) {
            this.tags = tags;
            this.select2Tags(tags);
          }

          // console.log(this.tagsArray);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  select2Tags(tags) {

    tags.forEach(function (v) {
      v.text = v.title;
      delete v.title;
      delete v.slug;
      delete v.posts;
      delete v.projects;

      // console.log(v);
    });

    this.tagsArray = tags;

    // console.log(tags);

    // Once we have all the tags fire the getProject funciton in order to load the function and its tags.
    this.getProject();
  }

  select2Changed(data) {

    // console.log(data.value.length);

    if (data.value !== null) {
      this.current = data.value.join('|');

      // console.log(data.value);

      if (data.value.length > 0) {
        this.tagSelection = this.current.split('|');
      }

      if (data.value.length === 0) {
        this.tagSelection = [];
      }

      this.tagsChanged = true;
    }

    // console.log(this.tagSelection);

  }

  logoChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e: any) {
        const preview = document.getElementById('logoPreview') as HTMLImageElement;
        const remove_image = document.getElementById('remove_logo_image') as HTMLElement;

        preview.src = e.target.result;
        remove_image.classList.remove('d-none');

      };

      this.showLogoImage = true;

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeLogoImage() {
    const logo_image = document.getElementById('logo_image') as HTMLInputElement;
    const remove_image = document.getElementById('remove_logo_image') as HTMLElement;

    logo_image.value = '';
    this.renderer.addClass(remove_image, 'd-none');
    this.logoImage = 'delete';
    this.logoImageSrc = '';

    setTimeout(() => {
      this.showLogoImage = false;
    }, 1);

    // console.log(this.logoImage);
  }

  coverChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      this.showCoverImage = true;

      setTimeout(() => {
        reader.onload = function (e: any) {
          const preview = document.getElementById('coverPreview') as HTMLImageElement;
          const remove_image = document.getElementById('remove_cover_image') as HTMLElement;

          preview.src = e.target.result;
          remove_image.classList.remove('d-none');

        };
      }, 1);

      this.coverHeight = '350px';

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeCoverImage() {
    console.log('Remove Cover Image');

    const cover_image = document.getElementById('cover_image') as HTMLInputElement;
    const remove_image = document.getElementById('remove_cover_image') as HTMLElement;

    cover_image.value = '';
    this.renderer.addClass(remove_image, 'd-none');
    this.coverImage = 'delete';
    this.coverImageSrc = '';
    this.coverHeight = '250px';

    setTimeout(() => {
      this.showCoverImage = false;
    }, 1);
  }

  showInput(event, id: string) {
    const src = event.srcElement;
    const element = document.getElementById(id) as HTMLElement;
    const el_preview = document.getElementById(id + '_preview') as HTMLElement;

    this.renderer.addClass(src, 'd-none');
    this.renderer.addClass(el_preview, 'd-none');
    this.renderer.removeClass(element, 'd-none');

    if (element.id === 'status') {
      this.disabledStatus = false;
    }

    if (element.id === 'user_id') {
      this.disabledAuthor = false;
    }
  }

  tinymceKeyup(content) {
    this.content = content;

    // console.log(content);
  }

  processLogoImage() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#logo_image');
    const imgPreview = document.getElementById('logoPreview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 600;
    const maxHeight = 424;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    file = inputEl.files.item(0);
    fileExt = file.name.split('.').pop();

    // console.log(fileExt);

    width = imgPreview.naturalWidth;
    height = imgPreview.naturalHeight;

    if (width > maxWidth) {
      ratio = maxWidth / width;
      width = width * ratio;
      height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      width = width * ratio;
      height = height * ratio;
    }

    canvas.width = width;
    canvas.height = height;

    ctx = canvas.getContext('2d');

    ctx.drawImage(imgPreview, 0, 0, width, height);

    if (fileExt === 'jpg') {
      fileExt = 'jpeg';
      dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
    } else {
      // console.log(fileExt + ' was used');
      dataURL = canvas.toDataURL();
    }

    callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};
    // formData.logo_image = callback.url;

    return this.logoImage = callback.url;
  }

  processCoverImage() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#cover_image');
    const imgPreview = document.getElementById('coverPreview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 1280;
    const maxHeight = 700;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    file = inputEl.files.item(0);
    fileExt = file.name.split('.').pop();

    // console.log(fileExt);

    width = imgPreview.naturalWidth;
    height = imgPreview.naturalHeight;

    if (width > maxWidth) {
      ratio = maxWidth / width;
      width = width * ratio;
      height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      width = width * ratio;
      height = height * ratio;
    }

    canvas.width = width;
    canvas.height = height;

    ctx = canvas.getContext('2d');

    ctx.drawImage(imgPreview, 0, 0, width, height);

    if (fileExt === 'jpg') {
      fileExt = 'jpeg';
      dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
    } else {
      // console.log(fileExt + ' was used');
      dataURL = canvas.toDataURL();
    }

    callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};
    // formData.cover_image = callback.url;

    return this.coverImage = callback.url;
  }

  onCheckboxChange(event, val) {
    const el = event.target;
    const index = this.selectedServices.indexOf(val.toString());

    if (index > -1) {
      this.selectedServices.splice(index, 1);
    } else {
      this.selectedServices.push(val.toString());
    }

    // console.log(this.selectedServices);
  }

  onUpdate(form: NgForm) {
    const formData = form.value;
    const inputLogoEl: HTMLInputElement = this.el.nativeElement.querySelector('#logo_image');
    const logoFileCount: number = inputLogoEl.files.length;
    const inputCoverEl: HTMLInputElement = this.el.nativeElement.querySelector('#cover_image');
    const coverFileCount: number = inputCoverEl.files.length;

    // Reset Error Messages
    this.errorService.resetErrors();
    this.loadingService.showLoading();

    if (this.selectedServices.length === 0) {
      this.loadingService.hideLoading();
      return this.errorService.showErrorHandler('Please select services for this project.');
    }

    formData.main_content = this.content;

    if (this.tagsChanged) {
      formData.tags = this.tagSelection;
      // console.log('tags have changed');
    } else {
      formData.tags = this.value;
      // console.log('tags the same');
    }

    if (this.selectedServices.length > 0) {
      formData.services = this.selectedServices;
    }

    if (this.logoImage !== 'delete' || this.logoImage !== null) {
      if (this.logoImage !== this.project.logo_image) {
        formData.logo_image = this.logoImage;
      }
    }

    if (this.coverImage !== 'delete' || this.coverImage !== null) {
      if (this.coverImage !== this.project.cover_image) {
        formData.cover_image = this.coverImage;
      }
    }

    // NOT CURRENTLY USED //
    // if (this.logoImage === 'delete' || this.logoImage === null) {
    //   // formData.logo_image = null;
    // }

    if (logoFileCount > 0) {
      this.processLogoImage();
      formData.logo_image = this.logoImage;
    }

    if (coverFileCount > 0) {
      this.processCoverImage();
      formData.cover_image = this.coverImage;
    }

    // console.log(formData);
    // console.log(JSON.stringify(formData));

    this.projectService.updateProject(this.id, formData)
      .subscribe(
        (response) => {
          // console.log(response);
          this.changesSaved = true;
          this.errorService.successHandler('Project Updated');
          this.loadingService.hideLoading();

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);
          this.loadingService.hideLoading();

        }
      );
  }

}
