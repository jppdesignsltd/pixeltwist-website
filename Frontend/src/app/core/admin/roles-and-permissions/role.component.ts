import {Component, Input, OnInit} from '@angular/core';
import {Roles} from '../../../shared/interfaces/role.interface';
import {ErrorService} from 'app/shared/services/error.service';
import {RoleService} from '../../../shared/services/role.service';
import {PermissionService} from '../../../shared/services/permission.service';

@Component({
  selector: '[app-role]',
  templateUrl: './role.component.html',
})
export class RoleComponent implements OnInit {

  @Input() role: Roles;
  editing = false;
  allPermissions;
  rolePermissions;
  selectedPermissions = [];

  constructor(private errorService: ErrorService,
              private roleService: RoleService,
              private permissionService: PermissionService) {
  }

  ngOnInit() {
    this.rolePermissions = this.role.permissions;

    this.getPermissions();
  }

  getPermissions() {
    this.errorService.resetErrors();

    this.permissionService.getPermissions()
      .subscribe(
        (response) => {
          this.allPermissions = response;

          setTimeout(() => {
            this.permissionsChecked();
          }, 1);

          // console.log(this.allPermissions);
        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

  permissionsChecked() {
    for (let i = 0, l = this.rolePermissions.length; i < l; i++) {
      const checkboxName = 'checkbox_' + this.rolePermissions[i].name + '_' + this.role.id;
      const checkbox = document.getElementById(checkboxName) as HTMLInputElement;

      // Set checked to true for all found checkboxes
      checkbox.checked = true;

      this.selectedPermissions.push(this.rolePermissions[i].id.toString());
    }

    // console.log(this.selectedPermissions);
  }

  onEdit() {
    this.editing = true;

    const container = document.getElementById(this.role.id + '_checkboxCont');
    const checkboxes = container.querySelectorAll('input[type=checkbox]');

    for (let i = 0, l = checkboxes.length; i < l; i++) {
      const checkbox = checkboxes[i];
      checkbox.removeAttribute('disabled');
    }

    // console.log(checkboxes);
  }

  onCheckboxChange(event, val) {
    const el = event.target;
    const index = this.selectedPermissions.indexOf(val.toString());

    if (index > -1) {
      this.selectedPermissions.splice(index, 1);
    } else {
      this.selectedPermissions.push(val.toString());
    }

    // console.log(this.selectedPermissions);
  }

  onUpdate() {
    this.errorService.resetErrors();

    const formData = {
      name: this.role.name,
      display_name: this.role.display_name,
      permissions: this.selectedPermissions
    };

    // console.log(formData);

    this.roleService.updateRole(this.role.id, formData)
      .subscribe(
        () => {
          const container = document.getElementById(this.role.id + '_checkboxCont');
          const checkboxes = container.querySelectorAll('input[type=checkbox]');

          for (let i = 0, l = checkboxes.length; i < l; i++) {
            const checkbox = checkboxes[i];
            checkbox.setAttribute('disabled', 'disabled');
          }

          this.errorService.successHandler('Role Updated!', true);
          this.editing = false;
        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }
}
