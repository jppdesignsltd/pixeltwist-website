import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ErrorService} from '../../../shared/services/error.service';
import {RoleService} from '../../../shared/services/role.service';
import {GlobalService} from '../../../shared/services/global.service';
import {SlugifyPipe} from 'ngx-pipes';
import {PermissionService} from '../../../shared/services/permission.service';
import {Permission} from '../../../shared/interfaces/permission.interface';
import {Roles} from '../../../shared/interfaces/role.interface';
import {RolesAndPermissionsComponent} from './roles-and-permissions.component';

@Component({
  selector: 'app-new-role',
  templateUrl: './new-role.component.html',
  providers: [
    SlugifyPipe
  ]
})
export class NewRoleComponent implements OnInit {

  display_name = '';
  permissions: Permission[];
  persmissionArray = [];

  constructor(private errorService: ErrorService,
              private slugPipe: SlugifyPipe,
              private roleComponent: RolesAndPermissionsComponent,
              private permissionService: PermissionService,
              private roleService: RoleService,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.getPermissions();
  }

  getPermissions() {
    this.permissionService.getPermissions()
      .subscribe(
        (permissions: Permission[]) => {

          this.permissions = permissions;
          // console.log(this.permissions);

        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

  onCheckboxChange(event, val) {
    const el = event.target;
    const index = this.persmissionArray.indexOf(val.toString());

    if (index > -1) {
      this.persmissionArray.splice(index, 1);
    } else {
      this.persmissionArray.push(val.toString());
    }

    // console.log(this.persmissionArray);
  }

  onSubmit(form: NgForm) {
    // Reset Errors
    this.errorService.resetErrors();

    const formData = form.value;
    formData.name = this.slugPipe.transform(this.display_name);

    if (this.persmissionArray.length > 0) {
      formData.permissions = this.persmissionArray;
    } else {
      return this.errorService.showErrorHandler('Please select permissions');
    }

    // console.log(formData);
    // console.log(JSON.stringify(formData));

    this.roleService.createRole(formData)
      .subscribe(
        (response) => {
          // console.log(response);
          this.roleComponent.onRoleAdded(response.role);
          this.errorService.successHandler('Role created');
          form.reset();
        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

}
