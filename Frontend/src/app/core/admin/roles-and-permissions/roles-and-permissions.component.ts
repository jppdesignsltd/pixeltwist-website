import {Component, OnInit} from '@angular/core';
import {RoleService} from '../../../shared/services/role.service';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';
import {Roles} from '../../../shared/interfaces/role.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {PermissionService} from '../../../shared/services/permission.service';
import {Permission} from '../../../shared/interfaces/permission.interface';

@Component({
  selector: 'app-roles-and-permissions',
  templateUrl: './roles-and-permissions.component.html',
  animations: [
    listAnimationTrigger
  ]
})

export class RolesAndPermissionsComponent implements OnInit {

  roles: Roles[];
  permissions: Permission[];
  public isCollapsedRole = true;
  public isCollapsedPerm = true;

  constructor(private roleService: RoleService,
              private permissionService: PermissionService,
              private global: GlobalService,
              private errorService: ErrorService,
              private title: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getRoles();
    this.getPermissions();
  }

  setTitle() {
    this.title.setTitle('Roles and Permissions | ' + this.global.appTitle());
  }

  getRoles() {
    this.roleService.getRoles()
      .subscribe(
        (roles: Roles[]) => {

          this.roles = roles.slice().reverse();
          // console.log(this.roles);
        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

  getPermissions() {
    this.permissionService.getPermissions()
      .subscribe(
        (permissions: Permission[]) => {

          this.permissions = permissions;
          // console.log(this.permissions);

        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

  onRoleAdded(newRole: Roles) {
    this.roles.unshift(newRole);
  }

}
