import {Component, OnInit} from '@angular/core';
import {User} from '../../../shared/interfaces/user.interface';
import {AuthService} from 'app/shared/services/auth.service';
import {Title} from '@angular/platform-browser';
import {UserService} from 'app/shared/services/user.service';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styles: []
})
export class UserProfileComponent implements OnInit {

  token;
  user: User;
  env_url: string;

  constructor(private titleService: Title,
              private authService: AuthService,
              private userService: UserService,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.token = this.authService.getToken();

    // Set the Title of the page
    this.setTitle();

    if (this.token) {
      this.getUser();
    }
  }

  setTitle() {
    this.titleService.setTitle('My Profile | ' + this.global.appTitle());
  }

  getUser() {
    this.userService.getUser()
      .subscribe(
        (user: User) => {
          this.user = user;
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
