import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../shared/services/global.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})

export class AdminComponent implements OnInit {

  validRoles = ['admin', 'editor', 'author'];

  constructor(private titleService: Title,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('Admin Panel | ' + this.global.appTitle());
  }

}
