import {Component, OnInit} from '@angular/core';
import {SlugifyPipe} from 'ngx-pipes';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {TagService} from '../../../shared/services/tag.service';
import {NgForm} from '@angular/forms';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  providers: [
    SlugifyPipe
  ],
  animations: [
    listAnimationTrigger
  ]
})

export class TagsComponent implements OnInit {

  tags: Tags[];
  title: string;
  page: any;

  constructor(private tagService: TagService,
              private titleService: Title,
              private errorService: ErrorService,
              private global: GlobalService,
              private slugPipe: SlugifyPipe) {
  }

  ngOnInit() {
    this.setTitle();
    this.getTags();
  }

  setTitle() {
    this.titleService.setTitle('Edit Post | ' + this.global.appTitle());
  }

  getTags() {
    this.tagService.getTags()
      .subscribe(
        (tags: Tags[]) => {
          if (tags) {
            this.tags = tags.slice().reverse();
          }
          // console.log(tags);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  onAdded(newTag: Tags) {
    this.errorService.resetErrors();
    this.tags.unshift(newTag);
    this.errorService.successHandler('Tag successfully created');
  }

  onDeleted(tag: Tags) {
    this.errorService.resetErrors();

    const position = this.tags.findIndex(
      (catEl: Tags) => {
        return catEl.id === tag.id;
      }
    );
    this.tags.splice(position, 1);

    this.errorService.deleteHandler('Tag successfully deleted!');
  }

}
