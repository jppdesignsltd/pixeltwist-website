import {Component, OnInit} from '@angular/core';
import {SlugifyPipe} from 'ngx-pipes';
import {TagService} from '../../../shared/services/tag.service';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';
import {NgForm} from '@angular/forms';
import {TagsComponent} from './tags.component';

@Component({
  selector: 'app-new-tag',
  templateUrl: './new-tag.component.html',
  providers: [
    SlugifyPipe
  ],
  animations: [
    listAnimationTrigger
  ]
})

export class NewTagComponent implements OnInit {

  title: string;

  constructor(private tagService: TagService,
              private errorService: ErrorService,
              private tagsComponent: TagsComponent,
              private slugPipe: SlugifyPipe) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    const formData = form.value;

    formData.slug = this.slugPipe.transform(form.value.title);
    this.errorService.resetErrors();

    // console.log(formData);

    this.tagService.createTag(formData)
      .subscribe(
        (tag) => {

          this.tagsComponent.onAdded(tag);
          form.reset();

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);

        }
      );
  }
}
