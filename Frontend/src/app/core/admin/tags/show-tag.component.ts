import {Component, OnInit} from '@angular/core';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {Post} from '../../../shared/interfaces/post.interface';
import {Project} from '../../../shared/interfaces/project.interface';
import {ActivatedRoute, Data} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Location} from '@angular/common';

@Component({
  selector: 'app-show-tag',
  templateUrl: './show-tag.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowTagComponent implements OnInit {

  id: number;
  tag: Tags;
  posts: Post[];
  projects: Project[];
  env_url: string;
  postPage: any;
  projectPage: any;

  constructor(private activeRoute: ActivatedRoute,
              private titleService: Title,
              private _location: Location,
              private global: GlobalService) {
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.setTitle();
    this.getTag();
  }

  setTitle() {
    this.titleService.setTitle('Show Tag Posts & Projects | ' + this.global.appTitle());
  }

  getTag() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);

        this.tag = data.json;
        this.posts = data.json.posts;
        this.projects = data.json.projects;
      }
    );
  }

}
