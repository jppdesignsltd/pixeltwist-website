import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {TagService} from '../../../shared/services/tag.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: '[app-tag]',
  templateUrl: './tag.component.html'
})
export class TagComponent implements OnInit {

  @Input() tag: Tags;
  @Output() tagDeleted = new EventEmitter<Tags>();
  editing = false;
  editName = '';
  editSlug = '';

  constructor(private tagService: TagService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
  }

  onEdit() {
    this.editing = true;
    this.editName = this.tag.title;
    this.editSlug = this.tag.slug;
  }

  onUpdate() {
    this.errorService.resetErrors();

    this.tagService.updateTag(this.tag.id, this.editName, this.editSlug)
      .subscribe(
        (tag: Tags) => {
          // console.log(this.editName);

          this.tag.title = this.editName;
          this.tag.slug = this.editSlug;
          this.editName = '';
          this.editSlug = '';
          this.errorService.successHandler('Tag Updated!', true);
        }
      );

    this.editing = false;
  }

  onCancel() {
    this.editName = '';
    this.editSlug = '';
    this.editing = false;
  }

  onDelete() {
    this.errorService.resetErrors();

    this.tagService.deleteTag(this.tag.id)
      .subscribe(
        () => {
          this.tagDeleted.emit(this.tag);
        },
        (error) => {

          this.errorService.errorHandler(error);

        }
      );
  }

}
