import {Component, Input, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';

@Component({
  selector: 'app-show-tag-projects',
  templateUrl: './show-tag-projects.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowTagProjectsComponent implements OnInit {

  @Input() projects: Project[];
  projectPage: any;
  constructor() { }

  ngOnInit() {
  }

}
