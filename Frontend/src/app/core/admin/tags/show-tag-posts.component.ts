import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';

@Component({
  selector: 'app-show-tag-posts',
  templateUrl: './show-tag-posts.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowTagPostsComponent implements OnInit {

  @Input() posts: Post[];
  postPage: any;
  constructor() { }

  ngOnInit() {
  }

}
