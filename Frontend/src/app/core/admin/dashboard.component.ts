import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../shared/services/auth.service';
import {UserService} from '../../shared/services/user.service';
import {GlobalService} from '../../shared/services/global.service';
import {User} from '../../shared/interfaces/user.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  user: User;
  userImage = '/dist/img/default_user.png';
  env_url: string;
  token;

  constructor(private titleService: Title,
              private authService: AuthService,
              private userService: UserService,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.token = this.authService.getToken();

    // Set the Title of the page
    this.setTitle();

    if (this.token) {
      this.getUser();
    }
  }

  setTitle() {
    this.titleService.setTitle('Dashboard | ' + this.global.appTitle());
  }

  getUser() {
    this.userService.getUser()
      .subscribe(
        (user: User) => {
          this.user = user;

          if (this.user.avatar) {
            this.userImage = this.global.getUploadsUrl() + '/uploads/profiles/' + this.user.avatar;
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
