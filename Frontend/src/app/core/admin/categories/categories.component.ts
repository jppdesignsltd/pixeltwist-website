import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../../shared/services/category.service';
import {Category} from '../../../shared/interfaces/category.interface';
import {NgForm} from '@angular/forms';
import {SlugifyPipe} from 'ngx-pipes';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  providers: [
    SlugifyPipe
  ],
  animations: [
    listAnimationTrigger
  ]
})
export class CategoriesComponent implements OnInit {

  categories: Category[];
  title: string;
  page: any;

  constructor(private categoryService: CategoryService,
              private errorService: ErrorService,
              private slugPipe: SlugifyPipe,
              private global: GlobalService,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getCategories();
  }

  setTitle() {
    this.titleService.setTitle('All Categories | ' + this.global.appTitle());
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(
        (categories: Category[]) => {
          if (categories) {
            this.categories = categories.slice().reverse();
          }
          // console.log(categories);
        },
        (error: Response) => {
          // console.log(error);
        }
      );
  }

  onAdded(cat: Category) {
    this.categories.unshift(cat);
  }

  onDeleted(category: Category) {
    this.errorService.resetErrors();

    const position = this.categories.findIndex(
      (catEl: Category) => {
        return catEl.id === category.id;
      }
    );

    this.categories.splice(position, 1);
    this.errorService.deleteHandler('Category successfully deleted!', true);
  }

  onSubmit(form: NgForm) {
    const formData = form.value;

    this.errorService.resetErrors();

    formData.slug = this.slugPipe.transform(form.value.title);

    this.categoryService.createCategory(formData)
      .subscribe(
        (response) => {
          this.onAdded(response.category);
          this.errorService.successHandler('Category successfully created!', true);
          form.reset();
        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);

        }
      );
  }

}
