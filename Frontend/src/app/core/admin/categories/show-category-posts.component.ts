import {Component, Input, OnInit} from '@angular/core';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Post} from '../../../shared/interfaces/post.interface';

@Component({
  selector: 'app-show-category-posts',
  templateUrl: './show-category-posts.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowCategoryPostsComponent implements OnInit {

  @Input() posts: Post[];
  constructor() { }

  ngOnInit() {
  }

}
