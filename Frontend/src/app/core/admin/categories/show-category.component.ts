import {Component, OnInit} from '@angular/core';
import {Category} from '../../../shared/interfaces/category.interface';
import {Post} from '../../../shared/interfaces/post.interface';
import {ActivatedRoute, Data} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Location} from '@angular/common';

@Component({
  selector: 'app-show-category',
  templateUrl: './show-category.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowCategoryComponent implements OnInit {

  id: number;
  public category: Category;
  posts: Post[];
  title: string;
  page: string;
  env_url: string;

  constructor(private activeRoute: ActivatedRoute,
              private titleService: Title,
              private _location: Location,
              private global: GlobalService) {
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.setTitle();
    this.getCategory();
  }

  setTitle() {
    this.titleService.setTitle('Show Category Posts | ' + this.global.appTitle());
  }

  getCategory() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);

        this.category = data.json;
        this.posts = this.category.posts;

        // console.log(this.category);
        // console.log(this.posts);
      }
    );
  }

}
