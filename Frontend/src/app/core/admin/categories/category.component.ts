import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../../shared/interfaces/category.interface';
import {CategoryService} from '../../../shared/services/category.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: '[app-category]',
  templateUrl: './category.component.html',
  animations: [
    trigger('newCategory', [
      state('default', style({
        opacity: '0'
      })),
      state('loaded', style({
        opacity: '1'
      })),
      transition('default => loaded', animate('200ms 0.1ms ease-in'))
    ])
  ]
})
export class CategoryComponent implements OnInit {

  @Input() category: Category;
  @Output() categoryDeleted = new EventEmitter<Category>();
  editing = false;
  editName = '';
  editSlug = '';

  constructor(private categoryService: CategoryService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
  }

  onEdit() {
    this.editing = true;
    this.editName = this.category.title;
    this.editSlug = this.category.slug;
  }

  onUpdate() {
    this.errorService.resetErrors();

    this.categoryService.updateCategory(this.category.id, this.editName, this.editSlug)
      .subscribe(
        (category: Category) => {
          // console.log(this.editName);

          this.category.title = this.editName;
          this.category.slug = this.editSlug;
          this.editName = '';
          this.editSlug = '';
          this.errorService.successHandler('Category Updated!', true);
        }
      );

    this.editing = false;
  }

  onCancel() {
    this.editName = '';
    this.editSlug = '';
    this.editing = false;
  }

  onDelete() {
    this.errorService.resetErrors();

    this.categoryService.deleteCategory(this.category.id)
      .subscribe(
        () => {
          this.categoryDeleted.emit(this.category);
        },
        (error) => {
          this.errorService.errorHandler(error);
        }
      );
  }

}
