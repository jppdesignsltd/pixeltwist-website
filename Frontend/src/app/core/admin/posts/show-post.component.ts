import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Location} from '@angular/common';
import {Post} from '../../../shared/interfaces/post.interface';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styles: []
})
export class ShowPostComponent implements OnInit {

  post: Post;
  env_url: string;

  constructor(
    private activeRoute: ActivatedRoute,
    private titleService: Title,
    private _location: Location,
    private global: GlobalService
  ) {
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.setTitle();
    this.getPost();
  }

  setTitle() {
    this.titleService.setTitle('Preview Post | ' + this.global.appTitle());
  }

  getPost() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        this.post = data.json;
      }
    );
  }

}
