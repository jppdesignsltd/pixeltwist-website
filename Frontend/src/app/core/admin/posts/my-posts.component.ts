import {Component, OnInit} from '@angular/core';
import {Response} from '@angular/http';

import {Post} from '../../../shared/interfaces/post.interface';
import {PostService} from '../../../shared/services/post.service';
import {Title} from '@angular/platform-browser';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class MyPostsComponent implements OnInit {

  posts: Post[];
  title: string;
  page: any;

  constructor(private postService: PostService,
              private errorService: ErrorService,
              private global: GlobalService,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getPosts();
  }

  setTitle() {
    this.titleService.setTitle('All Posts | ' + this.global.appTitle());
  }

  getPosts() {
    this.postService.getAuthorPosts()
      .subscribe(
        (posts: Post[]) => {
          // console.log(posts);
          if (posts) {
            this.posts = posts.slice().reverse();
          }
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

}
