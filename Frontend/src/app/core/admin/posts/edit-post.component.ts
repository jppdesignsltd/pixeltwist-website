import {
  Component, ElementRef, EventEmitter, OnInit, Output,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {NgForm} from '@angular/forms';
import {SlugifyPipe} from 'ngx-pipes';
import {IMyDpOptions} from 'mydatepicker';
import {Tags} from 'app/shared/interfaces/tags.interface';
import {Select2OptionData} from 'ng2-select2';
import {Title} from '@angular/platform-browser';
import {Location} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {Post} from '../../../shared/interfaces/post.interface';
import {CanComponentDeactivate} from '../../../shared/interfaces/can-deactivate.interface';
import {Category} from '../../../shared/interfaces/category.interface';
import {PostService} from '../../../shared/services/post.service';
import {CategoryService} from 'app/shared/services/category.service';
import {TagService} from '../../../shared/services/tag.service';
import {GlobalService} from '../../../shared/services/global.service';
import {User} from '../../../shared/interfaces/user.interface';
import {AuthorService} from '../../../shared/services/author.service';
import {ErrorService} from '../../../shared/services/error.service';
import {AuthService} from '../../../shared/services/auth.service';
import {LoadingService} from '../../../shared/services/loading.service';

const today = new Date();
let todayDay;

if (today.getDate() !== 1) {
  todayDay = today.getDate() - 1;
} else {
  todayDay = today.getDate();
}

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: [
    '../../../../bower_components/select2/dist/css/select2.min.css'
  ],
  providers: [
    SlugifyPipe
  ],
  encapsulation: ViewEncapsulation.None
})
export class EditPostComponent implements OnInit, CanComponentDeactivate {

  @Output() onEditorKeyup = new EventEmitter<string>();
  env_url: string;
  id: number;
  live_date;
  post: Post;
  author: any;
  image: string;
  hasImage: boolean;
  categories: Category[];
  content;
  authors: User[];
  defaultStatus: string;
  defaultCategory;
  defaultAuthor: User;
  disabledAuthor = true;
  disabledStatus = true;
  disabledCategory = true;
  disableDP = true;
  tags: Tags[];
  postTags;
  changesSaved = false;
  role;
  public tagsChanged = false;
  public showPreviewImage = false;
  public tagSelection: string[] = [];
  public tagsArray: Array<Select2OptionData>;
  public options: Select2Options;
  public value: string[] = [];
  public current: string;

  public myDatePickerOptions: IMyDpOptions = {
    // Check out other options - https://github.com/kekeh/mydatepicker
    dateFormat: 'dd/mm/yyyy',
    // disableUntil: {
    //   year: today.getFullYear(),
    //   month: today.getMonth() + 1,
    //   day: todayDay
    // },
    alignSelectorRight: true,
    editableDateField: false,
    markCurrentDay: true
  };

  constructor(private activeRoute: ActivatedRoute,
              private postService: PostService,
              private authService: AuthService,
              private loadingService: LoadingService,
              private categoryService: CategoryService,
              private authorService: AuthorService,
              private errorService: ErrorService,
              private tagService: TagService,
              private el: ElementRef,
              private _location: Location,
              private global: GlobalService,
              private titleService: Title) {
  }

  backClicked() {
    this._location.back();
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.changesSaved) {
      return confirm('Are you sure you want to leave? All unsaved changes will be lost.');
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.role = this.authService.getRole();

    this.setTitle();
    this.getCategories();
    this.getTags();
    this.getAuthors();

    this.options = {
      width: '100%',
      multiple: true,
      theme: 'classic',
      closeOnSelect: true
    };
  }

  setTitle() {
    this.titleService.setTitle('Edit Post | ' + this.global.appTitle());
  }

  getAuthors() {
    this.authorService.getAuthors()
      .subscribe(
        (response) => {
          // console.log(response);
          this.authors = response.authors;
        },
        (error) => {
          // console.log(error);
        }
      );
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  getTags() {
    this.tagService.getTags()
      .subscribe(
        (tags: Tags[]) => {
          if (tags) {
            this.tags = tags;
            this.select2Tags(tags);
          }

          // console.log(this.tagsArray);
        },
        (error: Response) => {
          // console.log(error);
        }
      );
  }

  getPost() {
    this.activeRoute.data.subscribe(
      (data: Data) => {

        // console.log(data.json);
        this.post = data.json;
        this.defaultAuthor = this.post.user.id;
        this.defaultCategory = this.post.category.id;
        this.defaultStatus = this.post.status;
        this.id = this.post.id;
        this.postTags = this.post.tags;

        for (let i = 0; i < this.postTags.length; i++) {
          this.value.push(this.postTags[i].id);
        }

        this.current = this.value.join(' | ');

        if (this.post.featured_image !== '') {
          this.showPreviewImage = true;
          this.image = this.post.featured_image;
          // console.log('Show the preview image');
          // console.log(this.post.featured_image);

          if (this.post.featured_image === null) {
            this.showPreviewImage = false;
          }
        }

      }
    );

  }

  select2Tags(tags) {

    tags.forEach(function (v) {
      v.text = v.title;
      delete v.title;
      delete v.slug;
      delete v.posts;
      delete v.projects;

      // console.log(v);
    });

    this.tagsArray = tags;

    // console.log(tags);
    this.getPost();
  }

  select2Changed(data) {

    if (data.value !== null) {
      this.current = data.value.join('|');

      // console.log(data.value);

      if (data.value.length > 0) {
        this.tagSelection = this.current.split('|');
      }

      if (data.value.length === 0) {
        this.tagSelection = [];
      }

      this.tagsChanged = true;
    }

    // console.log(data);

  }

  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e: any) {
        const preview = document.getElementById('preview') as HTMLImageElement;
        const remove_image = document.getElementById('remove_image') as HTMLElement;

        preview.src = e.target.result;
        preview.classList.remove('d-none');

        // remove_image.classList.remove('d-none');
      };

      this.showPreviewImage = true;

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  showInput(event, id: string) {
    const src = event.srcElement;
    const element = document.getElementById(id) as HTMLElement;
    const el_preview = document.getElementById(id + '_preview') as HTMLElement;

    src.classList.add('d-none');
    element.classList.remove('d-none');
    el_preview.classList.add('d-none');

    if (element.id === 'status') {
      this.disabledStatus = false;
    }

    if (element.id === 'date_picker') {
      this.disableDP = false;
    }

    if (element.id === 'category') {
      this.disabledCategory = false;
    }

    if (element.id === 'user_id') {
      this.disabledAuthor = false;
    }
  }

  removeImage() {
    const featured_image = document.getElementById('featured_image') as HTMLInputElement;
    const preview = document.getElementById('preview') as HTMLImageElement;
    const remove_image = document.getElementById('remove_image') as HTMLElement;

    preview.src = '';
    remove_image.classList.add('d-none');

    featured_image.value = '';

    this.image = 'delete';

    this.showPreviewImage = false;

    // console.log(this.image);
  }

  tinymceKeyup(content) {
    this.content = content;

    // console.log(content);
  }

  onUpdate(form: NgForm) {
    const formData = form.value;
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#featured_image');
    const fileCount: number = inputEl.files.length;
    const imgPreview = document.getElementById('preview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 1280;
    const maxHeight = 700;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    // Reset Errors
    this.errorService.resetErrors();
    this.loadingService.showLoading();

    formData.main_content = this.content;

    if (this.tagsChanged) {
      formData.tags = this.tagSelection;
      // console.log('tags have changed');
    } else {
      formData.tags = this.value;
      // console.log('tags the same');
    }

    if (this.image !== 'delete' || this.image !== null) {
      if (this.image !== this.post.featured_image) {
        formData.featured_image = this.image;
      }
    }

    // if (this.showPreviewImage) {
    //   if (this.image === this.post.featured_image) {
    //     formData.featured_image = this.image;
    //   }
    // }

    // NOT USED //
    // if (this.image === 'delete' || this.image === null) {
    //   // formData.featured_image = null;
    // }

    console.log(JSON.stringify(formData));
    // console.log(this.image);
    console.log(formData);

    if (formData.mydate) {
      formData.live_date = formData.mydate.formatted;
      delete formData['mydate'];
    }

    if (fileCount > 0) {
      file = inputEl.files.item(0);
      fileExt = file.name.split('.').pop();

      width = imgPreview.naturalWidth;
      height = imgPreview.naturalHeight;

      if (width > maxWidth) {
        ratio = maxWidth / width;
        width = width * ratio;
        height = height * ratio;
      }

      if (height > maxHeight) {
        ratio = maxHeight / height;
        width = width * ratio;
        height = height * ratio;
      }

      canvas.width = width;
      canvas.height = height;

      ctx = canvas.getContext('2d');

      ctx.drawImage(imgPreview, 0, 0, width, height);

      if (fileExt === 'jpg') {
        fileExt = 'jpeg';
        dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
      } else {
        // console.log(fileExt + ' was used');
        dataURL = canvas.toDataURL();
      }

      callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};

      formData.featured_image = callback.url;
    }

    // console.log(formData);

    this.postService.updatePost(this.id, formData)
      .subscribe(
        () => {
          // console.log(response);
          this.changesSaved = true;
          this.errorService.successHandler('Post Updated!');
          this.loadingService.hideLoading();

        },
        (error) => {
          // console.log(error);
          this.errorService.errorHandler(error);
          this.loadingService.hideLoading();

        }
      );
  }

}
