import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {PostService} from '../../../shared/services/post.service';
import {ErrorService} from '../../../shared/services/error.service';
import {AuthService} from '../../../shared/services/auth.service';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: '[app-post]',
  templateUrl: './post.component.html'
})
export class PostComponent implements OnInit {

  @Input() post: Post;
  @Input() viewBlock: string;
  @Output() postDeleted = new EventEmitter<Post>();
  imageSrc: string;
  role: string;
  isAuthor = false;

  constructor(private postService: PostService,
              private authService: AuthService,
              private global: GlobalService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
    this.role = this.authService.getRole();
    this.imageSrc = this.global.getUploadsUrl() + 'uploads/posts/' + this.post.featured_image;

    if (this.role === 'author') {
      this.isAuthor = true;
    }
  }

  onDelete() {
    this.errorService.resetErrors();

    this.postService.deletePost(this.post.id)
      .subscribe(
        () => {
          this.postDeleted.emit(this.post);
        },
        (error) => {
          // console.log(error);
          this.errorService.errorHandler(error);
        }
      );
  }

}
