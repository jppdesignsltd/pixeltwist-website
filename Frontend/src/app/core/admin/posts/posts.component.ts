import {Component, OnInit, Renderer2} from '@angular/core';
import {Response} from '@angular/http';

import {Post} from '../../../shared/interfaces/post.interface';
import {PostService} from '../../../shared/services/post.service';
import {Title} from '@angular/platform-browser';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class PostsComponent implements OnInit {

  posts: Post[];
  viewBlock = 'block';
  postsCount;
  page: any;

  constructor(private postService: PostService,
              private errorService: ErrorService,
              private global: GlobalService,
              private renderer: Renderer2,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getPosts();
  }

  changeView(el) {
    if (this.viewBlock === 'table') {
      this.renderer.removeClass(el, 'fa-toggle-off');
      this.renderer.addClass(el, 'fa-toggle-on');
      return this.viewBlock = 'block';
    }

    this.renderer.removeClass(el, 'fa-toggle-on');
    this.renderer.addClass(el, 'fa-toggle-off');
    return this.viewBlock = 'table';
  }

  setTitle() {
    this.titleService.setTitle('All Posts | ' + this.global.appTitle());
  }

  getPosts() {
    this.postService.getPosts()
      .subscribe(
        (posts: Post[]) => {
          // console.log(posts);
          if (posts) {
            this.posts = posts.slice().reverse();
            this.postsCount = this.posts.length;
          }
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  onDeleted(post: Post) {
    // console.log('Will remove from the table now');
    this.errorService.resetErrors();

    const position = this.posts.findIndex(
      (postEl: Post) => {
        // console.log(postEl);
        return postEl.id === post.id;
      }
    );

    this.posts.splice(position, 1);

    this.errorService.deleteHandler('Post Deleted', true);
  }

}
