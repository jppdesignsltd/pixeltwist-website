import {
  Component, OnInit, ElementRef, ViewEncapsulation
} from '@angular/core';
import {NgForm} from '@angular/forms';
import {PostService} from '../../../shared/services/post.service';
import {CanActivate, Router} from '@angular/router';
import {SlugifyPipe} from 'ngx-pipes';
import {IMyDpOptions} from 'mydatepicker';
import {Category} from '../../../shared/interfaces/category.interface';
import {CategoryService} from '../../../shared/services/category.service';
import {Tags} from 'app/shared/interfaces/tags.interface';
import {TagService} from '../../../shared/services/tag.service';
import {Select2OptionData} from 'ng2-select2';
import {Title} from '@angular/platform-browser';
import {CanComponentDeactivate} from '../../../shared/interfaces/can-deactivate.interface';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from '../../../shared/services/global.service';
import {User} from '../../../shared/interfaces/user.interface';
import {AuthorService} from '../../../shared/services/author.service';
import {ErrorService} from '../../../shared/services/error.service';
import {AuthService} from '../../../shared/services/auth.service';

const today = new Date();
let todayDay;

if (today.getDate() !== 1) {
  todayDay = today.getDate() - 1;
} else {
  todayDay = today.getDate();
}

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: [
    '../../../../bower_components/select2/dist/css/select2.min.css'
  ],
  providers: [
    SlugifyPipe
  ],
  encapsulation: ViewEncapsulation.None
})

export class NewPostComponent implements OnInit, CanComponentDeactivate {

  categories: Category[];
  role: string;
  adminRoles = ['admin', 'editor'];
  defaultStatus = 'published';
  defaultCategory = 'Please choose';
  disabledOption = true;
  authors: User[];
  defaultAuthor: User;
  content;
  disabledSlug = true;
  tags: Tags[];
  changesSaved = false;
  title: string;
  imageUrl: string;
  live_date: any;
  public tagSelection = [];
  public tagsArray: Array<Select2OptionData>;
  public options: Select2Options;
  public current: string;

  public myDatePickerOptions: IMyDpOptions = {
    // Check out other options - https://github.com/kekeh/mydatepicker
    dateFormat: 'dd/mm/yyyy',
    // disableUntil: {
    //   year: today.getFullYear(),
    //   month: today.getMonth() + 1,
    //   day: todayDay
    // },
    alignSelectorRight: true,
    editableDateField: false,
    markCurrentDay: true
  };

  constructor(private postService: PostService,
              private tagService: TagService,
              private errorService: ErrorService,
              private authService: AuthService,
              private authorService: AuthorService,
              private router: Router,
              private slugPipe: SlugifyPipe,
              private el: ElementRef,
              private global: GlobalService,
              private categoryService: CategoryService,
              private titleService: Title) {

  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.changesSaved) {
      return confirm('Are you sure you want to leave? All unsaved changes will be lost.');
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.role = this.authService.getRole();

    this.setTitle();
    this.getCategories();
    this.getTags();
    this.getAuthors();
  }

  setTitle() {
    this.titleService.setTitle('New Post | ' + this.global.appTitle());
  }

  getAuthors() {
    this.authorService.getAuthors()
      .subscribe(
        (response) => {
          // console.log(response);
          this.authors = response.authors;
          this.defaultAuthor = response.currentUser.id;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  getTags() {
    this.tagService.getTags()
      .subscribe(
        (tags: Tags[]) => {
          if (tags) {
            this.tags = tags;
            this.select2Tags(tags);

            this.options = {
              width: '100%',
              multiple: true,
              theme: 'classic',
              closeOnSelect: true
            };
          }

          // console.log(this.tagsArray);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  select2Tags(tags) {

    tags.forEach(function (v) {
      v.text = v.title;
      delete v.title;
      delete v.slug;

      // console.log(v);
    });

    this.tagsArray = tags;
  }

  select2Changed(data) {

    // console.log(data.value);

    if (data.value.length !== 0) {
      this.current = data.value.join('|');
      this.tagSelection = this.current.split('|');
    } else {
      this.tagSelection = [];
    }

    // console.log(data);
    // console.log(this.tagSelection);

  }

  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();

      reader.onload = function (e: any) {
        const preview = document.getElementById('preview') as HTMLImageElement;
        const remove_image = document.getElementById('remove_image') as HTMLElement;

        preview.src = e.target.result;
        preview.classList.remove('d-none');

        remove_image.classList.remove('d-none');
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeImage() {
    const featured_image = document.getElementById('featured_image') as HTMLInputElement;
    const preview = document.getElementById('preview') as HTMLImageElement;
    const remove_image = document.getElementById('remove_image') as HTMLElement;

    preview.src = '';
    preview.classList.add('d-none');
    remove_image.classList.add('d-none');

    featured_image.value = '';
  }

  showInput(event, id: string) {
    const src = event.srcElement;
    const element = document.getElementById(id) as HTMLElement;
    const el_preview = document.getElementById(id + '_preview') as HTMLElement;

    element.classList.remove('d-none');
    el_preview.classList.add('d-none');

    if (element.id === 'slug') {
      this.disabledSlug = false;
    }
  }

  tinymceKeyup(content) {
    this.content = content;
    // console.log(content);
  }

  processImage() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#featured_image');
    const fileCount: number = inputEl.files.length;
    const imgPreview = document.getElementById('preview') as HTMLImageElement;
    const canvas = document.createElement('canvas');
    const maxWidth = 1280;
    const maxHeight = 700;
    let ratio = 0;
    let file;
    let width;
    let height;
    let ctx;
    let dataURL;
    let callback;
    let fileExt;

    file = inputEl.files.item(0);
    fileExt = file.name.split('.').pop();

    // console.log(fileExt);

    width = imgPreview.naturalWidth;
    height = imgPreview.naturalHeight;

    // console.log(width);
    // console.log(height);

    if (width > maxWidth) {
      ratio = maxWidth / width;
      width = width * ratio;
      height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      width = width * ratio;
      height = height * ratio;
    }

    // console.log(width);
    // console.log(height);

    canvas.width = width;
    canvas.height = height;

    ctx = canvas.getContext('2d');

    ctx.drawImage(imgPreview, 0, 0, width, height);

    if (fileExt === 'jpg') {
      fileExt = 'jpeg';
      dataURL = canvas.toDataURL('image/' + fileExt, 1.0);
    } else {
      // console.log(fileExt + ' was used');
      dataURL = canvas.toDataURL();
    }

    callback = {url: dataURL, imgLength: imgPreview.src.length, urlLength: dataURL.length};

    return this.imageUrl = callback.url;
  }

  onSubmit(form: NgForm) {

    const formData = form.value;
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#featured_image');
    const fileCount: number = inputEl.files.length;

    this.errorService.resetErrors();

    if (!formData.mydate) {
      return this.errorService.showErrorHandler('No date selected!');
    }

    formData.main_content = this.content;
    formData.live_date = formData.mydate.formatted;
    formData.slug = this.slugPipe.transform(form.value.title);

    if (formData.category === this.defaultCategory) {
      return this.errorService.showErrorHandler('Please choose a category');
    }

    if (this.tagSelection.length > 0) {
      formData.tags = this.tagSelection;
    }

    if (fileCount > 0) {
      this.processImage();
      formData.featured_image = this.imageUrl;
    }

    // console.log(formData);

    // console.log(JSON.stringify(formData));
    delete formData.mydate;

    this.postService.createPost(formData)
      .subscribe(
        () => {
          // alert('Post Created');
          this.changesSaved = true;

          if (this.role === 'author') {
            return this.router.navigateByUrl('/admin/my-posts');
          }

          return this.router.navigateByUrl('/admin/posts');

        },
        (error) => {

          // console.log(error.error);
          this.errorService.errorHandler(error);

        }
      );
  }

}
