import {Component, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {ActivatedRoute, Data} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Location} from '@angular/common';
import {Services} from '../../../shared/interfaces/service.interface';

@Component({
  selector: 'app-show-service',
  templateUrl: './show-service.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowServiceComponent implements OnInit {

  id: number;
  service: Services;
  projects: Project[];
  env_url: string;
  projectPage: any;

  constructor(private activeRoute: ActivatedRoute,
              private titleService: Title,
              private _location: Location,
              private global: GlobalService) {
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
    this.setTitle();
    this.getService();
  }

  setTitle() {
    this.titleService.setTitle('Show Service Posts & Projects | ' + this.global.appTitle());
  }

  getService() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);

        this.service = data.json;
        this.projects = data.json.projects;

        // console.log(this.projects);
      }
    );
  }

}
