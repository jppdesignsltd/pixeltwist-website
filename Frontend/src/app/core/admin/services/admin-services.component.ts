import {Component, OnInit} from '@angular/core';
import {SlugifyPipe} from 'ngx-pipes';
import {ServiceService} from '../../../shared/services/service.service';
import {NgForm} from '@angular/forms';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';
import {Services} from '../../../shared/interfaces/service.interface';

@Component({
  selector: 'app-admin-services',
  templateUrl: './admin-services.component.html',
  providers: [
    SlugifyPipe
  ],
  animations: [
    listAnimationTrigger
  ]
})

export class AdminServicesComponent implements OnInit {

  services: Services[];
  title: string;
  page: any;

  constructor(private serviceService: ServiceService,
              private titleService: Title,
              private errorService: ErrorService,
              private global: GlobalService,
              private slugPipe: SlugifyPipe) {
  }

  ngOnInit() {
    this.setTitle();
    this.getServices();
  }

  setTitle() {
    this.titleService.setTitle('Services | ' + this.global.appTitle());
  }

  getServices() {
    this.serviceService.getServices()
      .subscribe(
        (services: Services[]) => {
          // console.log(services);

          if (services) {
            this.services = services.slice().reverse();
          }
          // console.log(services);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  onAdded(newService: Services) {
    // console.log(newService);
    this.services.unshift(newService);
  }

  onDeleted(service: Services) {
    this.errorService.resetErrors();

    const position = this.services.findIndex(
      (catEl: Services) => {
        return catEl.id === service.id;
      }
    );
    this.services.splice(position, 1);

    this.errorService.deleteHandler('Service successfully deleted!', true);
  }

  onSubmit(form: NgForm) {
    const formData = form.value;

    formData.slug = this.slugPipe.transform(form.value.title);
    this.errorService.resetErrors();

    // console.log(formData);

    this.serviceService.createService(formData)
      .subscribe(
        (service) => {

          this.onAdded(service);
          this.errorService.successHandler('Service successfully created', true);
          form.reset();

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);

        }
      );
  }

}
