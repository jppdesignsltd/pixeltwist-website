import {Component, Input, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';

@Component({
  selector: 'app-show-service-projects',
  templateUrl: './show-service-projects.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class ShowServiceProjectsComponent implements OnInit {

  @Input() projects: Project[];
  projectPage: any;

  constructor() {
  }

  ngOnInit() {
  }

}
