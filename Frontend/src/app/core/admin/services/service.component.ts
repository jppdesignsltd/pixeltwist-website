import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ServiceService} from '../../../shared/services/service.service';
import {ErrorService} from '../../../shared/services/error.service';
import {Services} from '../../../shared/interfaces/service.interface';

@Component({
  selector: '[app-service]',
  templateUrl: './service.component.html'
})
export class ServiceComponent implements OnInit {

  @Input() service: Services;
  @Output() serviceDeleted = new EventEmitter<Services>();
  editing = false;
  editName = '';
  editSlug = '';

  constructor(private serviceService: ServiceService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
  }

  onEdit() {
    this.editing = true;
    this.editName = this.service.title;
    this.editSlug = this.service.slug;
  }

  onUpdate() {
    this.errorService.resetErrors();

    this.serviceService.updateService(this.service.id, this.editName, this.editSlug)
      .subscribe(
        (service: Services) => {
          // console.log(this.editName);

          this.service.title = this.editName;
          this.service.slug = this.editSlug;
          this.editName = '';
          this.editSlug = '';
          this.errorService.successHandler('Service Updated!', true);
        }
      );

    this.editing = false;
  }

  onCancel() {
    this.editName = '';
    this.editSlug = '';
    this.editing = false;
  }

  onDelete() {
    this.errorService.resetErrors();

    this.serviceService.deleteService(this.service.id)
      .subscribe(
        () => {
          this.serviceDeleted.emit(this.service);
        },
        (error) => {

          this.errorService.errorHandler(error);

        }
      );
  }

}
