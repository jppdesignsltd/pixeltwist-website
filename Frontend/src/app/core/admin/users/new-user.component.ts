import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {Roles} from '../../../shared/interfaces/role.interface';
import {RoleService} from '../../../shared/services/role.service';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: [
    '../../../../bower_components/select2/dist/css/select2.min.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class NewUserComponent implements OnInit {

  passwordConfirm = true;
  password: string;
  disabledConfirmation = false;
  defaultRole: string;
  roles: Roles[];

  constructor(
    private authService: AuthService,
    private titleService: Title,
    private errorService: ErrorService,
    private roleService: RoleService,
    private global: GlobalService,
    private router: Router
  ) { }

  ngOnInit() {
    this.setTitle();
    this.getRoles();
  }

  setTitle() {
    this.titleService.setTitle('New User | ' + this.global.appTitle());
  }

  getRoles() {
    this.roleService.getRoles()
      .subscribe(
        (roles: Roles[]) => {
          if (roles) {
            this.roles = roles;
            this.defaultRole = this.roles[0].name;
          }
          // console.log(this.roles);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  passwordValidation(event: any) {

    this.errorService.resetErrors();
    this.errorService.passwordHandler(event, this.password);

  }

  onSubmit(form: NgForm) {
    const formData = form.value;
    // console.log(formData);

    this.errorService.resetErrors();

    if (formData.password_confirmation !== formData.password) {
      this.passwordConfirm = false;
      return this.errorService.showErrorHandler('Your passwords do not match!');
    }


    this.authService.signup(formData)
      .subscribe(
        (success) =>  {

          // console.log(success);
          this.errorService.successHandler('You have successfully signed up. Please login.');
          this.router.navigateByUrl('admin/users');
          form.reset();

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);

        }
      );
  }

}
