import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../shared/interfaces/user.interface';
import {UserService} from '../../../shared/services/user.service';
import {ErrorService} from '../../../shared/services/error.service';
import {Roles} from '../../../shared/interfaces/role.interface';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: '[app-user]',
  templateUrl: './user.component.html',
  styles: []
})
export class UserComponent implements OnInit {

  @Input() user: User;
  role: Roles;
  userImage: string;
  activeVal: number;
  editing = false;
  editFirstName = '';
  editLastName = '';
  editEmail = '';

  constructor(private userService: UserService,
              private global: GlobalService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
    this.role = this.user.roles[0];

    if (this.user.avatar) {
      this.userImage = this.global.getUploadsUrl() + '/uploads/profiles/' + this.user.avatar;
    } else {
      this.userImage = '/dist/img/default_user.png';
    }
  }

  onEdit() {
    this.editing = true;
    this.editFirstName = this.user.firstname;
    this.editLastName = this.user.lastname;
    this.editEmail = this.user.email;
  }

  onCancel() {
    this.editing = false;
  }

  onUpdate() {

    this.errorService.resetErrors();

    const formData = {
      firstname: this.editFirstName,
      lastname: this.editLastName,
      email: this.editEmail
    };

    this.userService.updateUser(this.user.id, formData)
      .subscribe(
        () => {

          this.user.firstname = this.editFirstName;
          this.user.lastname = this.editLastName;
          this.user.email = this.editEmail;
          this.editFirstName = '';
          this.editLastName = '';
          this.editEmail = '';

          this.errorService.successHandler('User updated', true);

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);


        }
      );

    this.editing = false;
  }

  toggleActive() {
    // console.log();
    const formData = {
      m: 'active',
      active: this.activeVal
    };

    this.errorService.resetErrors();

    if (this.user.active === '1') {
      this.activeVal = 0;
    } else {
      this.activeVal = 1;
    }

    formData.active = this.activeVal;

    this.userService.toggleUserActive(this.user.id, formData)
      .subscribe(
        (response) => {

          // console.log(response);
          if (this.user.active === '1') {
            this.user.active = '0';
          } else {
            this.user.active = '1';
          }
          this.errorService.successHandler('User updated!', true);

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);

        }
      )
  }

}
