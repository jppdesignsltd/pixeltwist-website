import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {User} from '../../../shared/interfaces/user.interface';
import {UserService} from '../../../shared/services/user.service';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class UsersComponent implements OnInit {

  users: User[];
  userCount: number;
  page: any;

  constructor(
    private userService: UserService,
    private global: GlobalService,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.setTitle();
    this.getUsers();
  }

  setTitle() {
    this.titleService.setTitle('All Users | ' + this.global.appTitle());
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe(
        (users: User[]) => {
          this.users = users.slice().reverse().sort();
          this.userCount = this.users.length;
          // console.log(users);
        }
      );
  }



}
