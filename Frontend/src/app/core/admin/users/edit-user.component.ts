import {Component, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {User} from '../../../shared/interfaces/user.interface';
import {UserService} from '../../../shared/services/user.service';
import {GlobalService} from '../../../shared/services/global.service';
import {ErrorService} from '../../../shared/services/error.service';
import {Roles} from '../../../shared/interfaces/role.interface';
import {RoleService} from '../../../shared/services/role.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html'
})
export class EditUserComponent implements OnInit {

  id: number;
  user: User;
  disabledRole = true;
  disabledPassword = true;
  disabledConfirmation = true;
  passwordConfirm = true;
  password_confirmation: string;
  password: string;
  defaultRole: string;
  roles: Roles[];

  constructor(private activeRoute: ActivatedRoute,
              private userService: UserService,
              private roleService: RoleService,
              private loadingService: LoadingService,
              private renderer: Renderer2,
              private errorService: ErrorService,
              private global: GlobalService,
              private titleService: Title) {
  }

  ngOnInit() {
    this.setTitle();
    this.getUser();
    this.getRoles();
  }

  setTitle() {
    this.titleService.setTitle('Edit Post | ' + this.global.appTitle());
  }

  getUser() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        this.user = data.json;
        this.id = this.user.id;
        this.defaultRole = this.user.roles[0].name;
      }
    );
  }

  getRoles() {
    this.roleService.getRoles()
      .subscribe(
        (roles: Roles[]) => {
          if (roles) {
            this.roles = roles;
          }
          // console.log(this.roles);
        },
        (error: Response) => {
          console.log(error);
        }
      );
  }

  passwordValidation(event: any) {
    this.errorService.resetErrors();
    this.errorService.passwordHandler(event, this.password);
  }

  showInput(event, id: string) {
    const src = event.srcElement;
    const element = document.getElementById(id) as HTMLElement;
    const password_cont = document.getElementById('password-cont') as HTMLElement;

    this.renderer.addClass(src, 'd-none');

    if (element.id === 'password') {
      this.disabledPassword = false;
      this.disabledConfirmation = false;
      this.renderer.removeClass(password_cont, 'd-none');
    }

    if (element.id === 'role') {
      this.disabledRole = false;
    }
  }

  onUpdate(form: NgForm) {
    const formData = form.value;

    // Reset Errors
    this.errorService.resetErrors();
    this.loadingService.showLoading();

    if (formData.password_confirmation !== formData.password) {
      this.passwordConfirm = false;
      return this.errorService.showErrorHandler('Your passwords do not match!');
    }


    this.userService.updateUser(this.id, formData)
      .subscribe(
        () => {

          this.errorService.successHandler('User Updated', true);
          this.loadingService.hideLoading();

        },
        (error) => {

          // console.log(error);
          this.errorService.errorHandler(error);
          this.loadingService.hideLoading();

        }
      );
  }

}
