import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {GlobalService} from '../../../shared/services/global.service';
import {Title} from '@angular/platform-browser';
import {ErrorService} from '../../../shared/services/error.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private authService: AuthService,
              private global: GlobalService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private titleService: Title,
              private router: Router) {

    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/admin');
    }

  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('Forgot Password | ' + this.global.appTitle());
  }

  onSubmit(form: NgForm) {
    this.errorService.resetErrors();
    this.loadingService.showLoading();
    // console.log('😃');

    this.authService.reset(form.value.email)
      .subscribe(
        () => {

          this.errorService.successHandler('An email has been sent with a reset link. Please follow the instruction on the email.');
          this.loadingService.hideLoading();

          form.reset();

        },
        (error) => {
          this.loadingService.hideLoading();
          this.errorService.showErrorHandler(JSON.parse(error._body).error);
        }
      );
  }
}
