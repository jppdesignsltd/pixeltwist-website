import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {GlobalService} from '../../../shared/services/global.service';
import {Title} from '@angular/platform-browser';
import {ErrorService} from '../../../shared/services/error.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})

export class SignupComponent implements OnInit {

  password: string;
  password_confirmation: string;

  constructor(private authService: AuthService,
              private global: GlobalService,
              private errorService: ErrorService,
              private titleService: Title,
              private router: Router) {
  }

  ngOnInit() {
    this.setTitle();

    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/admin');
    }
  }

  setTitle() {
    this.titleService.setTitle('Signup | ' + this.global.appTitle());
  }

  passwordValidation(event) {
    this.errorService.resetErrors();
    this.errorService.passwordHandler(event, this.password);
  }

  onSignup(form: NgForm) {
    const formData = form.value;
    this.errorService.resetErrors();

    if (formData.password_confirmation !== formData.password) {
      return this.errorService.showErrorHandler('Your passwords do not match!');
    }

    formData.role = 'standard';

    this.authService.signup(formData)
      .subscribe(
        () => {
          this.errorService.successHandler('You have successfully signed up. You will be redirected to the login page shortly.');

          form.reset();

          return setTimeout(() => {
            this.router.navigateByUrl('/auth/signin');
          }, 3000);
        },
        (errors) => {

          const errorCont = document.getElementById('error-messages') as HTMLElement;
          const errorUl = document.getElementById('errors') as HTMLElement;
          let liTag;

          errorCont.classList.remove('d-none');
          errorCont.classList.add('alert-danger');

          for (const key in JSON.parse(errors._body)) {
            if (JSON.parse(errors._body).hasOwnProperty(key)) {
              if (key === 'error') {
                liTag = document.createElement('li');
                errorUl.appendChild(liTag).innerHTML = errors[key];
              } else {
                const array = JSON.parse(errors._body)[key];

                array.forEach((value, index) => {
                  liTag = document.createElement('li');
                  errorUl.appendChild(liTag).innerHTML = value;
                });
              }
            }
          }

        }
      );
  }

}
