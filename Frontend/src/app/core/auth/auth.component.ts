import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {

  token;
  role;
  validRoles = ['admin', 'editor', 'author'];

  constructor(private location: Location,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.token = this.authService.getToken();
    this.role = this.authService.getRole();

    const hasRole = this.validRoles.indexOf(this.role) !== -1;

    if (this.token) {
      if (hasRole) {
        return this.router.navigateByUrl('/admin');
      }
      return this.router.navigateByUrl('/');
    }
  }

  closeClicked() {
    // this.location.back();
    this.router.navigateByUrl('/');
  }

}
