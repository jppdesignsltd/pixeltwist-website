import {Component, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {GlobalService} from '../../../shared/services/global.service';
import {Title} from '@angular/platform-browser';
import {ErrorService} from '../../../shared/services/error.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService,
              private global: GlobalService,
              private loadingService: LoadingService,
              private errorService: ErrorService,
              private titleService: Title,
              private router: Router) {

    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/admin');
    }

  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('Signin | ' + this.global.appTitle());
  }

  onSignin(form: NgForm) {

    this.errorService.resetErrors();
    this.loadingService.showLoading();

    this.authService.signin(form.value.email, form.value.password)
      .subscribe(
        (success) => {
          this.router.navigateByUrl('/admin');
          // console.log(success);
        },
        (errors) => {
          this.errorService.showErrorHandler(JSON.parse(errors._body).error);
          this.loadingService.hideLoading();
        }
      );
  }

}
