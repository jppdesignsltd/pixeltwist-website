import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '../../../shared/services/global.service';
import {Title} from '@angular/platform-browser';
import {ErrorService} from '../../../shared/services/error.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {

  private reset_token;
  password: string;
  password_confirmation: string;

  constructor(private authService: AuthService,
              private router: Router,
              private errorService: ErrorService,
              private titleService: Title,
              private loadingService: LoadingService,
              private global: GlobalService,
              private activeRoute: ActivatedRoute) {

    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/admin');
    }
  }

  ngOnInit() {
    this.setTitle();

    const resetToken = this.authService.getResetToken();

    this.activeRoute
      .queryParams
      .subscribe(params => {
        this.reset_token = params['token'];

        if (typeof this.reset_token === 'undefined' || this.reset_token !== resetToken) {

          if (this.reset_token !== resetToken) {

            this.errorService.showErrorHandler('Token does not match. Redirecting back to signin page');

          } else {

            this.errorService.showErrorHandler('No token. Redirecting back to signin page');

          }

          return setTimeout(() => {
            localStorage.removeItem('reset_token');
            this.router.navigateByUrl('/auth/signin');
          }, 3000);
        }
      });
  }

  setTitle() {
    this.titleService.setTitle('Reset Password | ' + this.global.appTitle());
  }

  passwordValidation(event) {
    this.errorService.resetErrors();
    this.errorService.passwordHandler(event, this.password);
  }

  onSubmit(form: NgForm) {
    const reset_token = this.authService.getResetToken();
    const formData = form.value;
    this.loadingService.showLoading();

    if (formData.password_confirmation !== formData.password) {
      this.loadingService.hideLoading();
      return false;
    }

    this.errorService.resetErrors();

    formData.token = reset_token;

    this.authService.resetPassword(formData)
      .subscribe(
        success => {

          this.errorService.successHandler('Password has been updated. ' +
            'You will be redirected to the login page shortly. If not ' +
            '<a [routerLink]=\'["/signing"]\' class="text-success">click here</a>');

          this.loadingService.hideLoading();

          setTimeout(() => {
            localStorage.removeItem('reset_token');
            this.router.navigateByUrl('/signin');
          }, 3000);
        },
        (error) => {
          this.errorService.showErrorHandler('Error please contact support');
          this.loadingService.hideLoading();

          // localStorage.removeItem('reset_token');
          // console.log(JSON.parse(error._body).error);
        }
      );
  }

}
