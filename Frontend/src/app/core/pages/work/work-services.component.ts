import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Project} from '../../../shared/interfaces/project.interface';

@Component({
  selector: 'app-work-service',
  templateUrl: './work-service.component.html',
  animations: [
    listAnimationTrigger
  ],
})
export class WorkServiceComponent implements OnInit, OnDestroy {

  projects: Project[];
  slug: any;
  service: string;
  postPage: any;
  projectPage: any;

  constructor(private activeRoute: ActivatedRoute,
              private global: GlobalService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit() {
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );

    this.getProjects();
    this.setTitle();
    this.getSlugName();
  }

  setTitle() {
    this.titleService.setTitle('Service Archive | ' + this.global.appTitle());
  }

  getSlugName() {
    this.slug = this.activeRoute.params.subscribe(params => {
      this.service = params['slug'];
      this.service = this.service.replace(/[\-\]&]+/g, ' ');
      // console.log(this.service);
    });
  }

  getProjects() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        // this.posts = data.json.posts;
        this.projects = data.json.projects;
      }
    );
  }

  ngOnDestroy() {
    this.slug.unsubscribe();
  }

}
