import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {User} from '../../../shared/interfaces/user.interface';
import {Tags} from '../../../shared/interfaces/tags.interface';
import {Services} from '../../../shared/interfaces/service.interface';
import {Location} from '@angular/common';
import {WorkService} from '../../../shared/services/work.service';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';

@Component({
  selector: 'app-our-work-show',
  templateUrl: './our-work-show.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class OurWorkShowComponent implements OnInit, AfterViewInit, OnDestroy {

  author: User;
  project: Project;
  projects: Project[];
  tags: Tags[];
  services: Services[];

  getAllProjects: any;
  projectCoverImage: string;
  coverHeight: string;
  projectLogoImage: string;
  projectTitle: string;
  meta_keywords: string;
  meta_description: string;

  hasCoverImage = false;

  constructor(private activeRoute: ActivatedRoute,
              private titleService: Title,
              private workService: WorkService,
              private metaService: Meta,
              private _location: Location,
              private global: GlobalService,
              private router: Router) {
  }

  ngOnInit() {
    this.getProject();
  }

  ngAfterViewInit() {
    this.router.events.subscribe(
      () => {
        this.getProjects();
      }
    );
  }

  backClicked() {
    this._location.back();
  }

  getProjects() {
    this.getAllProjects = this.workService.getProjects()
      .subscribe(
        (projects: Project[]) => {

          this.projects = projects;

          if (this.project) {
            this.projects = this.projects.filter(project => project.id !== this.project.id);
          }

          // console.log(this.projects);

        },
        (error) => {
          // console.log(error);
        }
      );
  }

  getProject() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);

        this.project = data.json;

        if (this.project) {
          this.author = this.project.user;
          this.services = this.project.services;
          this.tags = this.project.tags;
          this.projectTitle = this.project.title;
          this.meta_keywords = this.project.meta_keywords;
          this.meta_description = this.project.meta_description;
          this.setTitle(this.projectTitle);

          if (this.project.cover_image) {
            this.coverHeight = '350px';
            this.hasCoverImage = true;
            this.projectCoverImage = this.global.getUploadsUrl() + '/uploads/projects/' + this.project.cover_image;
          } else {
            this.hasCoverImage = false;
            this.coverHeight = '150px';
          }

          if (this.project.logo_image) {
            this.projectLogoImage = this.global.getUploadsUrl() + '/uploads/projects/' + this.project.logo_image;
          }

          this.getProjects();
          return this.setMeta();
        }

        return this.router.navigateByUrl('/page-not-found');
      },
      (error) => {
        console.log(error);

        return this.router.navigateByUrl('/page-not-found');
      }
    );
  }

  setTitle(pageTitle) {
    this.titleService.setTitle(pageTitle + ' ' + this.global.appTitle());
  }

  setMeta() {

    // Set Page author
    this.metaService.removeTag('name="author"');
    this.metaService.addTag({
      name: 'author',
      content: this.author.firstname + ' ' + this.author.lastname
    });

    // Set Page Keywords -- not that these are used much anymore
    if (this.meta_keywords !== null) {
      // console.log(this.meta_keywords);

      if (this.meta_keywords.length !== 0) {
        this.metaService.removeTag('name="keywords"');
        this.metaService.addTag({
          name: 'keywords',
          content: this.meta_keywords
        });
      }
    }

    // Set Page Description
    if (this.meta_description !== null) {
      // console.log(this.meta_description);

      if (this.meta_description.length !== 0) {
        this.metaService.removeTag('name="description"');
        this.metaService.addTag({
          name: 'description',
          content: this.meta_description
        });
      }
    }
  }

  ngOnDestroy() {
    this.getAllProjects.unsubscribe();
  }
}
