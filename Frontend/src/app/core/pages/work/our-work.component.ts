import {Component, OnInit} from '@angular/core';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Project} from '../../../shared/interfaces/project.interface';
import {Title} from '@angular/platform-browser';
import {WorkService} from '../../../shared/services/work.service';
import {GlobalService} from '../../../shared/services/global.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-our-work',
  templateUrl: './our-work.component.html',
  animations: [
    listAnimationTrigger
  ]
})
export class OurWorkComponent implements OnInit {

  projects: Project[];
  projectPage: string;
  env_url: string;

  constructor(private workService: WorkService,
              private global: GlobalService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit() {
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );

    this.getProjects();
    this.setTitle();
    this.env_url = this.global.getUploadsUrl();
  }

  setTitle() {
    this.titleService.setTitle('Our Work | ' + this.global.appTitle());
  }

  getProjects() {
    this.workService.getProjects()
      .subscribe(
        (projects: Project[]) => {
          this.projects = projects;
          // console.log(projects);

        },
        (error) => {
          console.log(error);
        }
      );
  }

}
