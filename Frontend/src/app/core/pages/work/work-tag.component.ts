import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Project} from '../../../shared/interfaces/project.interface';

@Component({
  selector: 'app-work-tag',
  templateUrl: './work-tag.component.html',
  animations: [
    listAnimationTrigger
  ],
})
export class WorkTagComponent implements OnInit, OnDestroy {

  // posts: Post[];
  projects: Project[];
  slug: any;
  tag: string;
  postPage: any;
  projectPage: any;

  constructor(private activeRoute: ActivatedRoute,
              private global: GlobalService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit() {
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );

    this.getData();
    this.setTitle();
    this.getTagName();
  }

  setTitle() {
    this.titleService.setTitle('Tag Archive | ' + this.global.appTitle());
  }

  getTagName() {
    this.slug = this.activeRoute.params.subscribe(params => {
      this.tag = params['slug'];
      this.tag = this.tag.replace(/[\-\]&]+/g, ' ');
      // console.log(this.tag);
    });
  }

  getData() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        this.projects = data.json;

        if (!this.projects) {
          return this.router.navigateByUrl('/page-not-found');
        }
      },
      (error) => {
        return this.router.navigateByUrl('/page-not-found');
      }
    );
  }

  ngOnDestroy() {
    this.slug.unsubscribe();
  }

}
