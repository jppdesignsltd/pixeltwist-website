import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-hosting',
  templateUrl: './hosting.component.html',
  styles: []
})
export class HostingComponent implements OnInit {

  constructor(
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Hosting | Pixel Twist');
  }

}
