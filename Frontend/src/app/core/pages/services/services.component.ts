import {Component, OnInit, Renderer2} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-what-we-do',
  templateUrl: './services.component.html',
  styles: []
})
export class ServicesComponent implements OnInit {

  constructor(private router: Router,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.renderer.removeClass(document.querySelector('body'), 'active');
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );
  }

}
