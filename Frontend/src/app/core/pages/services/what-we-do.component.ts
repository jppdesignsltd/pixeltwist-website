import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-what-we-do',
  templateUrl: './what-we-do.component.html',
  styles: []
})
export class WhatWeDoComponent implements OnInit {

  constructor(private titleService: Title,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('What we do | ' + this.global.appTitle());
  }

}
