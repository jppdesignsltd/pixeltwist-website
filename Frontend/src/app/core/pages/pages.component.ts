import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  encapsulation: ViewEncapsulation.Emulated
})
export class PagesComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.reset();
  }

  reset() {
    document.querySelector('body').classList.remove('active');

    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );
  }

}
