import {Component, Input, OnInit} from '@angular/core';
import {Project} from '../../../shared/interfaces/project.interface';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: '[app-project-block]',
  templateUrl: './project-block.component.html'
})

export class ProjectBlockComponent implements OnInit {

  @Input() project: Project;
  logoImageSrc: string; // Put default image link here
  env_url: string;

  constructor(private global: GlobalService) {
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();

    if (this.project.logo_image !== null) {
      this.logoImageSrc = this.env_url + 'uploads/projects/' + this.project.logo_image;
    } else {
      this.logoImageSrc = this.env_url + 'uploads/projects/default-logo.jpg';
    }
  }

}
