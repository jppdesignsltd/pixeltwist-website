import {Component, OnInit, Renderer2} from '@angular/core';

@Component({
  selector: 'app-process-module',
  templateUrl: './process-module.component.html'
})

export class ProcessModuleComponent implements OnInit {

  constructor(private renderer: Renderer2) {
  }

  ngOnInit() {
  }

  toggleProcess(number: number) {
    // Declare our variables
    const pb1 = document.getElementById('process-ball-1') as HTMLElement;
    const pb2 = document.getElementById('process-ball-2') as HTMLElement;
    const pb3 = document.getElementById('process-ball-3') as HTMLElement;
    const pb4 = document.getElementById('process-ball-4') as HTMLElement;

    const ps1 = document.getElementById('process-step-1') as HTMLElement;
    const ps2 = document.getElementById('process-step-2') as HTMLElement;
    const ps3 = document.getElementById('process-step-3') as HTMLElement;
    const ps4 = document.getElementById('process-step-4') as HTMLElement;

    // Remove all Active classes from elements in question
    this.renderer.removeClass(pb1, 'active');
    this.renderer.removeClass(pb2, 'active');
    this.renderer.removeClass(pb3, 'active');
    this.renderer.removeClass(pb4, 'active');
    this.renderer.removeClass(ps1, 'active');
    this.renderer.removeClass(ps2, 'active');
    this.renderer.removeClass(ps3, 'active');
    this.renderer.removeClass(ps4, 'active');

    switch (number) {
      case 1:
        pb1.classList.toggle('active');
        ps1.classList.toggle('active');
        break;

      case 2:
        pb2.classList.toggle('active');
        ps2.classList.toggle('active');
        break;

      case 3:
        pb3.classList.toggle('active');
        ps3.classList.toggle('active');
        break;

      case 4:
        pb4.classList.toggle('active');
        ps4.classList.toggle('active');
        break;
    }

  }

}
