import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../shared/services/global.service';
import {Post} from '../../../shared/interfaces/post.interface';

@Component({
  selector: '[app-post-block]',
  templateUrl: './post-block.component.html'
})

export class PostBlockComponent implements OnInit {

  @Input() post: Post;
  env_url: string;
  tags: any;

  constructor(private global: GlobalService) {
  }

  ngOnInit() {
    this.env_url = this.global.getUploadsUrl();
  }

}
