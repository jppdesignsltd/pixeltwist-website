import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../shared/services/global.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styles: []
})

export class AboutComponent implements OnInit {

  constructor(private titleService: Title,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('About | ' + this.global.appTitle());
  }

}
