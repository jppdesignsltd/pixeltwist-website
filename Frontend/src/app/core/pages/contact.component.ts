import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../shared/services/global.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styles: []
})
export class ContactComponent implements OnInit {

  constructor(private titleService: Title,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    this.titleService.setTitle('Contact | ' + this.global.appTitle());
  }

}
