import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {WorkService} from '../../../shared/services/work.service';
import {Project} from '../../../shared/interfaces/project.interface';
import {GlobalService} from '../../../shared/services/global.service';
import {Router} from '@angular/router';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    '../../../../dist/css/home.css'
  ],
  animations: [
    listAnimationTrigger
  ],
  encapsulation: ViewEncapsulation.None
})


export class HomeComponent implements OnInit {

  projects: Project[];
  env_url: string;

  constructor(private titleService: Title,
              private global: GlobalService,
              private workService: WorkService,
              private router: Router) {

  }

  ngOnInit() {
    // Change the meta title of the page
    this.setTitle();

    // Get env url for images
    this.env_url = this.global.getUploadsUrl();

    // Get Projects
    this.getProjects();

    // Fire some page resets
    this.reset();
  }

  setTitle() {
    this.titleService.setTitle('Home | ' + this.global.appTitle());
  }

  getProjects() {
    this.workService.getProjects()
      .subscribe(
        (projects: Project[]) => {
          this.projects = projects;
          // console.log(projects);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  reset() {
    // Fix for the menu
    document.querySelector('body').classList.remove('active');

    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );
  }
}
