import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {ActivatedRoute, Data, Router} from '@angular/router';

@Component({
  selector: 'app-blog-category',
  templateUrl: './blog-category.component.html',
  animations: [
    listAnimationTrigger
  ],
})
export class BlogCategoryComponent implements OnInit, OnDestroy {

  posts: Post[];
  slug: any;
  category: string;
  postPage: any;

  constructor(private activeRoute: ActivatedRoute,
              private global: GlobalService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit() {
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );

    this.getPosts();
    this.setTitle();
    this.getCategoryName();
  }

  setTitle() {
    this.titleService.setTitle('Category Archive | ' + this.global.appTitle());
  }

  getCategoryName() {
    this.slug = this.activeRoute.params.subscribe(params => {
      this.category = params['slug'];
      this.category = this.category.replace(/[\-\]&]+/g, ' ');
      // console.log(this.category);
    });
  }

  getPosts() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);

        this.posts = data.json.posts;
      }
    );
  }

  ngOnDestroy() {
    this.slug.unsubscribe();
  }

}
