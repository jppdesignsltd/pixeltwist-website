import {Component, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {listAnimationTrigger} from '../../../shared/animations/list.animation';
import {BlogService} from '../../../shared/services/blog.service';
import {Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  animations: [
    listAnimationTrigger
  ],
})
export class BlogComponent implements OnInit {

  posts: Post[];
  postPage: any;
  env_url: string;

  constructor(private blogService: BlogService,
              private global: GlobalService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit() {
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );

    this.getPosts();
    this.setTitle();
    this.env_url = this.global.getUploadsUrl();
  }

  setTitle() {
    this.titleService.setTitle('Our Blog | ' + this.global.appTitle());
  }

  getPosts() {
    this.blogService.getPosts()
      .subscribe(
        (posts: Post[]) => {

          this.posts = posts;
          // console.log(posts);

        },
        (error) => {
          console.log(error);
        }
      );
  }

}
