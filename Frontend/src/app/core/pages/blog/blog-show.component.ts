import {Component, OnInit} from '@angular/core';
import {Post} from '../../../shared/interfaces/post.interface';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {GlobalService} from '../../../shared/services/global.service';
import {User} from '../../../shared/interfaces/user.interface';
import {Category} from '../../../shared/interfaces/category.interface';
import {Location} from '@angular/common';


@Component({
  selector: 'app-blog-show',
  templateUrl: './blog-show.component.html',
  styles: []
})
export class BlogShowComponent implements OnInit {

  post: Post;
  author: User;
  category: Category;
  postTitle: string;
  meta_keywords: string;
  meta_description: string;

  constructor(private activeRoute: ActivatedRoute,
              private global: GlobalService,
              private metaService: Meta,
              private titleService: Title,
              private _location: Location,
              private router: Router) {
  }

  ngOnInit() {
    this.getPost();
  }

  backClicked() {
    this._location.back();
  }

  getPost() {
    this.activeRoute.data.subscribe(
      (data: Data) => {
        // console.log(data.json);
        this.post = data.json;

        if (this.post) {
          window.scrollTo(0, 0);

          this.author = this.post.user;
          this.category = this.post.category;
          this.postTitle = this.post.title;
          this.meta_keywords = this.post.meta_keywords;
          this.meta_description = this.post.meta_description;
          this.setTitle(this.postTitle);
          return this.setMeta();
        }

        return this.router.navigateByUrl('/page-not-found');
      }
    );
  }

  setTitle(pageTitle) {
    this.titleService.setTitle(pageTitle + ' ' + this.global.appTitle());
  }

  setMeta() {

    // Set Page author
    this.metaService.removeTag('name="author"');
    this.metaService.addTag({
      name: 'author',
      content: this.author.firstname + ' ' + this.author.lastname
    });

    // Set Page Keywords -- not that these are used much anymore
    if (this.meta_keywords !== null) {
      // console.log(this.meta_keywords);

      if (this.meta_keywords.length !== 0) {
        this.metaService.removeTag('name="keywords"');
        this.metaService.addTag({
          name: 'keywords',
          content: this.meta_keywords
        });
      }
    }

    // Set Page Description
    if (this.meta_description !== null) {
      // console.log(this.meta_description);

      if (this.meta_description.length !== 0) {
        this.metaService.removeTag('name="description"');
        this.metaService.addTag({
          name: 'description',
          content: this.meta_description
        });
      }
    }

  }

}
