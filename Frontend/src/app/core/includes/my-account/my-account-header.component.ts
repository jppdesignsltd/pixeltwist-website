import {Component, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';

@Component({
  selector: 'app-my-account-header',
  templateUrl: './my-account-header.component.html'
})
export class MyAccountHeaderComponent implements OnInit {

  loggedIn = false;

  constructor(private authService: AuthService,
              private renderer: Renderer2) {
  }

  ngOnInit() {

    if (this.authService.isLoggedIn()) {
      this.loggedIn = true;
    }

  }

  onLoad() {
    const token = this.authService.isLoggedIn();

    if (!token) {
      this.authService.logout();
    }
  }

  onLogout() {
    this.authService.logout();
  }
}
