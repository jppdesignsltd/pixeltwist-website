import {Component, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styles: []
})
export class AdminHeaderComponent implements OnInit {

  loggedIn = false;

  constructor(private authService: AuthService,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.loggedIn = true;
    }

  }

  onLoad() {
    const pageOverlay = document.getElementById('page-overlay') as HTMLElement;
    const sideNav = document.getElementById('side_nav-container') as HTMLElement;
    const adminHeader = document.getElementById('admin-header') as HTMLElement;
    const adminMain = document.getElementById('admin-main') as HTMLElement;
    const navToggle = document.getElementById('nav-toggle') as HTMLElement;
    const token = this.authService.isLoggedIn();

    if (!token) {
      this.authService.logout();
    }

    if (pageOverlay.classList.contains('active')) {
      this.renderer.removeClass(pageOverlay, 'active');
    }

    if (sideNav.classList.contains('active')) {
      this.renderer.removeClass(sideNav, 'active');
    }

    if (adminHeader.classList.contains('active')) {
      this.renderer.removeClass(adminHeader, 'active');
    }

    if (adminMain.classList.contains('active')) {
      this.renderer.removeClass(adminMain, 'active');
    }

    if (navToggle.classList.contains('active')) {
      this.renderer.removeClass(navToggle, 'active');
    }
  }

  onLogout() {
    this.authService.logout();
  }

  toggleSideNav() {
    const pageOverlay = document.getElementById('page-overlay') as HTMLElement;
    const sideNav = document.getElementById('side_nav-container') as HTMLElement;
    const adminHeader = document.getElementById('admin-header') as HTMLElement;
    const adminMain = document.getElementById('admin-main') as HTMLElement;
    const navToggle = document.getElementById('nav-toggle') as HTMLElement;

    pageOverlay.classList.toggle('active');
    sideNav.classList.toggle('active');
    adminHeader.classList.toggle('active');
    adminMain.classList.toggle('active');
    navToggle.classList.toggle('active');
  }

}
