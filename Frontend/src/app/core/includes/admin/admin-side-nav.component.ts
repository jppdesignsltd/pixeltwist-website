import {Component, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {UserService} from '../../../shared/services/user.service';
import {User} from '../../../shared/interfaces/user.interface';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-admin-side-nav',
  templateUrl: './admin-side-nav.component.html',
})
export class AdminSideNavComponent implements OnInit {

  role;
  firstname;
  lastname;
  private user: User;
  public userImage: string;
  public isCollapsedProjects = true;
  public isCollapsedPosts = true;
  public isCollapsedUsers = true;
  adminArray = ['admin'];
  authorArray = ['author'];
  editorArray = ['admin', 'editor'];
  projectArray = ['admin', 'editor', 'author'];
  postArray = ['admin', 'editor', 'author'];

  constructor(private authService: AuthService,
              private userService: UserService,
              private global: GlobalService,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.role = this.authService.getRole();
    this.getUser();
  }

  getUser() {
    this.userService.getUser()
      .subscribe(
        (user: User) => {

          this.user = user;
          this.firstname = user.firstname;
          this.lastname = user.lastname;

          // console.log(this.user);

          if (this.user.avatar) {
            this.userImage = this.global.getUploadsUrl() + '/uploads/profiles/' + this.user.avatar;
          } else {
            this.userImage = '/dist/img/default_user.png';
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  onLoad() {
    const pageOverlay = document.getElementById('page-overlay') as HTMLElement;
    const sideNav = document.getElementById('side_nav-container') as HTMLElement;
    const adminHeader = document.getElementById('admin-header') as HTMLElement;
    const adminMain = document.getElementById('admin-main') as HTMLElement;
    const navToggle = document.getElementById('nav-toggle') as HTMLElement;
    const token = this.authService.isLoggedIn();

    if (!token) {
      this.authService.logout();
    }

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {

      if (pageOverlay.classList.contains('active')) {
        this.renderer.removeClass(pageOverlay, 'active');
      }

      if (sideNav.classList.contains('active')) {
        this.renderer.removeClass(sideNav, 'active');
      }

      if (adminHeader.classList.contains('active')) {
        this.renderer.removeClass(adminHeader, 'active');
      }

      if (adminMain.classList.contains('active')) {
        this.renderer.removeClass(adminMain, 'active');
      }

      if (navToggle.classList.contains('active')) {
        this.renderer.removeClass(navToggle, 'active');
      }

    }
  }
}
