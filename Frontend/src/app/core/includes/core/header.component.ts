import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {User} from '../../../shared/interfaces/user.interface';
import {UserService} from '../../../shared/services/user.service';
import {LoadingService} from '../../../shared/services/loading.service';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  private token: boolean;
  role: string;
  adminArray = ['admin'];
  authorArray = ['author'];
  editorArray = ['admin', 'editor'];
  projectArray = ['admin', 'editor', 'author'];
  postArray = ['admin', 'editor', 'author'];
  user: User;
  userImage = '/dist/img/default_user.png';
  showAdminPanel = false;

  constructor(private authService: AuthService,
              private renderer: Renderer2,
              private global: GlobalService,
              private loadingService: LoadingService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.token = this.authService.isLoggedIn();
    this.role = this.authService.getRole();

    if (this.token) {
      this.userService.getUser()
        .subscribe(
          (user: User) => {
            this.user = user;

            if (this.user.avatar) {
              this.userImage = this.global.getUploadsUrl() + '/uploads/profiles/' + this.user.avatar;
            }

            if (this.role === 'standard') {
              return this.adminPanel(false);
            }

            return this.adminPanel(true);
          },
          (error) => {
            // console.log(error);
            localStorage.clear();
          }
        );
    } else {
      localStorage.clear();
    }
  }

  toggleMenu() {
    const menuToggle = document.getElementById('menu-toggle') as HTMLElement;
    const menu = document.getElementById('menu') as HTMLElement;
    const page = document.getElementById('page') as HTMLElement;
    const pushWrapper = document.getElementById('push-wrapper') as HTMLElement;
    const body = document.querySelector('body') as HTMLElement;

    menuToggle.classList.toggle('open');
    menu.classList.toggle('open');
    pushWrapper.classList.toggle('active');
    body.classList.toggle('active');
    page.classList.toggle('active');

    if (body.classList.contains('active')) {
      this.renderer.setStyle(body, 'height', window.innerHeight + 'px');
      this.renderer.setStyle(page, 'height', window.innerHeight + 'px');
      // this.renderer.setStyle(pushWrapper, 'height', window.innerHeight + 'px');
    } else {
      this.renderer.setStyle(body, 'height', '100%');
      this.renderer.setStyle(page, 'height', '100%');
      // this.renderer.setStyle(pushWrapper, 'height', '100%');
    }
  }

  adminPanel(state) {
    if (state) {
      return this.showAdminPanel = true;
    }

    return this.showAdminPanel = false;
  }

  showLoading() {
    this.loadingService.showLoading();
  }

  logout() {
    this.authService.logout();
    this.showAdminPanel = false;
  }

}
