import {Component, OnInit, Renderer2} from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styles: []
})
export class NavComponent implements OnInit {

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  toggleMenu() {
    const menuToggle = document.getElementById('menu-toggle') as HTMLElement;
    const menu = document.getElementById('menu') as HTMLElement;
    const page = document.getElementById('page') as HTMLElement;
    const pushWrapper = document.getElementById('push-wrapper') as HTMLElement;
    const body = document.querySelector('body') as HTMLElement;

    menuToggle.classList.toggle('open');
    menu.classList.toggle('open');
    pushWrapper.classList.toggle('active');
    body.classList.toggle('active');
    page.classList.toggle('active');

    if (body.classList.contains('active')) {
      this.renderer.setStyle(body, 'height', window.innerHeight + 'px');
      this.renderer.setStyle(page, 'height', window.innerHeight + 'px');
      // this.renderer.setStyle(pushWrapper, 'height', window.innerHeight + 'px');
    } else {
      this.renderer.setStyle(body, 'height', '100%');
      this.renderer.setStyle(page, 'height', '100%');
      // this.renderer.setStyle(pushWrapper, 'height', '100%');
    }
  }
}
