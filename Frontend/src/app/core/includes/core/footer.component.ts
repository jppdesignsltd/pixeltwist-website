import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  loggedIn = false;
  canLogout = false;
  hasAdminAccess = false;
  hasMyAccountAccess = false;
  token;

  constructor(private authService: AuthService,
              private loadingService: LoadingService) {
  }

  showLoading() {
    this.loadingService.showLoading();
  }

  ngOnInit() {
    this.token = this.authService.getToken();

    if (this.token) {
      if (this.authService.getRole() === 'standard') {

        this.hasMyAccountAccess = true;

      } else {

        this.hasAdminAccess = true;

      }

      this.canLogout = true;
      return this.loggedIn = true;
    }
  }

  onLogout() {
    this.authService.logout();
  }

}
