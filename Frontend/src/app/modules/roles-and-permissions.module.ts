import {NgModule} from '@angular/core';

import {SharedModule} from './shared.module';
import {RolesAndPermissionsComponent} from '../core/admin/roles-and-permissions/roles-and-permissions.component';
import {NewRoleComponent} from '../core/admin/roles-and-permissions/new-role.component';
import {NewPermissionComponent} from '../core/admin/roles-and-permissions/new-permission.component';
import {RoleComponent} from '../core/admin/roles-and-permissions/role.component';
import {PermissionComponent} from '../core/admin/roles-and-permissions/permission.component';

@NgModule({
  declarations: [
    RolesAndPermissionsComponent,
    NewRoleComponent,
    NewPermissionComponent,
    RoleComponent,
    PermissionComponent
  ],
  imports: [
    SharedModule
  ]
})

export class RolesAndPermissionsModule {
}
