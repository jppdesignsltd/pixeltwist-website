import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';
import {ShowCategoryComponent} from '../core/admin/categories/show-category.component';
import {CategoriesComponent} from '../core/admin/categories/categories.component';
import {CategoryComponent} from '../core/admin/categories/category.component';
import {ShowCategoryPostsComponent} from '../core/admin/categories/show-category-posts.component';

@NgModule({
  declarations: [
    ShowCategoryComponent,
    CategoriesComponent,
    CategoryComponent,
    ShowCategoryPostsComponent
  ],
  imports: [
    SharedModule
  ]
})

export class CategoryModule {}
