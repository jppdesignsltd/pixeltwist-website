import {NgModule} from '@angular/core';

import {PostComponent} from '../core/admin/posts/post.component';
import {PostsComponent} from '../core/admin/posts/posts.component';
import {NewPostComponent} from '../core/admin/posts/new-post.component';
import {EditPostComponent} from '../core/admin/posts/edit-post.component';
import {ShowPostComponent} from '../core/admin/posts/show-post.component';
import {SharedModule} from './shared.module';
import {MyPostsComponent} from '../core/admin/posts/my-posts.component';

@NgModule({
  declarations: [
    PostComponent,
    PostsComponent,
    NewPostComponent,
    EditPostComponent,
    ShowPostComponent,
    MyPostsComponent
  ],
  imports: [
    SharedModule
  ]
})

export class PostModule {}
