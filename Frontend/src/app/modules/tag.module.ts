import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';
import {TagComponent} from 'app/core/admin/tags/tag.component';
import {TagsComponent} from 'app/core/admin/tags/tags.component';
import {ShowTagComponent} from '../core/admin/tags/show-tag.component';
import {ShowTagPostsComponent} from '../core/admin/tags/show-tag-posts.component';
import {ShowTagProjectsComponent} from '../core/admin/tags/show-tag-projects.component';
import {NewTagComponent} from '../core/admin/tags/new-tag.component';

@NgModule({
  declarations: [
    TagsComponent,
    TagComponent,
    NewTagComponent,
    ShowTagComponent,
    ShowTagPostsComponent,
    ShowTagProjectsComponent
  ],
  imports: [
    SharedModule
  ]
})

export class TagModule {}
