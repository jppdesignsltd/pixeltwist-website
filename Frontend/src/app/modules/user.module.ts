import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';

import {NewUserComponent} from '../core/admin/users/new-user.component';
import {EditUserComponent} from '../core/admin/users/edit-user.component';
import {UserProfileComponent} from '../core/admin/profile/user-profile.component';
import {UsersComponent} from '../core/admin/users/users.component';
import {UserComponent} from '../core/admin/users/user.component';

@NgModule({
  declarations: [
    UserComponent,
    UsersComponent,
    UserProfileComponent,
    EditUserComponent,
    NewUserComponent
  ],
  imports: [
    SharedModule
  ]
})

export class UserModule {}
