import {NgModule} from '@angular/core';
import {TincymceComponent} from '../shared/plugins/tincymce/tincymce.component';
import {CommonModule} from '@angular/common';
import {NgPipesModule} from 'ngx-pipes';
import {MyDatePickerModule} from 'mydatepicker';
import {Select2Module} from 'ng2-select2';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ErrorMessagesComponent} from '../shared/plugins/error-messages.component';
import {LoadingComponent} from '../shared/plugins/loading.component';
import {PipeModule} from './pipe.module';
import {ParallaxScrollDirective} from '../shared/directives/parallax-scroll.directive';
import {CountUpDirective} from '../shared/directives/countUp.directive';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    TincymceComponent,
    ErrorMessagesComponent,
    LoadingComponent,
    ParallaxScrollDirective,
    CountUpDirective
  ],
  exports: [
    ParallaxScrollDirective,
    CountUpDirective,
    ErrorMessagesComponent,
    NgbModule,
    TincymceComponent,
    LoadingComponent,
    CommonModule,
    NgPipesModule,
    PipeModule,
    MyDatePickerModule,
    Select2Module,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ]
})

export class SharedModule {
}
