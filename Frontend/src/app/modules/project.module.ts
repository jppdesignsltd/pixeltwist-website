import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';
import {ProjectsComponent} from '../core/admin/projects/projects.component';
import {ProjectComponent} from '../core/admin/projects/project.component';
import {EditProjectComponent} from '../core/admin/projects/edit-project.component';
import {ShowProjectComponent} from '../core/admin/projects/show-project.component';
import {NewProjectComponent} from '../core/admin/projects/new-project.component';
import {MyProjectsComponent} from '../core/admin/projects/my-projects.component';

@NgModule({
  declarations: [
    ProjectsComponent,
    ProjectComponent,
    EditProjectComponent,
    ShowProjectComponent,
    NewProjectComponent,
    MyProjectsComponent,
  ],
  imports: [
    SharedModule
  ]
})

export class ProjectModule {}
