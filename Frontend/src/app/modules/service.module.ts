import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';
import {AdminServicesComponent} from '../core/admin/services/admin-services.component';
import {ServiceComponent} from '../core/admin/services/service.component';
import {ShowServiceComponent} from '../core/admin/services/show-service.component';
import {ShowServiceProjectsComponent} from '../core/admin/services/show-service-projects.component';


@NgModule({
  declarations: [
    AdminServicesComponent,
    ServiceComponent,
    ShowServiceComponent,
    ShowServiceProjectsComponent
  ],
  imports: [
    SharedModule
  ]
})

export class ServiceModule {}
