import {NgModule} from '@angular/core';
import {AdminRoutingModule} from '../routing/admin-routing.module';
import {SharedModule} from './shared.module';
import {PostModule} from './post.module';
import {UserModule} from './user.module';
import {ProjectModule} from './project.module';
import {CategoryModule} from './category.module';
import {TagModule} from './tag.module';

import {AdminComponent} from '../core/admin/admin.component';
import {AdminFooterComponent} from '../core/includes/admin/admin-footer.component';
import {AdminHeaderComponent} from '../core/includes/admin/admin-header.component';
import {DashboardComponent} from '../core/admin/dashboard.component';
import {AdminSideNavComponent} from '../core/includes/admin/admin-side-nav.component';
import {ProjectResolver} from '../shared/resolvers/project-resolver.service';
import {PostService} from '../shared/services/post.service';
import {ProjectService} from '../shared/services/project.service';
import {TagService} from '../shared/services/tag.service';
import {CategoryService} from '../shared/services/category.service';
import {RoleService} from '../shared/services/role.service';
import {PostResolver} from '../shared/resolvers/post-resolver.service';
import {CanDeactivateGuard} from '../shared/guards/can-deactivate.guard';
import {AuthGuard} from '../shared/guards/auth.guard';
import {CategoryResolver} from '../shared/resolvers/category-resolver.service';
import {TagResolver} from '../shared/resolvers/tag-resolver.service';
import {UserResolver} from '../shared/resolvers/user-resolver.service';
import {AuthorService} from '../shared/services/author.service';
import {RolesAndPermissionsModule} from './roles-and-permissions.module';
import {PermissionService} from '../shared/services/permission.service';
import {EditorGuard} from 'app/shared/guards/editor.guard';
import {AdminGuard} from '../shared/guards/admin.guard';
import {AuthorGuard} from '../shared/guards/author.guard';
import {ServiceResolver} from '../shared/resolvers/service-resolver.service';
import {ServiceService} from '../shared/services/service.service';
import {ServiceModule} from './service.module';

@NgModule({
  declarations: [
    AdminSideNavComponent,
    DashboardComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminComponent,
  ],
  imports: [
    AdminRoutingModule,
    SharedModule
  ],
  exports: [
    PostModule,
    RolesAndPermissionsModule,
    ProjectModule,
    CategoryModule,
    TagModule,
    ServiceModule,
    UserModule
  ],
  providers: [
    AuthGuard,
    AuthorGuard,
    EditorGuard,
    AdminGuard,
    CanDeactivateGuard,
    AuthorService,
    PostService,
    ProjectService,
    TagService,
    ServiceService,
    CategoryService,
    RoleService,
    PermissionService,
    ServiceResolver,
    PostResolver,
    ProjectResolver,
    CategoryResolver,
    TagResolver,
    UserResolver
  ]
})

export class AdminModule {}
