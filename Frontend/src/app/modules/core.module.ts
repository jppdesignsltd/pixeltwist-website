import {NgModule} from '@angular/core';
import {PageRoutingModule} from '../routing/page-routing.module';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from '../core/includes/core/header.component';
import {NavComponent} from 'app/core/includes/core/nav.component';
import {FooterComponent} from '../core/includes/core/footer.component';
import {PageCtaComponent} from '../core/pages/modular/page-cta.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {PagesComponent} from '../core/pages/pages.component';
import {NotFoundComponent} from '../core/pages/errors/not-found.component';
import {BlogShowComponent} from '../core/pages/blog/blog-show.component';
import {BlogComponent} from '../core/pages/blog/blog.component';
import {ServicesComponent} from '../core/pages/services/services.component';
import {OurWorkComponent} from '../core/pages/work/our-work.component';
import {OurWorkShowComponent} from '../core/pages/work/our-work-show.component';
import {WebDevelopmentComponent} from '../core/pages/services/web-development.component';
import {WhatWeDoComponent} from '../core/pages/services/what-we-do.component';
import {SEOComponent} from '../core/pages/services/seo.component';
import {HostingComponent} from '../core/pages/services/hosting.component';
import {AppRoutingModule} from '../routing/routing.module';
import {WorkService} from '../shared/services/work.service';
import {BlogService} from '../shared/services/blog.service';
import {AuthService} from 'app/shared/services/auth.service';
import {UserService} from '../shared/services/user.service';
import {AboutComponent} from '../core/pages/about.component';
import {ContactComponent} from '../core/pages/contact.component';
import {PipeModule} from './pipe.module';
import {GraphicDesignComponent} from '../core/pages/services/graphic-design.component';
import {ProjectBlockComponent} from '../core/pages/modular/project-block.component';
import {PostBlockComponent} from '../core/pages/modular/post-block.component';
import {SharedModule} from './shared.module';
import {WorkResolver} from '../shared/resolvers/work-resolver.service';
import {BlogResolver} from '../shared/resolvers/blog-resolver.service';
import {BlogTagComponent} from '../core/pages/blog/blog-tag.component';
import {BlogTagResolver} from '../shared/resolvers/blog-tag-resolver.service';
import {BlogCategoryResolver} from '../shared/resolvers/blog-category-resolver.service';
import {BlogCategoryComponent} from '../core/pages/blog/blog-category.component';
import {WorkServiceComponent} from '../core/pages/work/work-services.component';
import {WorkTagComponent} from '../core/pages/work/work-tag.component';
import {WorkTagResolver} from '../shared/resolvers/work-tag-resolver.service';
import {WorkServiceResolver} from '../shared/resolvers/work-service-resolver.service';
import {BreadcrumbsComponent} from '../shared/plugins/breadcrumbs.component';
import {HomeComponent} from '../core/pages/home/home.component';
import {ProcessModuleComponent} from '../core/pages/modular/process-module.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavComponent,
    PageCtaComponent,
    ProcessModuleComponent,
    PagesComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    NotFoundComponent,
    BlogComponent,
    BlogTagComponent,
    BlogCategoryComponent,
    BlogShowComponent,
    ServicesComponent,
    OurWorkComponent,
    OurWorkShowComponent,
    WorkServiceComponent,
    WorkTagComponent,
    WebDevelopmentComponent,
    GraphicDesignComponent,
    WhatWeDoComponent,
    SEOComponent,
    HostingComponent,
    ProjectBlockComponent,
    PostBlockComponent,
    BreadcrumbsComponent
  ],
  imports: [
    AppRoutingModule,
    PageRoutingModule,
    SharedModule,
    CommonModule,
    PipeModule,
    NgxPaginationModule
  ],
  exports: [
    AppRoutingModule,
  ],
  providers: [
    UserService,
    WorkService,
    BlogService,
    AuthService,
    WorkResolver,
    WorkTagResolver,
    WorkServiceResolver,
    BlogResolver,
    BlogTagResolver,
    BlogCategoryResolver
  ]
})

export class CoreModule {
}
