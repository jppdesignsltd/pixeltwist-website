import {NgModule} from '@angular/core';
import {SharedModule} from './shared.module';
import {StandardAccountComponent} from '../core/standard-account/standard-account.component';
import {MyAccountRoutingModule} from '../routing/my-account-routing.module';
import {MyAccountComponent} from '../core/standard-account/my-account.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {MyAccountHeaderComponent} from '../core/includes/my-account/my-account-header.component';
import {MyAccountFooterComponent} from '../core/includes/my-account/my-account-footer.component';

@NgModule({
  declarations: [
    StandardAccountComponent,
    MyAccountComponent,
    MyAccountHeaderComponent,
    MyAccountFooterComponent
  ],
  imports: [
    MyAccountRoutingModule,
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule
  ],
  exports: [],
  providers: []
})

export class StandardAccountModule {
}
