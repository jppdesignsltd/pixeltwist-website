import {NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {ResetPasswordComponent} from '../core/auth/reset-password/reset-password.component';
import {ForgotPasswordComponent} from '../core/auth/forgot-password/forgot-password.component';
import {SigninComponent} from '../core/auth/signin/signin.component';
import {SignupComponent} from '../core/auth/signup/signup.component';
import {AuthComponent} from '../core/auth/auth.component';
import {AuthRoutingModule} from '../routing/auth-routing.module';
import {SharedModule} from './shared.module';


@NgModule({
  declarations: [
    AuthComponent,
    SignupComponent,
    SigninComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    AuthRoutingModule,
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule
  ]
})

export class AuthModule {}
