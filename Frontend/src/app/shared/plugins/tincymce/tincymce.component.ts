import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';

@Component({
  selector: 'app-tinymce',
  template: `
    <textarea id="{{ elementId }}" style="height:200px"></textarea>
  `
})
export class TincymceComponent implements AfterViewInit, OnDestroy {

  @Input() elementId: string;
  @Input() content: string;
  @Input() initialContent: string;
  @Output() onEditorKeyup = new EventEmitter<any>();
  editor;

  ngAfterViewInit() {
    tinymce.init({
      selector: '#' + this.elementId,
      plugins: ['link', 'wordcount', 'hr', 'fullscreen', 'lists', 'advlist'],
      skin_url: '../../../bower_components/tinymce/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyup.emit(content);
        });
      },
      init_instance_callback: (editor: any) => {
        editor && this.initialContent && this.editor.setContent(this.initialContent)
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }

}
