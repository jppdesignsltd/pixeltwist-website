import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-messages',
  template: `
    <div class="clearfix"></div>
    <div id="error-messages" class="alert d-none nmbottom nb-radius">
      <ul id="errors" class="list-unstyled nmbottom">
      </ul>
    </div>
    <div class="clearfix"></div>
  `
})
export class ErrorMessagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
