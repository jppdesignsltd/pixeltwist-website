import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-loading',
  template: `
    <div id="loading-container" class="d-none">
      <div class="loading-body">
        <div class="loading-content">
          <i class="fa fa-spinner fa-spin fa-2x"></i>
        </div>
      </div>
    </div>
  `
})
export class LoadingComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
