import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Breadcrumb} from '../interfaces/breadcrumb.interface';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-breadcrumbs',
  template: `
    <div class="clearfix"></div>

    <div id="breadcrumbs-container">
      <div class="container">
        <ol class="breadcrumbs-list list-inline">
          <li class="breadcrumbs-item list-inline-item">
            <a routerLink="/">
              Home
            </a>
          </li>

          <li *ngFor="let breadcrumb of breadcrumbs" class="text-capitalize list-inline-item">

            <div *ngIf="excludeArray.indexOf(breadcrumb.label)===-1">

              <a *ngIf="breadcrumb.url !== ''" [routerLink]="'/' + breadcrumb.url">
                {{ breadcrumb.label }}
              </a>

              <span *ngIf="breadcrumb.url === ''">
                {{ breadcrumb.label }}
              </span>

            </div>

          </li>
        </ol>
      </div>
    </div>

    <div class="clearfix"></div>
  `
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {

  breadcrumbs: Breadcrumb[] = [];
  excludeArray = ['tag', 'service', 'category'];
  url: any;

  constructor(private activeRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.getUrlSegments();

    this.router.events.subscribe(
      () => {
        this.breadcrumbs = [];
        this.getUrlSegments();
      }
    );
  }

  getUrlSegments() {
    this.url = this.activeRoute.url.subscribe(
      url => {
        return this.getBreadcrumbs(url);
      }
    );
  }

  getBreadcrumbs(url) {
    for (let i = 0, len = url.length; i < len; i++) {
      let sanitisePath = url[i].path.replace(/[\-\]&]+/g, ' ');
      let urlPath;

      if (sanitisePath === 'projects') {
        urlPath = 'work';
        sanitisePath = 'Our Work';
      } else if (sanitisePath === 'blog') {
        urlPath = 'journal';
        sanitisePath = 'Our Journal';
      } else if (sanitisePath === 'services') {
        urlPath = 'what-we-do';
        sanitisePath = 'What we do';
      } else {
        urlPath = '';
      }

      const breadcrumb = {
        'label': sanitisePath,
        'url': urlPath
      };

      // console.log(breadcrumb);
      // console.log(this.breadcrumbs);

      this.breadcrumbs.push(breadcrumb);
    }
  }

  ngOnDestroy() {
    this.url.unsubscribe();
  }
}
