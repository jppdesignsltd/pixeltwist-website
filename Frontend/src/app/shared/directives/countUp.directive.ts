import {Directive, Input, Output, EventEmitter, ViewContainerRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appCountUp]'
})

export class CountUpDirective implements OnInit {

  @Output() countUpChange = new EventEmitter();
  @Output() countUpEnd = new EventEmitter();
  @Input() duration: number;
  @Input() countTo: number;
  @Input() countFrom: number;
  @Input() step: number;
  el;

  constructor(private viewContainer: ViewContainerRef) {
  }

  ngOnInit() {
    this.el = this.viewContainer.element.nativeElement;
    this.countUp();
  }


  countUp() {
    let start = this.countFrom;
    const end = this.countTo;
    const range = end - start;
    let increment;

    // if (this.decimalChecker(end)) {
    //   increment = end > start ? 0.1 : -0.1;
    // } else {
    // }
    increment = end > start ? 1 : -1;

    const duration = this.duration + '00';
    const step = parseInt(duration, 10) / range;
    const stepTime = Math.abs(Math.floor(step));

    this.el.textContent = this.countFrom;

    // console.log(stepTime);
    // console.log(step);
    // console.log(start / end);

    const timer = setInterval(() => {

      start += increment;
      this.el.textContent = start;

      if (start === end) {
        clearInterval(timer);
      }

    }, stepTime);
  }

  // decimalChecker(val) {
  //   if (val % 1 !== 0) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
