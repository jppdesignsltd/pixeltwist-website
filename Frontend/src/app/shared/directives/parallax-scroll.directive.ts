import {ViewContainerRef, Directive, AfterViewInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appParallax], appParallax'
})
export class ParallaxScrollDirective implements AfterViewInit {

  el: HTMLElement;
  img: HTMLImageElement;
  imgOrgWidth: any;
  imgOrgHeight: any;

  constructor(private viewContainer: ViewContainerRef,
              private renderer: Renderer2) {

    this.el = this.viewContainer.element.nativeElement;
    // console.log(this.el);
  }

  ngAfterViewInit() {
    // console.log('working');

    this.img = <HTMLImageElement>this.el.querySelector('img');

    this.img.onload = () => {

      this.imgOrgWidth = this.imgOrgWidth || this.img.offsetWidth;
      this.imgOrgHeight = this.imgOrgHeight || this.img.offsetHeight;

      // console.log(this.img.offsetWidth, this.img.offsetHeight, this.img.width, this.img.height);

      this.setParallaxImage();
      this.updateParallaxImage();

      window.addEventListener('scroll', () => {
        this.updateParallaxImage();
      });

      window.addEventListener('resize', () => {
        this.setParallaxImage();
        this.updateParallaxImage();
      })
    };

    // console.log(this.img);
  }

  private setParallaxImage() {
    // Wrap image with a div, then set style
    let imgWrapperEl: HTMLElement = <HTMLElement>this.el.querySelector('.parallax-img-wrapper');

    // If no imgWrapper then create it and append img to it
    if (!imgWrapperEl) {
      imgWrapperEl = this.renderer.createElement('div');
      this.renderer.addClass(imgWrapperEl, 'parallax-img-wrapper');
      this.renderer.appendChild(imgWrapperEl, this.img);
      this.renderer.appendChild(this.el, imgWrapperEl);
    }

    // Set imgWrapper styles
    this.renderer.setStyle(imgWrapperEl, 'position', 'absolute');
    this.renderer.setStyle(imgWrapperEl, 'z-index', '-1');
    this.renderer.setStyle(imgWrapperEl, 'top', '0');
    this.renderer.setStyle(imgWrapperEl, 'bottom', '0');
    this.renderer.setStyle(imgWrapperEl, 'left', '0');
    this.renderer.setStyle(imgWrapperEl, 'right', '0');

    // Set Image Styles
    this.renderer.setStyle(this.img, 'display', 'block');
    this.renderer.setStyle(this.img, 'position', 'absolute');
    this.renderer.setStyle(this.img, 'height', 'auto');
    this.renderer.setStyle(this.img, 'top', '0');

    // if (this.imgOrgWidth > this.el.offsetWidth) {
    //   this.renderer.setStyle(this.img, 'width', this.el.offsetWidth + 'px');
    // } else {
    //   this.renderer.setStyle(this.img, 'height', this.imgOrgHeight + 'px');
    // }

    // Set Container Style
    this.renderer.setStyle(this.el, 'z-index', '99');
    this.renderer.setStyle(this.el, 'overflow', 'd-none');
    this.renderer.setStyle(this.el, 'position', 'relative');
    this.renderer.setStyle(this.el, 'height', (this.img.height - this.img.offsetTop) + 'px');
  }

  private updateParallaxImage(): void {
    const elRect = this.el.getBoundingClientRect();
    const imgRect = this.img.getBoundingClientRect();

    const imgDist = imgRect.height - elRect.height;
    const bottom = this.el.offsetTop + elRect.height;
    const top = this.el.offsetTop;
    const scrollTop = document.body.scrollTop;
    const windowBottom = scrollTop + window.innerHeight;
    const percentScrolled = (windowBottom - top) / (elRect.height + window.innerHeight);
    const parallax = Math.round((imgDist * percentScrolled));

    // if ((bottom > scrollTop) && (top < (scrollTop + window.innerHeight))) {
      this.renderer.setStyle(this.img, 'bottom', parallax * -1 + 'px');
    // }
  }
}
