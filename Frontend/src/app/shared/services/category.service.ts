import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import {Category} from '../interfaces/category.interface';
import 'rxjs/Rx';

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getCategories(): Observable<any> {
    return this.http.get<Category[]>(this.global.getUrl() + 'category')
      .map(
        (categories) => {
          return categories;
        }
      );
  }

  createCategory(form) {
    const body = JSON.stringify(form);

    return this.http.post<Category>(this.global.getUrl() + 'category', body)
      .map(
        (category) => {
          return category;
        }
      )
      .catch(error => Observable.throw(error));
  }

  getSingleCategory(id: number): Observable<any> {
    return this.http.get<Category>(this.global.getUrl() + 'category/' + id)
      .map(
        (category) => {
          return category;
        }
      );
  }

  updateCategory(id: number, title, slug) {
    const body = JSON.stringify({
      title: title,
      slug: slug
    });

    return this.http.put(this.global.getUrl() + 'category/' + id, body)
      .map(
        (category) => {
          return category;
        }
      );
  }

  deleteCategory(id: number) {
    return this.http.delete(this.global.getUrl() + 'category/' + id);
  }

}
