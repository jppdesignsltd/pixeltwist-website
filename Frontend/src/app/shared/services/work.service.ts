import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import 'rxjs/Rx';
import {Project} from '../interfaces/project.interface';

@Injectable()
export class WorkService {
  constructor(private http: HttpClient,
              private router: Router,
              private global: GlobalService) {
  }

  getProjects(): Observable<any> {
    return this.http.get<Project[]>(this.global.getUrl() + 'work')
      .map(
        (projects) => {
          return projects;
        }
      );
  }

  getTagProjects(slug: string): Observable<any> {
    return this.http.get<Project>(this.global.getUrl() + 'work/tag/' + slug)
      .map(
        (projects) => {
          return projects;
        }
      );
  }

  getServiceProjects(slug: string): Observable<any> {
    return this.http.get<Project[]>(this.global.getUrl() + 'work/service/' + slug)
      .map(
        (projects) => {
          return projects;
        }
      )
  }

  getSingleProject(slug: string): Observable<any> {
    return this.http.get<Project>(this.global.getUrl() + 'work/' + slug)
      .map(
        (project) => {
          return project;
        }
      );
  }

}
