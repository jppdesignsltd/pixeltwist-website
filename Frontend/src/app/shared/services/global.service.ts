import {Injectable} from '@angular/core';

@Injectable()

export class GlobalService {

  env = 'live'; // Environment is either 'dev'/'live';

  constructor() {
  }

  appTitle() {
    return 'Pixel Twist | Set the Standard';
  }

  getUrl() {
    if (this.env === 'live') {
      return 'http://api.pixel-twist.co.uk/api/';
    }

    if (this.env === 'dev') {
      return 'http://pixeltwist.test/api/';
    }
  }

  getUploadsUrl() {
    if (this.env === 'live') {
      return 'http://api.pixel-twist.co.uk/';
    }

    if (this.env === 'dev') {
      return 'http://pixeltwist.test/';
    }
  }
}
