import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {AuthService} from './auth.service';

@Injectable()
export class AuthorService {
  constructor(private http: Http,
              private authService: AuthService,
              private global: GlobalService) {
  }

  getAuthors(): Observable<any> {
    const token = this.authService.getToken();

    return this.http.get(this.global.getUrl() + 'authors?token=' + token)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

}
