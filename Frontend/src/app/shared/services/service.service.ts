import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import {Services} from '../interfaces/service.interface';
import 'rxjs/Rx';


@Injectable()
export class ServiceService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getServices(): Observable<any> {
    return this.http.get<Services[]>(this.global.getUrl() + 'service')
      .map(
        (services) => {
          return services;
        }
      );
  }


  createService(form) {
    const body = JSON.stringify(form);

    return this.http.post<Services>(this.global.getUrl() + 'service', body)
      .map(
        (service) => {
          return service;
        }
      );
  }

  getSingleService(id: number): Observable<any> {
    return this.http.get<Services>(this.global.getUrl() + 'service/' + id)
      .map(
        (service) => {
          return service;
        }
      );
  }

  updateService(id: number, title, slug) {
    const body = JSON.stringify({
      title: title,
      slug: slug
    });

    return this.http.put<Services>(this.global.getUrl() + 'service/' + id , body).map(
      (response) => {
        return response;
      }
    );
  }

  deleteService(id: number) {
    return this.http.delete(this.global.getUrl() + 'service/' + id);
  }

}
