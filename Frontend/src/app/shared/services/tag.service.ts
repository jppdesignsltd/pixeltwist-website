import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import 'rxjs/Rx';
import {HttpClient} from '@angular/common/http';
import {Tags} from '../interfaces/tags.interface';


@Injectable()
export class TagService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getTags(): Observable<any> {
    return this.http.get<Tags[]>(this.global.getUrl() + 'tag')
      .map(
        (tags) => {
          return tags;
        }
      );
  }

  createTag(form) {
    const body = JSON.stringify(form);

    return this.http.post<Tags>(this.global.getUrl() + 'tag', body)
      .map(
        (tag) => {
          return tag;
        }
      );
  }

  getSingleTag(id: number): Observable<any> {
    return this.http.get<Tags>(this.global.getUrl() + 'tag/' + id)
      .map(
        (tag) => {
          return tag;
        }
      );
  }

  updateTag(id: number, title, slug) {
    const body = JSON.stringify({
      title: title,
      slug: slug
    });


    return this.http.put<Tags>(this.global.getUrl() + 'tag/' + id , body).map(
      (tag) => {
        return tag;
      }
    );
  }

  deleteTag(id: number) {
    return this.http.delete(this.global.getUrl() + 'tag/' + id);
  }

}
