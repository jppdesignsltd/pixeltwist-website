import {Injectable} from '@angular/core';

@Injectable()
export class ErrorService {

  constructor() {
  }

  resetErrors() {
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;

    // Reset Errors Container
    errorCont.classList.remove('alert-danger');
    errorCont.classList.remove('alert-success');
    errorCont.classList.add('d-none');

    while (errorUl.firstChild) {
      errorUl.removeChild(errorUl.firstChild);
    }
  }

  passwordHandler(event, current) {
    const password_confirmation = document.getElementById('password_confirmation') as HTMLElement;
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;
    let liTag;

    password_confirmation.classList.add('ng-invalid');
    password_confirmation.classList.remove('ng-valid');

    // console.log(event);
    // console.log(current);

    if (event !== current) {
      password_confirmation.classList.add('ng-invalid');
      password_confirmation.classList.remove('ng-valid');

      errorCont.classList.remove('d-none');
      errorCont.classList.add('alert-danger');

      liTag = <HTMLElement> document.createElement('li');
      errorUl.appendChild(liTag).innerHTML = 'Passwords do not match!';

      return false;

    } else if (event === current) {

      password_confirmation.classList.remove('ng-invalid');
      password_confirmation.classList.add('ng-valid');

      errorCont.classList.remove('d-none');
      errorCont.classList.add('d-none');

      return true;

    }
  }

  deleteHandler(notification, autoClose?) {
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;
    const liTag = document.createElement('li');

    errorCont.classList.remove('d-none');
    errorCont.classList.add('alert-danger');

    if (autoClose) {
      setTimeout(() => {
        errorCont.classList.add('d-none');
      }, 3e3);
    }

    return errorUl.appendChild(liTag).innerHTML = notification;
  }

  successHandler(notification, autoClose?) {
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;
    const liTag = document.createElement('li');

    errorCont.classList.remove('d-none');
    errorCont.classList.add('alert-success');

    if (autoClose) {
      setTimeout(() => {
        errorCont.classList.add('d-none');
      }, 2e3);
    }

    return errorUl.appendChild(liTag).innerHTML = notification;
  }

  showErrorHandler(notification, autoClose?) {
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;
    const liTag = document.createElement('li');

    errorCont.classList.remove('d-none');
    errorCont.classList.add('alert-danger');

    if (autoClose) {
      setTimeout(() => {
        errorCont.classList.add('d-none');
      }, 2e3);
    }

    return errorUl.appendChild(liTag).innerHTML = notification;
  }

  errorHandler(error) {
    // console.log(error.error);
    const errors = error.error;
    const errorCont = document.getElementById('error-messages') as HTMLElement;
    const errorUl = document.getElementById('errors') as HTMLElement;
    let liTag;

    errorCont.classList.remove('d-none');
    errorCont.classList.add('alert-danger');

    if (error.status === 500) {
      liTag = document.createElement('li');
      return errorUl.appendChild(liTag).innerHTML = 'Internal Server Error';
    }

    for (const key in errors) {
      if (errors.hasOwnProperty(key)) {
        if (key === 'error') {
          liTag = document.createElement('li');
          errorUl.appendChild(liTag).innerHTML = errors[key];
        } else {
          const array = errors[key];

          array.forEach((value, index) => {
            liTag = document.createElement('li');
            errorUl.appendChild(liTag).innerHTML = value;
          });
        }
      }
    }
  }

}
