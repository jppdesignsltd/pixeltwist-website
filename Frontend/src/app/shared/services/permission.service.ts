import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import {Permission} from '../interfaces/permission.interface';
import 'rxjs/Rx';

@Injectable()
export class PermissionService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getPermissions(): Observable<any> {
    return this.http.get<Permission>(this.global.getUrl() + 'permissions')
      .map(
        (permissions) => {

          return permissions;
        }
      );
  }

}
