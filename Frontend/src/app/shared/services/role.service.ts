import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './auth.service';
import {GlobalService} from './global.service';
import 'rxjs/Rx';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Roles} from '../interfaces/role.interface';


@Injectable()
export class RoleService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getRoles(): Observable<any> {
    return this.http.get<Roles[]>(this.global.getUrl() + 'roles')
      .map(
        (roles) => {

          return roles;
        }
      );
  }

  createRole(form) {
    const body = JSON.stringify(form);

    return this.http.post<Roles>(this.global.getUrl() + 'roles', body)
      .map(
        (role) => {
          return role;
        }
      )
      .catch(error => Observable.throw(error));
  }

  updateRole(id, form) {
    const body = JSON.stringify(form);

    return this.http.put<Roles>(this.global.getUrl() + 'roles/' + id, body)
      .map(
        (role) => {
          return role;
        }
      )
  }

}
