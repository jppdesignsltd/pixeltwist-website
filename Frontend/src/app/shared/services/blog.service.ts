import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import {Post} from '../interfaces/post.interface';
import 'rxjs/Rx';

@Injectable()
export class BlogService {

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getPosts(): Observable<any> {
    return this.http.get<Post[]>(this.global.getUrl() + 'blog')
      .map(
        (posts) => {
          return posts;
        }
      );
  }

  getTagPosts(slug: string): Observable<any> {
    return this.http.get<Post[]>(this.global.getUrl() + 'blog/tag/' + slug)
      .map(
        (posts) => {
          return posts;
        }
      )
  }

  getCategoryPosts(slug: string): Observable<any> {
    return this.http.get<Post[]>(this.global.getUrl() + 'blog/category/' + slug)
      .map(
        (posts) => {
          return posts;
        }
      )
  }

  getSinglePost(slug: string): Observable<any> {
    return this.http.get<Post>(this.global.getUrl() + 'blog/' + slug)
      .map(
        (post) => {
          return post;
        }
      );
  }

}
