import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../interfaces/user.interface';
import 'rxjs/Rx';

@Injectable()
export class UserService {
  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  getUsers(): Observable<any> {
    return this.http.get<User[]>(this.global.getUrl() + 'user')
      .map(
        (users) => {

          return users;
        }
      );
  }

  updateUser(id: number, form) {
    const body = JSON.stringify(form);

    return this.http.put(this.global.getUrl() + 'user/' + id, body)
      .map(
        (response) => {
          return response;
        }
      );
  }

  toggleUserActive(id: number, form) {
    const body = JSON.stringify(form);

    return this.http.put(this.global.getUrl() + 'user/' + id, body)
      .map(
        (response) => {
          return response;
        }
      );
  }

  getUser(id?: number): Observable<any> {
    if (!id) {
      return this.http.get<User>(this.global.getUrl() + 'current-user')
        .map(
          (user) => {
            return user;
          }
        );
    }

    return this.http.get<User>(this.global.getUrl() + 'user/' + id)
      .map(
        (user) => {
          return user;
        }
      );
  }
}
