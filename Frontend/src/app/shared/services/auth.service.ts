import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Router} from '@angular/router';
import {GlobalService} from './global.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class AuthService {

  constructor(private http: Http,
              private router: Router,
              private global: GlobalService) {

  }

  signup(form) {
    const body = JSON.stringify(form);

    return this.http.post(this.global.getUrl() + 'register', body);
  }

  signin(email: string, password: string) {

    return this.http.post(this.global.getUrl() + 'signin',
      {email: email, password: password})
      .map(
        (response: Response) => {
          const token = response.json().token;
          const user = response.json().user;
          const role = response.json().role;
          const base64Url = token.split('.')[1];
          const base64 = base64Url.replace('-', '+').replace('_', '/');

          return {
            token: token,
            // user: user,
            role: role,
            decoded: JSON.parse(window.atob(base64)),
            exp: JSON.parse(window.atob(base64)).exp
          };
        }
      )
      .do(
        tokenData => {
          localStorage.setItem('token', tokenData.token);
          localStorage.setItem('exp', tokenData.exp);
          // localStorage.setItem('user', tokenData.user);
          localStorage.setItem('role', tokenData.role);
        }
      );
  }

  refreshToken(): Observable<any> {
    const url = this.global.getUrl() + 'refresh_token?token=' + this.getToken();

    return this.http.get(url)
      .map(
        (response: Response) => {
          const token = response.json().token;
          const base64Url = token.split('.')[1];
          const base64 = base64Url.replace('-', '+').replace('_', '/');

          return {
            token: token,
            decoded: JSON.parse(window.atob(base64)),
            exp: JSON.parse(window.atob(base64)).exp
          }
        }
      )
      .do(
        tokenData => {
          localStorage.setItem('token', tokenData.token);
          localStorage.setItem('exp', tokenData.exp);
        }
      );
  }

  reset(email: string) {
    return this.http.post(this.global.getUrl() + 'password/email',
      {email: email})
      .map(
        (response: Response) => {
          const reset_token = response.json().reset_token;

          return {
            reset_token: reset_token
          };
        }
      )
      .do(
        tokenData => {
          localStorage.setItem('reset_token', tokenData.reset_token);
        }
      );
  }

  resetPassword(form) {
    const body = JSON.stringify(form);
    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(this.global.getUrl() + 'password/reset', body, {
      headers: headers
    })
      .map(
        (response: Response) => {
          response.json();
        }
      );
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  getRole(): string {
    return localStorage.getItem('role');
  }

  getResetToken(): string {
    return localStorage.getItem('reset_token');
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/');
    this.router.events.subscribe(
      (path) => {
        window.scrollTo(0, 0);
      }
    );
  }

  isLoggedIn() {
    const exp = parseInt(localStorage.getItem('exp'), 10);
    const date = Math.floor(Date.now() / 1000);
    const time = Math.floor((exp - date) / 60);

    // console.log(localStorage.getItem('token') !== null);

    if (exp < date) {
      this.logout();
      return localStorage.getItem('token') === null;
    }

    if (time < 15) {
      console.log('Refresh Token');
      this.refreshToken().subscribe();
    }

    return localStorage.getItem('token') !== null;
  }


}
