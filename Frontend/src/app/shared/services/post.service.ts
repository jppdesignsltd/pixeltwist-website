import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {HttpClient} from '@angular/common/http';
import 'rxjs/Rx';
import {Post} from '../interfaces/post.interface';

@Injectable()
export class PostService {
  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  createPost(form) {
    const body = JSON.stringify(form);

    return this.http.post<Post>(this.global.getUrl() + 'post', body)
      .map(
        (post) => {
          return post;
        }
      )
      .catch(error => Observable.throw(error));
  }

  getPosts(): Observable<any> {
    return this.http.get<Post[]>(this.global.getUrl() + 'post')
      .map(
        (posts) => {
          return posts;
        }
      );
  }

  getAuthorPosts(): Observable<any> {
    return this.http.get<Post[]>(this.global.getUrl() + 'author-posts')
      .map(
        (posts) => {
          return posts;
        }
      );
  }

  getSinglePost(id: number): Observable<any> {
    return this.http.get<Post>(this.global.getUrl() + 'post/' + id)
      .map(
        (post) => {
          return post;
        }
      );
  }

  updatePost(id: number, form) {
    const body = JSON.stringify(form);

    return this.http.put<Post>(this.global.getUrl() + 'post/' + id, body)
      .map(
        (post) => {
          return post;
        }
      );
  }

  deletePost(id: number) {
    return this.http.delete(this.global.getUrl() + 'post/' + id);
  }

}
