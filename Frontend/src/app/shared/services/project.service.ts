import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from './global.service';
import {Project} from '../interfaces/project.interface';
import {HttpClient} from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class ProjectService {
  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  createProject(form) {
    const body = JSON.stringify(form);

    return this.http.post<Project>(this.global.getUrl() + 'project', body)
      .map(
        (project) => {
          return project;
        }
      );
      // .catch(error => Observable.throw(error));
  }

  getProjects(): Observable<any> {
    return this.http.get<Project[]>(this.global.getUrl() + 'project')
      .map(
        (projects) => {
          return projects;
        }
      );
  }

  getAuthorProjects(): Observable<any> {
    return this.http.get<Project[]>(this.global.getUrl() + 'author-projects')
      .map(
        (projects) => {
          return projects;
        }
      );
  }

  getSingleProject(id: number): Observable<any> {
    return this.http.get<Project>(this.global.getUrl() + 'project/' + id)
      .map(
        (project) => {
          return project;
        }
      );
  }

  updateProject(id: number, form) {
    const body = JSON.stringify(form);

    return this.http.put<Project>(this.global.getUrl() + 'project/' + id, body)
      .map(
        (project) => {
          return project;
        }
      );
  }

  deleteProject(id: number) {
    return this.http.delete(this.global.getUrl() + 'project/' + id);

  // .map((response) => {return response})
  }

}
