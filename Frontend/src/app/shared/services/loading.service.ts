import {Injectable} from '@angular/core';

@Injectable()
export class LoadingService {

  constructor() {
  }

  showLoading() {
    const loadingCont = document.getElementById('loading-container') as HTMLElement;
    loadingCont.classList.remove('d-none');
    window.scrollTo(0, 0);
  }

  hideLoading() {
    const loadingCont = document.getElementById('loading-container') as HTMLElement;
    loadingCont.classList.add('d-none');
    window.scrollTo(0, 0);
  }
}
