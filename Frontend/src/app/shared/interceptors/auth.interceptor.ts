import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {AuthService} from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // console.log('Intercepted', req);

    const copiedReq = req.clone({
      headers: req.headers.set('Content-Type', 'application/json').append('X-Requested-With', 'XMLHttpRequest'),
      params: req.params.set('token', this.authService.getToken())
    });

    // console.log(copiedReq);

    return next.handle(copiedReq);
  }
}
