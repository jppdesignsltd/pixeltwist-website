import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {TagService} from '../services/tag.service';
import {Tags} from '../interfaces/tags.interface';

@Injectable()
export class TagResolver implements Resolve<Tags> {

  constructor(
    private tagService: TagService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Tags> | Promise<Tags> | Tags {
    return this.tagService.getSingleTag(+activeRoute.params['id']);
  }
}
