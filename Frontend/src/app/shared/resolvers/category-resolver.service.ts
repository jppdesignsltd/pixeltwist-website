import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {CategoryService} from '../services/category.service';
import {Category} from '../interfaces/category.interface';

@Injectable()
export class CategoryResolver implements Resolve<Category> {

  constructor(
    private categoryService: CategoryService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category> | Promise<Category> | Category {
    return this.categoryService.getSingleCategory(+activeRoute.params['id']);
  }
}
