import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ProjectService} from '../services/project.service';
import {Injectable} from '@angular/core';
import {Project} from '../interfaces/project.interface';

@Injectable()
export class ProjectResolver implements Resolve<Project> {

  constructor(
    private projectService: ProjectService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> | Promise<Project> | Project {
    return this.projectService.getSingleProject(+activeRoute.params['id']);
  }
}
