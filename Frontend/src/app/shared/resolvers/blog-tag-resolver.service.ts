import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {BlogService} from '../services/blog.service';

@Injectable()
export class BlogTagResolver implements Resolve<any> {

  constructor(private blogService: BlogService) {
  }

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> {

    return this.blogService.getTagPosts(activeRoute.params['slug']);

  }
}
