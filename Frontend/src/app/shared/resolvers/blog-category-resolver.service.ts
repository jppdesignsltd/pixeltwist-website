import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Post} from '../interfaces/post.interface';
import {BlogService} from '../services/blog.service';

@Injectable()
export class BlogCategoryResolver implements Resolve<Post> {

  constructor(private blogService: BlogService) {
  }

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post> | Promise<Post> | Post {

    return this.blogService.getCategoryPosts(activeRoute.params['slug']);
  }
}
