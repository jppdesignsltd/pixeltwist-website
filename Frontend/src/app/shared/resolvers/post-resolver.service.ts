import {Post} from '../interfaces/post.interface';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {PostService} from '../services/post.service';
import {Injectable} from '@angular/core';

@Injectable()
export class PostResolver implements Resolve<Post> {

  constructor(
    private postService: PostService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post> | Promise<Post> | Post {
    return this.postService.getSinglePost(+activeRoute.params['id']);
  }
}
