import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Services} from '../interfaces/service.interface';
import {ServiceService} from '../services/service.service';

@Injectable()
export class ServiceResolver implements Resolve<Services> {

  constructor(
    private serviceService: ServiceService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Services> | Promise<Services> | Services {
    return this.serviceService.getSingleService(+activeRoute.params['id']);
  }
}
