import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Project} from '../interfaces/project.interface';
import {WorkService} from '../services/work.service';

@Injectable()
export class WorkServiceResolver implements Resolve<Project> {

  constructor(private workService: WorkService) {
  }

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> | Promise<Project> | Project {

    return this.workService.getServiceProjects(activeRoute.params['slug']);
  }
}
