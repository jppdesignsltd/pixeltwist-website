import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {User} from '../interfaces/user.interface';
import {UserService} from '../services/user.service';

@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(
    private userService: UserService
  ) {}

  resolve(activeRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this.userService.getUser(+activeRoute.params['id']);
  }
}
