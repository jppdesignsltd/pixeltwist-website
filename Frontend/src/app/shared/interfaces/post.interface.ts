export interface Post {
  title: string;
  slug: string;
  category?: any;
  subtitle?: string;
  main_content: string;
  featured_image: string;
  user_id: number;
  status: string;
  live_date: string;
  category_id: number;
  id: number;
  meta_keywords: string;
  meta_description: string;
  tags?: any;
  user?: any;
}
