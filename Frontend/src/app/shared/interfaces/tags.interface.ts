export interface Tags {
  title: string;
  slug: string;
  id: number;
  posts?: any;
  projects?: any;
}
