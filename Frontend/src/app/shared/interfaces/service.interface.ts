export interface Services {
  title: string;
  slug: string;
  id: number;
  projects?: any;
}
