export interface User {
  id: number;
  firstname: string;
  avatar: string;
  lastname: string;
  email: string;
  password: string;
  active: string;
  username?: string;
  created_at?: string;
  updated_at?: string;
  roles?: any;
}
