export interface Category {
  title: string;
  slug: string;
  id: number;
  posts?: any;
}
