export interface Project {
  title: string;
  slug: string;
  subtitle?: string;
  main_content: string;
  logo_image?: string;
  cover_image?: string;
  user_id: number;
  status: string;
  id: number;
  tags?: any;
  services?: any;
  user?: any;
  website_url: string;
  meta_keywords: string;
  meta_description: string;
}
