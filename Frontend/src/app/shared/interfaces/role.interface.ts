export interface Roles {
  id: number;
  name: string;
  display_name: string;
  description: string;
  permissions: any;
}
