import {Pipe, PipeTransform} from '@angular/core';
import {format} from 'date-fns';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, args: string[]): any {
    if (value) {
      const date = value instanceof Date ? value : new Date(value);
      return format(date, 'DD/MM/YY');
    }
  }

}
