import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  validRoles: string[] = ['admin', 'editor', 'author'];

  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const role = this.authService.getRole();
    const token = this.authService.getToken();
    const hasRole = this.validRoles.indexOf(role) !== -1;

    if (!token) {
      this.router.navigateByUrl('/auth/signin');
      return false;

    } else {

      if (role === 'standard') {
        this.router.navigateByUrl('/my-account');
        // console.log('doing the ting');
        return false
      }

      if (hasRole) {
        return true;
      }
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const role = this.authService.getRole();
    const token = this.authService.getToken();
    const hasRole = this.validRoles.indexOf(role) !== -1;

    if (!token) {
      this.router.navigateByUrl('/auth/signin');
      return false;

    } else {

      if (role === 'standard') {
        this.router.navigateByUrl('/my-account');
        // console.log('doing the ting');
        return false
      }

      if (hasRole) {
        return true;
      }
    }
  }
}
