import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthComponent} from '../core/auth/auth.component';
import {SigninComponent} from '../core/auth/signin/signin.component';
import {SignupComponent} from '../core/auth/signup/signup.component';
import {ForgotPasswordComponent} from '../core/auth/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from '../core/auth/reset-password/reset-password.component';

const authRoutes: Routes = [
  {
    path: '', component: AuthComponent, children: [
    {path: 'signin', component: SigninComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent},
    {path: 'reset-password', component: ResetPasswordComponent},
  ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AuthRoutingModule {
}
