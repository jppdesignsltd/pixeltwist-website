import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PagesComponent} from '../core/pages/pages.component';
import {BlogComponent} from '../core/pages/blog/blog.component';
import {BlogShowComponent} from '../core/pages/blog/blog-show.component';
import {OurWorkComponent} from '../core/pages/work/our-work.component';
import {OurWorkShowComponent} from '../core/pages/work/our-work-show.component';
import {WhatWeDoComponent} from '../core/pages/services/what-we-do.component';
import {WebDevelopmentComponent} from '../core/pages/services/web-development.component';
import {SEOComponent} from '../core/pages/services/seo.component';
import {HostingComponent} from '../core/pages/services/hosting.component';
import {AboutComponent} from '../core/pages/about.component';
import {ContactComponent} from '../core/pages/contact.component';
import {GraphicDesignComponent} from '../core/pages/services/graphic-design.component';
import {WorkResolver} from '../shared/resolvers/work-resolver.service';
import {BlogResolver} from '../shared/resolvers/blog-resolver.service';
import {BlogTagResolver} from '../shared/resolvers/blog-tag-resolver.service';
import {BlogTagComponent} from '../core/pages/blog/blog-tag.component';
import {BlogCategoryComponent} from '../core/pages/blog/blog-category.component';
import {BlogCategoryResolver} from '../shared/resolvers/blog-category-resolver.service';
import {WorkTagResolver} from '../shared/resolvers/work-tag-resolver.service';
import {WorkServiceResolver} from '../shared/resolvers/work-service-resolver.service';
import {WorkTagComponent} from '../core/pages/work/work-tag.component';
import {WorkServiceComponent} from '../core/pages/work/work-services.component';

const pageRoutes: Routes = [
  {
    path: '', component: PagesComponent, children: [
    {path: 'about', component: AboutComponent},
    {path: 'contact', component: ContactComponent},
    {
      path: 'journal',
      component: BlogComponent
    },
    {
      path: 'blog/:slug',
      component: BlogShowComponent,
      resolve: {json: BlogResolver}
    },
    {
      path: 'blog/tag/:slug',
      component: BlogTagComponent,
      resolve: {json: BlogTagResolver}
    },
    {
      path: 'blog/category/:slug',
      component: BlogCategoryComponent,
      resolve: {json: BlogCategoryResolver}
    },
    {
      path: 'work',
      component: OurWorkComponent
    },
    {
      path: 'projects/:slug',
      component: OurWorkShowComponent,
      resolve: {json: WorkResolver}
    },
    {
      path: 'projects/tag/:slug',
      component: WorkTagComponent,
      resolve: {json: WorkTagResolver}
    },
    {
      path: 'projects/service/:slug',
      component: WorkServiceComponent,
      resolve: {json: WorkServiceResolver}
    },
    {path: 'what-we-do', component: WhatWeDoComponent},
    {path: 'services/web-development', component: WebDevelopmentComponent},
    {path: 'services/graphic-design', component: GraphicDesignComponent},
    {path: 'services/search-engine-optimisation', component: SEOComponent},
    {path: 'services/hosting', component: HostingComponent}
  ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(pageRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class PageRoutingModule {
}
