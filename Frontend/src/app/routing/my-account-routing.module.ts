import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {StandardAccountComponent} from '../core/standard-account/standard-account.component';
import {MyAccountComponent} from '../core/standard-account/my-account.component';

const myAccountRoutes: Routes = [
  {
    path: '', component: StandardAccountComponent, children: [
    {path: '', component: MyAccountComponent}
  ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(myAccountRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class MyAccountRoutingModule {
}
