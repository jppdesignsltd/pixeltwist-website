import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {NotFoundComponent} from '../core/pages/errors/not-found.component';

const wildCardRoutes: Routes = [
  {
    path: '**',
    component: NotFoundComponent,
    data: {
      message: 'Page Not Found!'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(wildCardRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class WildcardRoutingModule {}
