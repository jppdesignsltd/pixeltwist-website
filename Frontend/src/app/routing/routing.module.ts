import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomeComponent} from '../core/pages/home/home.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'admin', loadChildren: '../modules/admin.module#AdminModule'},
  {path: 'my-account', loadChildren: '../modules/standard-account.module#StandardAccountModule'},
  {path: 'auth', loadChildren: '../modules/auth.module#AuthModule'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes,
      {
        useHash: false,
        // preloadingStrategy: PreloadAllModules
      })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {

}
