import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AdminComponent} from '../core/admin/admin.component';
import {AuthGuard} from '../shared/guards/auth.guard';
import {DashboardComponent} from '../core/admin/dashboard.component';
import {UserProfileComponent} from '../core/admin/profile/user-profile.component';
import {PostsComponent} from '../core/admin/posts/posts.component';
import {NewPostComponent} from '../core/admin/posts/new-post.component';
import {CanDeactivateGuard} from '../shared/guards/can-deactivate.guard';
import {EditPostComponent} from '../core/admin/posts/edit-post.component';
import {PostResolver} from '../shared/resolvers/post-resolver.service';
import {ShowPostComponent} from '../core/admin/posts/show-post.component';
import {ProjectsComponent} from '../core/admin/projects/projects.component';
import {NewProjectComponent} from '../core/admin/projects/new-project.component';
import {EditProjectComponent} from '../core/admin/projects/edit-project.component';
import {ProjectResolver} from '../shared/resolvers/project-resolver.service';
import {ShowProjectComponent} from '../core/admin/projects/show-project.component';
import {CategoriesComponent} from '../core/admin/categories/categories.component';
import {ShowCategoryComponent} from '../core/admin/categories/show-category.component';
import {TagsComponent} from '../core/admin/tags/tags.component';
import {ShowTagComponent} from '../core/admin/tags/show-tag.component';
import {UsersComponent} from '../core/admin/users/users.component';
import {NewUserComponent} from '../core/admin/users/new-user.component';
import {EditUserComponent} from '../core/admin/users/edit-user.component';
import {CategoryResolver} from '../shared/resolvers/category-resolver.service';
import {TagResolver} from '../shared/resolvers/tag-resolver.service';
import {UserResolver} from '../shared/resolvers/user-resolver.service';
import {RolesAndPermissionsComponent} from '../core/admin/roles-and-permissions/roles-and-permissions.component';
import {EditorGuard} from '../shared/guards/editor.guard';
import {AdminGuard} from '../shared/guards/admin.guard';
import {MyProjectsComponent} from '../core/admin/projects/my-projects.component';
import {AuthorGuard} from '../shared/guards/author.guard';
import {MyPostsComponent} from '../core/admin/posts/my-posts.component';
import {AdminServicesComponent} from '../core/admin/services/admin-services.component';
import {ShowServiceComponent} from '../core/admin/services/show-service.component';
import {ServiceResolver} from '../shared/resolvers/service-resolver.service';

const adminRoutes: Routes = [
  {
    path: '', component: AdminComponent,
    canActivateChild: [AuthGuard],
    children: [
      {path: '', component: DashboardComponent},
      {path: 'profile', component: UserProfileComponent},
      {
        path: 'posts',
        component: PostsComponent,
        canActivate: [EditorGuard]
      },
      {
        path: 'my-posts',
        component: MyPostsComponent,
        canActivate: [AuthorGuard]
      },
      {
        path: 'post/create',
        component: NewPostComponent,
        // canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'post/edit/:id',
        component: EditPostComponent,
        resolve: {json: PostResolver},
        // canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'post/show/:id',
        component: ShowPostComponent,
        resolve: {json: PostResolver}
      },
      {
        path: 'projects',
        component: ProjectsComponent,
        canActivate: [EditorGuard]
      },
      {
        path: 'my-projects',
        component: MyProjectsComponent,
        canActivate: [AuthorGuard]
      },
      {
        path: 'project/create',
        component: NewProjectComponent,
        // canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'project/edit/:id',
        component: EditProjectComponent,
        resolve: {json: ProjectResolver},
        // canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'project/show/:id',
        component: ShowProjectComponent,
        resolve: {json: ProjectResolver}
      },
      {
        path: 'categories',
        component: CategoriesComponent,
        canActivate: [EditorGuard],
      },
      {
        path: 'category/show/:id',
        component: ShowCategoryComponent,
        canActivate: [EditorGuard],
        resolve: {json: CategoryResolver}
      },
      {
        path: 'tags',
        component: TagsComponent,
        canActivate: [EditorGuard]
      },
      {
        path: 'tag/show/:id',
        component: ShowTagComponent,
        canActivate: [EditorGuard],
        resolve: {json: TagResolver}
      },
      {
        path: 'services',
        component: AdminServicesComponent,
        canActivate: [EditorGuard]
      },
      {
        path: 'service/show/:id',
        component: ShowServiceComponent,
        canActivate: [EditorGuard],
        resolve: {json: ServiceResolver}
      },
      {
        path: 'users',
        canActivate: [AdminGuard],
        component: UsersComponent
      },
      {
        path: 'user/create', component: NewUserComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'user/edit/:id',
        component: EditUserComponent,
        canActivate: [AdminGuard],
        resolve: {json: UserResolver}
      },
      {
        path: 'roles-and-permissions',
        canActivate: [AdminGuard],
        component: RolesAndPermissionsComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AdminRoutingModule {
}
