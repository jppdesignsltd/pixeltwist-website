import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from '../app.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('App: PixelTwist', () => {

  let fixture;
  let app;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    });
  });

  it('Should create the app', async(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;

    expect(app).toBeTruthy();
  }));
});
