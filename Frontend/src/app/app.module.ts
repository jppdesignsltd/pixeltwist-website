import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {GlobalService} from './shared/services/global.service';
import {SharedModule} from './modules/shared.module';
import {CoreModule} from './modules/core.module';
import {WildcardRoutingModule} from './routing/wildcard-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorService} from './shared/services/error.service';
import {LoadingService} from './shared/services/loading.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {AuthInterceptor} from './shared/interceptors/auth.interceptor';
import {LogInterceptor} from './shared/interceptors/log.interceptor';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    WildcardRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [
    GlobalService,
    ErrorService,
    LoadingService,
    Title,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: LogInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
