/*
* Main Compressed JS
*/

Waves.attach('.waves', ['waves-float']);
Waves.init();


// // Map for Contact Page - Add no click function when page load
// $(document).ready(function () {
//   // Enable the pointer events when user clicks the iframe container.
//   $('#map').addClass('scrolloff'); // set the pointer events to none on doc ready
//   $('#map-canvas').on('click', function () {
//       $('#map').removeClass('scrolloff'); // set the pointer events true on click
//   });
//
//   // Disable pointer events when the mouse leave the canvas area;
//   $("#map").mouseleave(function () {
//       $('#map').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
//   });
// });
//
// // Masonry JS
// if ($(".js-masonry")[0]){
//   $(document).ready(function () {
//     var container = document.querySelector(".js-masonry");
//     //create empty var msnry
//     var msnry;
//     // initialize Masonry after all images have loaded
//     imagesLoaded( container, function() {
//         msnry = new Masonry( container, {
//             itemSelector: ".masonry-item"
//         });
//     });
//   });
// }
